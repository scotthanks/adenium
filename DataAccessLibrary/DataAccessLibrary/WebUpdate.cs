﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace DataAccessLibrary
{
    public class UpateResponse
    {
        //"IsAuthenticated":true,
        //"GrowerOrderGuid":"b379e6f5-ce50-45d5-a813-4d20b6b4eec7",
        //"OrderLinesChanged":1,
        //"ProductsOrdered":0,
        //"PrecartOrderLinesDeleted":0,
        //"Success":false,
        //"Message":null,
        //"SupplierOrderStatuses":null  
        public bool IsAuthenticated { get; set; }
        public string GrowerOrderGuid { get; set; }
        public int OrderLinesChanged { get; set; }
        public int ProductsOrdered { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public string SupplierOrderStatuses { get; set; }
    }

    public class WebUpdate<T> where T : class
    {
        private HttpWebRequest httpWebRequest = null;
        public string ErrorMessage { get; set; }
        //public UpateResponse PutRequest(string controller, object request, string httpType ="PUT")
        public bool PutRequest(string controller, string httpType = "PUT")
        {
            try
            {

                string url = string.Format("{0}/{1}", EpsUrlService.EpsUrl, controller);

                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.CookieContainer = new CookieContainer(3);

                httpWebRequest.CookieContainer.Add(new Uri(url), CookieService.GetAccessCookie());
                if (CookieBox.AuthCookie != null)
                    httpWebRequest.CookieContainer.Add(new Uri(url), CookieBox.AuthCookie);
                else
                    httpWebRequest.CookieContainer.Add(new Uri(url), CookieService.GetAuthCookie());

                httpWebRequest.ContentType = "text/json";
                httpWebRequest.Method = httpType;

                //UpateResponse result = null;
                //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                //{
                //    string json = new JavaScriptSerializer().Serialize(request);

                //    streamWriter.Write(json);
                //    streamWriter.Flush();
                //    streamWriter.Close();

                //    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //    var stream = httpResponse.GetResponseStream();
                //    var serializer = new DataContractJsonSerializer(typeof(UpateResponse));
                //    result = (UpateResponse)serializer.ReadObject(stream);

                //    //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                //    //{
                //    //    result = streamReader.ReadToEnd();
                //    //}
                //}

                //return result;
                return true;
            }
            catch (Exception ex)
            {
                //UpateResponse result = new UpateResponse();
                //result.Message = ex.Message;
                //result.Success = false;
                //return result;
                ErrorMessage = ex.Message;
                return false;
            }
        }

        public T GetResponse(object request)
        {
            T result = null;

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(request);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    var stream = httpResponse.GetResponseStream();
                    var serializer = new DataContractJsonSerializer(typeof(T));
                    result = (T)serializer.ReadObject(stream);

                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return null;
            }

            return result;
        }
    }
}
