﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

using System.Data;
using BusinessObjectsLibrary;


namespace DataAccessLibrary
{
    public class SharedDataService : DataServiceBase
    {
        public SharedDataService(StatusObject status)
            : base(status)
        {}


        public List<ListItem> GetLookupList(string listName)
        {
            var lookupList = new List<ListItem>();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[LookupListGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                         .Add(new SqlParameter("@ListName", SqlDbType.NVarChar,50))
                         .Value = listName;
                        connection.Open();

                        var reader = command.ExecuteReader();
                        
                        while (reader.Read()){
                            var item = new ListItem();
                        
                            item.Guid = (Guid)reader["Guid"];
                            item.Code = (string)reader["Code"];
                            item.Name = (string)reader["Name"];
                            item.Display = item.Code + " - " + item.Name;

                            lookupList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Lookup List Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "SharedDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return lookupList;
        }

    }
}
