﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    static public class ePSAccount
    {
        public static string UserId { get { return "dan.deleeuw@latitude40.com"; } }
        public static string Pwd { get { return "greenhouse13"; } }
        public static string PersonName { get { return "Dan DeLeeuw"; } }
        public static string AdminName { get { return "Admin User"; } }
    }

    public class CookieService
    {
        public static Cookie GetAccessCookie()
        {
            const string ACCESS_CODE_VALUE = "369";
            const string ACCESS_COOKIE_NAME = "AccessCookie";
            const double ACCESS_COOKIE_DURATION_HOURS = 36d;
            var accessCookie = new Cookie(ACCESS_COOKIE_NAME, ACCESS_CODE_VALUE);
            accessCookie.Path = "/";
            accessCookie.Expires = DateTime.Now.AddHours(ACCESS_COOKIE_DURATION_HOURS);
            return accessCookie;
        }



        public static Cookie GetAuthCookie()
        {
            CookieBox.AuthCookie = null;
            string url = EpsUrlService.EpsUrl + "/AdminAuth";

            string form = "Guid=" + Guid.Empty;
            //form += "&username=" + "michaelr";
            //form += "&password=" + "1234-five";
            form += "&username=" + ePSAccount.UserId;
            form += "&password=" + ePSAccount.Pwd;

            //http://EpsTest.azurewebsites.net
            //http://stackoverflow.com/questions/930807/c-sharp-login-to-website-via-program
            HttpWebRequest httpWebRequest = System.Net.HttpWebRequest.CreateHttp(url);

            httpWebRequest.CookieContainer = new CookieContainer(3);
            httpWebRequest.Method = "POST";

            httpWebRequest.CookieContainer.Add(new Uri(url), CookieService.GetAccessCookie());

            //Add these, as we're doing a POST
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";
            //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(form);
            httpWebRequest.ContentLength = bytes.Length;
            System.IO.Stream os = httpWebRequest.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp;
            try
            {
                resp = httpWebRequest.GetResponse();
                if (resp == null)
                    throw new Exception("httpWebRequest to producion app returned null");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            //.ASPXAUTH=E464219A46AC25CFC9BF4D1D2A4060B618A74EB19ACC6ABDC2BE55643FC5F3ED43AFA61D5E8249F76ACDF4406F247A7C700983E795A3B8A57D4A5131BB402D6FFDFB53F2A9B06FFC8A6FE11546CEA08D3AD46D5A57037E5B33EEF3FE194FCA58; path=/; HttpOnly
            string cookies = resp.Headers["Set-cookie"];
            if (!cookies.ToUpper().Contains(".ASPXAUTH="))
                throw new Exception("Producion application refused login request.");

            int off = cookies.IndexOf(".ASPXAUTH=");
            off = cookies.IndexOf("=", off) + 1;
            int end = cookies.IndexOf(";", off);
            string val = cookies.Substring(off, end - off);
            CookieBox.AuthCookie = new Cookie(".ASPXAUTH", val);
            CookieBox.AuthCookie.Expires = DateTime.Now.AddHours(48.0);

            return CookieBox.AuthCookie;
        }
    }
}
