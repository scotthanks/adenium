﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace DataAccessLibrary
{
    public class CookieBox
    {
        static Cookie authCookie = null;
        public static Cookie AuthCookie { get { return authCookie; } set { authCookie = value; } }
    }

    public class WebApi<T> where T : class 
    {
        public string Controller { get; set; }
        public string JsonString { get; set; }
        private readonly Dictionary<string, string> _parameterDictionary; 

        public WebApi(string controllerName)
        {
            _parameterDictionary = new Dictionary<string, string>();
            Controller = controllerName;
        }

        public void AddParameter(string parameterName, string value)
        {
            if (_parameterDictionary.ContainsKey(parameterName))
            {
                throw new ApplicationException(string.Format( "A parameter called \"{0}\" has already been added.", parameterName));
            }
            else
            {
                _parameterDictionary.Add(parameterName, value);
            }
        }

        private string Url
        {
            get
            {
                return string.Format("{0}/{1}", EpsUrlService.EpsUrl, Controller);
            }
        }

        private string ParameterString
        {
            get
            {
                var stringBuilder = new StringBuilder();
                char separator = '?';
                foreach (var key in _parameterDictionary.Keys)
                {
                    stringBuilder.Append(separator);
                    stringBuilder.Append(key);
                    stringBuilder.Append('=');

                    string value;
                    _parameterDictionary.TryGetValue(key, out value);
                    value = HttpUtility.UrlEncode(value);
                    stringBuilder.Append(value);

                    separator = '&';
                }

                return stringBuilder.ToString();
            }
        }

        private string FullUri
        {
            get { return Url + ParameterString; }
        }


        public T GetResponse()
        {


            //const string ACCESS_CODE_VALUE = "369";
            ////const string ACCESS_CODE_KEY = "AccessCode"; // gib take out
            //const string ACCESS_COOKIE_NAME = "AccessCookie";
            //const double ACCESS_COOKIE_DURATION_HOURS = 36d;

            ////HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(FullUri);

            HttpWebRequest httpWebRequest = System.Net.HttpWebRequest.CreateHttp(FullUri);
            httpWebRequest.CookieContainer = new CookieContainer(3);

            //var accessCookie = new Cookie(ACCESS_COOKIE_NAME, ACCESS_CODE_VALUE);
            ////var accessCookie = new Cookie(ACCESS_CODE_KEY, ACCESS_CODE_VALUE);
            //accessCookie.Path = "/";
            ////accessCookie.Domain = "http://EpsTest.AzureWebsites.net";
            //accessCookie.Expires = DateTime.Now.AddHours(ACCESS_COOKIE_DURATION_HOURS);

            try
            {
                httpWebRequest.CookieContainer.Add(new Uri(FullUri), CookieService.GetAccessCookie());
                if (CookieBox.AuthCookie != null)
                    httpWebRequest.CookieContainer.Add(new Uri(FullUri), CookieBox.AuthCookie);
                else
                    httpWebRequest.CookieContainer.Add(new Uri(FullUri), CookieService.GetAuthCookie());
            }
            catch (Exception e)
            {
                throw e;
            }

            try
            {
                return GetResponse(httpWebRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
 
        }

        private T GetResponse(HttpWebRequest webRequest)
        {
            T result = null;

            WebResponse response = null;
            try
            {
                response = webRequest.GetResponse();
            }
            catch (WebException we)
            {
                HttpStatusCode wRespStatusCode = ((HttpWebResponse)we.Response).StatusCode;
                throw we;
            }

            if (response == null)
                throw new ApplicationException("API response was null.");

            var stream = response.GetResponseStream();

            if (stream == null)
                throw new ApplicationException("Stream from response was null.");

            //responseString = responseString.Replace("\"IsAuthenticated\":true,", "");

            //var s = new JavaScriptSerializer();
            //Object obj = s.DeserializeObject(responseString);
            //T pobj = (T)s.Deserialize(responseString, typeof(T));

            var serializer = new DataContractJsonSerializer(typeof(T));

            result = (T)serializer.ReadObject(stream);

            return result;
        }

       
    }
}
