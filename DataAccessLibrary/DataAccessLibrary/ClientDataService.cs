﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using BusinessObjectsLibrary;
using System.Configuration;
using System.Globalization;

namespace DataAccessLibrary
{
    public class ClientDataService : DataServiceBase
    {
        public ClientDataService(StatusObject status)
            : base(status)
        {}


        public List<Client> GetClientList()
        {
            var clientList = new List<Client>();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ClientsGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                       
                        
                        connection.Open();

                        var reader = command.ExecuteReader();
                        
                        while (reader.Read()){
                            var item = new Client();
                            item.Name = (string)reader["Name"];
                            item.Guid = (Guid)reader["Guid"];


                            clientList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Clients Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ClientDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return clientList;
        }

        public Client GetClient(Guid clientGuid)
        {
            var client = new Client();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ClientGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ClientGuid", SqlDbType.UniqueIdentifier))
                           .Value = clientGuid;
                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            client = new Client();
                            client.Name = (string)reader["Name"];
                            client.Guid = (Guid)reader["Guid"];


                           
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Client Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ClientDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return client;
        }
        public Guid AddUpdateClient(Guid clientGuid, string clientName)
        {
            Guid clientGuidOutput = new Guid();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ClientAddUpdate]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ClientID", SqlDbType.BigInt))
                           .Value = 0;
                        command.Parameters
                           .Add(new SqlParameter("@ClientGuid", SqlDbType.UniqueIdentifier))
                           .Value = clientGuid;
                        command.Parameters
                           .Add(new SqlParameter("@ClientName", SqlDbType.NVarChar,50))
                           .Value = clientName;
                        command.Parameters
                               .Add(new SqlParameter("@ClientGuidOutput", SqlDbType.UniqueIdentifier))
                              .Direction = ParameterDirection.Output;
                        connection.Open();

                        var iRet = (int)command.ExecuteNonQuery();
                        clientGuidOutput = (Guid)command.Parameters["@ClientGuidOutput"].Value;
                      
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Client Saved";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ClientDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return clientGuidOutput;
        }
        public bool DeleteClient(Guid clientGuid)
        {
           
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ClientDelete]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ClientGuid", SqlDbType.UniqueIdentifier))
                           .Value = clientGuid;
                        connection.Open();

                      
                        var iRet = (int)command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Client Deleted";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ClientDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return Status.Success;
        }
    }
}
