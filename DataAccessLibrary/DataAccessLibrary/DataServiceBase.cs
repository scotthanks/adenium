﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


namespace DataAccessLibrary
{
    public abstract class DataServiceBase
    {
        public DataServiceBase(StatusObject status)
        {

            this.Status = status;

        }
        public StatusObject Status { get; set; }

        public bool AddError(ErrorObject error)
        {
            List<ErrorObject> errorList = null;
            if (Status.Errors == null)
            { errorList = new List<ErrorObject>(); }
            else
            { errorList = Status.Errors.ToList(); }
            errorList.Add(error);
            Status.Errors = errorList;
            return true;

        }
        public SqlConnection GetConnection(string connectionType)
        {
            SqlConnection connection;
            ConnectionStringSettings connectionStringObject = ConfigurationManager.ConnectionStrings["DataDb"]; 

            switch (connectionType) { 
                case "Data":
                    var theURL = EpsUrlService.EpsUrl;
                    connectionStringObject = ConfigurationManager.ConnectionStrings["DataDb"];
                    if (theURL == "http://ordersdev.green-fuse.com/api" || Status.ForceDevEnvironment)  //this could be changed to Web Config if we ever move to a DEV and PROD platform for this website
                    {
                        connectionStringObject = ConfigurationManager.ConnectionStrings["DataDb"];
                    }
                    else
                    {
                        connectionStringObject = ConfigurationManager.ConnectionStrings["DataDbProd"];
                    }
                    break;
                case "Search":
                    connectionStringObject = ConfigurationManager.ConnectionStrings["DataDbSearch"];
                    break;
                case "B2B":
                    connectionStringObject = ConfigurationManager.ConnectionStrings["DataDbB2B"];
                    break;
                case "Cube":
                    connectionStringObject = ConfigurationManager.ConnectionStrings["DataCube"];
                    break;
                default:
                    break;
            }

            connection = new SqlConnection(connectionStringObject.ToString());
            return connection;

                
        }

    }
}





