﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using BusinessObjectsLibrary;
using System.Configuration;


namespace DataAccessLibrary
{
    public class EmailQueueDataService : DataServiceBase
    {

        public EmailQueueDataService(StatusObject status)
            : base(status)
        {

        }
        public bool AddEmail(string subject, string body, int delaySendMinutes, string emailType, string emailFrom, string emailTo, string emailCC, string emailBCC, Guid orderGuid, int updateCount, int addCount, int cancelCount)
        {
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                             
                    using (var command = new SqlCommand("[dbo].[EmailQueueAdd]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@sSubject", SqlDbType.NVarChar, 200))
                            .Value = subject;
                        command.Parameters
                            .Add(new SqlParameter("@sBody", SqlDbType.NVarChar, 0))
                            .Value = body;
                        command.Parameters
                            .Add(new SqlParameter("@iDelaySendMinutes", SqlDbType.Int))
                            .Value = delaySendMinutes;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailType", SqlDbType.NVarChar, 50))
                            .Value = emailType;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailTo", SqlDbType.NVarChar, 100))
                            .Value = emailTo;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailCC", SqlDbType.NVarChar, 100))
                            .Value = emailCC;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailBCC", SqlDbType.NVarChar, 100))
                            .Value = emailBCC;
                        command.Parameters
                            .Add(new SqlParameter("@gOrderGuid", SqlDbType.UniqueIdentifier))
                            .Value = orderGuid;
                        command.Parameters
                            .Add(new SqlParameter("@iUpdateCount", SqlDbType.Int))
                            .Value = updateCount;
                        command.Parameters
                            .Add(new SqlParameter("@iAddCount", SqlDbType.Int))
                            .Value = addCount;
                        command.Parameters
                            .Add(new SqlParameter("@iCancelCount", SqlDbType.Int))
                            .Value = cancelCount;
                        command.Parameters
                           .Add(new SqlParameter("@sEmailFrom", SqlDbType.NVarChar, 100))
                           .Value = emailFrom;
                        connection.Open();
                        var iReturn = command.ExecuteNonQuery();
                        connection.Close();
                    }
                }

                Status.StatusMessage = "Email Added to Queue";
                Status.Success = true;

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "EmailDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }

            return Status.Success;




        }
        public bool AddErrorLog(string subject, string body, string direction, string growerName, string orderNo)
        {
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {

                    using (var command = new SqlCommand("[dbo].[ErrorLogAdd]", connection))
                    {
 
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@Subject", SqlDbType.NVarChar, 200))
                            .Value = subject;
                        command.Parameters
                            .Add(new SqlParameter("@Body", SqlDbType.NVarChar, 0))
                            .Value = body;
                        command.Parameters
                            .Add(new SqlParameter("@Direction", SqlDbType.NVarChar, 100))
                            .Value = direction;
                        command.Parameters
                            .Add(new SqlParameter("@GrowerName", SqlDbType.NVarChar, 200))
                            .Value = growerName;
                        command.Parameters
                            .Add(new SqlParameter("@OrderNo", SqlDbType.NVarChar, 100))
                            .Value = orderNo;
                       
                        connection.Open();
                        var iReturn = command.ExecuteNonQuery();
                        connection.Close();
                    }
                }

                Status.StatusMessage = "Email Added to Error Log";
                Status.Success = true;

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "EmailDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }

            return Status.Success;




        }
        public bool UserGetFromCurrentUserName(out Guid userGuid, out string email)
        {

            var theEmail = "";
            var theUserGuid = new Guid();
            try
            {
                var connection = GetConnection("Data");
               
                using (connection)
                {

                    using (var command = new SqlCommand("[dbo].[UserGetFromUserName]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@UserName", SqlDbType.NVarChar,70))
                            .Value = Status.User.UserName;
                        command.Parameters
                            .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                            .Direction = ParameterDirection.Output;
                        
                        command.Parameters
                           .Add(new SqlParameter("@Email", SqlDbType.NVarChar, 100))
                           .Direction = ParameterDirection.Output;
                        connection.Open();
                        var response = (int)command.ExecuteNonQuery();
                        theEmail = (string)command.Parameters["@Email"].Value;
                        theUserGuid = (Guid)command.Parameters["@UserGuid"].Value;
                        connection.Close();
                        
                       
                        
                    }
                }

                Status.Success = true;
                Status.StatusMessage = "Email Retrieved";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "EmailDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }
            email = theEmail;
            userGuid = theUserGuid;
            return Status.Success;

        }
    }
}
