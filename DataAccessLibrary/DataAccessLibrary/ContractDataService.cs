﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using BusinessObjectsLibrary;
using System.Configuration;
using System.Globalization;

namespace DataAccessLibrary
{
    public class ContractDataService : DataServiceBase
    {
        public ContractDataService(StatusObject status)
            : base(status)
        {}


        public List<Consultant> GetConsultantList()
        {
            var consultantList = new List<Consultant>();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ConsultantsGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                       
                        
                        connection.Open();

                        var reader = command.ExecuteReader();
                        
                        while (reader.Read()){
                            var item = new Consultant();
                            item.CompanyName = (string)reader["CompanyName"];
                            item.FirstName = (string)reader["FirstName"];
                            item.LastName = (string)reader["LastName"];
                            item.IsInternal = (bool)reader["IsInternal"];
                            item.Guid = (Guid)reader["Guid"];


                            consultantList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Consultants Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ContractDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return consultantList;
        }

        public Consultant GetConsultant(Guid consultantGuid)
        {
            var consultant = new Consultant();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ConsultantGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ConsultantGuid", SqlDbType.UniqueIdentifier))
                           .Value = consultantGuid;
                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            consultant = new Consultant();
                            consultant.CompanyName = (string)reader["CompanyName"];
                            consultant.FirstName = (string)reader["FirstName"];
                            consultant.LastName = (string)reader["LastName"];
                            consultant.IsInternal = (bool)reader["IsInternal"];
                            consultant.Guid = (Guid)reader["Guid"];


                           
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Consultant Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ContractDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return consultant;
        }
        public Guid AddUpdateConsultant(Guid consultantGuid, string companyName, string firstName, string lastName, bool isInternal)
        {
            int isInteralInt = 0;
            if (isInternal == true)
            {
                isInteralInt = 1;
            }
            Guid consultantGuidOutput = new Guid();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ConsultantAddUpdate]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ConsultantID", SqlDbType.BigInt))
                           .Value = 0;
                        command.Parameters
                           .Add(new SqlParameter("@ConsultantGuid", SqlDbType.UniqueIdentifier))
                           .Value = consultantGuid;
                        command.Parameters
                           .Add(new SqlParameter("@CompanyName", SqlDbType.NVarChar, 50))
                           .Value = companyName;
                        command.Parameters
                           .Add(new SqlParameter("@FirstName", SqlDbType.NVarChar, 50))
                           .Value = firstName;
                        command.Parameters
                           .Add(new SqlParameter("@LastName", SqlDbType.NVarChar, 50))
                           .Value = lastName;
                        command.Parameters
                           .Add(new SqlParameter("@IsInternal", SqlDbType.Int))
                           .Value = isInteralInt;
                        command.Parameters
                              .Add(new SqlParameter("@ConsultantGuidOutput", SqlDbType.UniqueIdentifier))
                             .Direction = ParameterDirection.Output;
                        connection.Open();
                        var iRet = (int)command.ExecuteNonQuery();
                        consultantGuidOutput = (Guid)command.Parameters["@ConsultantGuidOutput"].Value;

                        connection.Close();
                        
                    }
                }
                Status.StatusMessage = "Consultant Saved";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ContractDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return consultantGuidOutput;
        }

        public bool DeleteConsultant(Guid consultantGuid)
        {

            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ConsultantDelete]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ConsultantGuid", SqlDbType.UniqueIdentifier))
                           .Value = consultantGuid;
                        connection.Open();


                        var iRet = (int)command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Consultant Deleted";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ClientDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return Status.Success;
        }


        public List<Contract> GetContractList()
        {
            var contractList = new List<Contract>();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ContractsGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;


                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var item = new Contract();
                            item.Guid = (Guid)reader["Guid"];
                            item.ClientGuid = (Guid)reader["ClientGuid"];
                            item.ConsultantGuid = (Guid)reader["ConsultantGuid"];
                            item.ClientName = (string)reader["ClientName"];
                            item.CompanyName = (string)reader["CompanyName"];
                            item.FirstName = (string)reader["FirstName"];
                            item.LastName = (string)reader["LastName"];
                            item.StartDate = (DateTime)reader["StartDate"];
                            item.EndDate = (DateTime)reader["EndDate"];
                            item.ClientRate = (decimal)reader["ClientRate"];
                            item.ConsultantRate = (decimal)reader["ConsultantRate"];
                            item.IsProposed = (bool)reader["IsProposed"];
                            item.IsUnderContract = (bool)reader["IsUnderContract"];
                            item.PercentExpected = (int)reader["PercentExpected"];


                            contractList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Contracts Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ContractDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return contractList;
        }
        public Contract GetContract(Guid contractGuid)
        {
            var contract = new Contract();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ContractGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ContractGuid", SqlDbType.UniqueIdentifier))
                           .Value = contractGuid;
                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            contract = new Contract();
                            contract.Guid = (Guid)reader["Guid"];
                            contract.ClientGuid = (Guid)reader["ClientGuid"];
                            contract.ConsultantGuid = (Guid)reader["ConsultantGuid"];
                            contract.ClientName = (string)reader["ClientName"];
                            contract.CompanyName = (string)reader["CompanyName"];
                            contract.FirstName = (string)reader["FirstName"];
                            contract.LastName = (string)reader["LastName"];
                            contract.StartDate = (DateTime)reader["StartDate"];
                            contract.EndDate = (DateTime)reader["EndDate"];
                            contract.ClientRate = (decimal)reader["ClientRate"];
                            contract.ConsultantRate = (decimal)reader["ConsultantRate"];
                            contract.IsProposed = (bool)reader["IsProposed"];
                            contract.IsUnderContract = (bool)reader["IsUnderContract"];
                            contract.PercentExpected = (int)reader["PercentExpected"];



                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Contract Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ContractDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return contract;
        }

        public Guid AddUpdateContract(Contract contract)
        {
            int isProposedInt = 0;
            if (contract.IsProposed == true)
            {
                isProposedInt = 1;
            }
            int isUnderContractInt = 0;
            if (contract.IsUnderContract == true)
            {
                isUnderContractInt = 1;
            }
            Guid contractGuidOutput = new Guid();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ContractAddUpdate]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ContractID", SqlDbType.BigInt))
                           .Value = 0;
                        command.Parameters
                           .Add(new SqlParameter("@ContractGuid", SqlDbType.UniqueIdentifier))
                           .Value = contract.Guid;
                        command.Parameters
                           .Add(new SqlParameter("@ClientGuid", SqlDbType.UniqueIdentifier))
                           .Value = contract.ClientGuid;
                        command.Parameters
                           .Add(new SqlParameter("@ConsultantGuid", SqlDbType.UniqueIdentifier))
                           .Value = contract.ConsultantGuid;
                        command.Parameters
                          .Add(new SqlParameter("@StartDate", SqlDbType.DateTime))
                          .Value = contract.StartDate;
                        command.Parameters
                          .Add(new SqlParameter("@EndDate", SqlDbType.DateTime))
                          .Value = contract.EndDate;
                        command.Parameters
                          .Add(new SqlParameter("@ClientRate", SqlDbType.Decimal, 2))
                          .Value = contract.ClientRate;
                        command.Parameters
                           .Add(new SqlParameter("@ConsultantRate", SqlDbType.Decimal,2))
                           .Value = contract.ConsultantRate;
                        command.Parameters
                           .Add(new SqlParameter("@IsProposed", SqlDbType.Int))
                           .Value = contract.IsProposed;
                        command.Parameters
                          .Add(new SqlParameter("@IsUnderContract", SqlDbType.Int))
                          .Value = contract.IsUnderContract;
                        command.Parameters
                              .Add(new SqlParameter("@ContractGuidOutput", SqlDbType.UniqueIdentifier))
                             .Direction = ParameterDirection.Output;
                        command.Parameters
                         .Add(new SqlParameter("@PercentExpected", SqlDbType.Int))
                         .Value = contract.PercentExpected;
                        connection.Open();
                        var iRet = (int)command.ExecuteNonQuery();
                        contractGuidOutput = (Guid)command.Parameters["@ContractGuidOutput"].Value;

                        connection.Close();

                    }
                }
                Status.StatusMessage = "Contract Saved";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ContractDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return contractGuidOutput;
        }



        public bool DeleteContract(Guid contractGuid)
        {

            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ContractDelete]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ContractGuid", SqlDbType.UniqueIdentifier))
                           .Value = contractGuid;
                        connection.Open();


                        var iRet = (int)command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Contract Deleted";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ClientDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return Status.Success;
        }


    }
}
