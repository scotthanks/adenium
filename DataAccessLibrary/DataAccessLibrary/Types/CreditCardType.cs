﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class CreditCardType
    {
        public Guid Guid { get; set; }
        public string CardDescription { get; set; }
        public string CardType { get; set; }
        public string CardTypeImageUrl { get; set; }
        public bool IsDefault { get; set; }
    }
}