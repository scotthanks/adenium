using System;
using System.Collections.Generic;

namespace DataAccessLibrary
{
    public class OrderSummaryType
    {
        public Guid OrderGuid { get; set; }
        public string OrderNo { get; set; }
        public string ShipWeekString { get; set; }
        public string ProgramCategoryName { get; set; }
        public string ProductFormName { get; set; }
        public int OrderQty { get; set; }

        public List<OrderSummaryType> OrderSummarys { get; set; }
    }



}