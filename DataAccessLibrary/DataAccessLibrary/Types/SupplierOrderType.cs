﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class SupplierOrderType
    {
        public Guid SupplierOrderGuid { get; set; }
        public string SupplierName { get; set; }
        public string SupplierOrderNo { get; set; }
        public IEnumerable<OrderLineType> OrderLines { get; set; }

        public IEnumerable<SelectListItemType> TagRatioList { get; set; }
        public IEnumerable<SelectListItemType> ShipMethodList { get; set; }
    }
}