﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    static public class EpsUrlService
    {
        static public string EpsUrl { get; set; }
        static EpsUrlService()
        {
            var appSettingsReader = new System.Configuration.AppSettingsReader();
            EpsUrl = (string)appSettingsReader.GetValue("DefaultApiBaseUri", typeof(string));
        }

        static public bool SetEpsUrl(string site)
        {
            CookieBox.AuthCookie = null;
            var appSettingsReader = new System.Configuration.AppSettingsReader();
            if (site == "Production")
                EpsUrl = (string)appSettingsReader.GetValue("ProductionApiBaseUri", typeof(string));
            else if (site == "Development")
                EpsUrl = (string)appSettingsReader.GetValue("DevelopmentApiBaseUri", typeof(string));
            else if (site == "Local")
                EpsUrl = (string)appSettingsReader.GetValue("LocalBaseUri", typeof(string));
            else
               return false;

            return true;
        }

    }
}
