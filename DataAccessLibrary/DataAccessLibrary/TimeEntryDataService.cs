﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

using System.Data;
using BusinessObjectsLibrary;


namespace DataAccessLibrary
{
    public class TimeEntryDataService : DataServiceBase
    {
        public TimeEntryDataService(StatusObject status)
            : base(status)
        {}


        public List<ReportedHour> GetReportedHoursList(int workedYear)
        {
            var reportedHourList = new List<ReportedHour>();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ReportedHoursGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                         .Add(new SqlParameter("@WorkedYear", SqlDbType.Int))
                         .Value = workedYear;
                        connection.Open();

                        var reader = command.ExecuteReader();
                        
                        while (reader.Read()){
                            var item = new ReportedHour();
                        
                            item.Guid = (Guid)reader["ReportingPeriodContractHoursGuid"];
                            item.ReportingPeriodGuid = (Guid)reader["ReportingPeriodGuid"];
                            item.ContractGuid = (Guid)reader["ContractGuid"];
                            item.InvoiceStatusGuid = (Guid)reader["InvoiceStatusGuid"];
                            item.ReportingPeriodDisplay = (string)reader["ReportingPeriodDisplay"];
                            item.ContractDisplay = (string)reader["ContratDisplay"];
                            item.Hours = (decimal)reader["Hours"];
                            item.ClientRate = (decimal)reader["ClientRate"];
                            item.ClientDollarsBilled = item.Hours * item.ClientRate;
                            item.ConsultantRate = (decimal)reader["ConsultantRate"];
                            item.ConsultantDollarsPaid = item.Hours * item.ConsultantRate;
                            item.InvoiceStatusDisplay = (string)reader["InvoiceStatusDisplay"];
                            
                            reportedHourList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Reported Hours Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "TimeEntryDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return reportedHourList;
        }

        public ReportedHour GetReportedHour(Guid reportedHourGuid)
        {
            var reportedHour = new ReportedHour();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ReportedHourGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ReportedHourGuid", SqlDbType.UniqueIdentifier))
                           .Value = reportedHourGuid;
                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            reportedHour = new ReportedHour();

                            reportedHour.Guid = (Guid)reader["Guid"];
                            reportedHour.ReportingPeriodGuid = (Guid)reader["ReportingPeriodGuid"];
                            reportedHour.ContractGuid = (Guid)reader["ContractGuid"];
                            reportedHour.InvoiceStatusGuid = (Guid)reader["InvoiceStatusGuid"];
                            reportedHour.ReportingPeriodDisplay = (string)reader["ReportingPeriodDisplay"];
                            reportedHour.ContractDisplay = (string)reader["ContratDisplay"];
                            reportedHour.Hours = (decimal)reader["Hours"];
                            reportedHour.ClientRate = (decimal)reader["ClientRate"];
                            reportedHour.ClientDollarsBilled = reportedHour.Hours * reportedHour.ClientRate;
                            reportedHour.ConsultantRate = (decimal)reader["ConsultantRate"];
                            reportedHour.ConsultantDollarsPaid = reportedHour.Hours * reportedHour.ConsultantRate;
                            reportedHour.InvoiceStatusDisplay = (string)reader["InvoiceStatusDisplay"];
                            reportedHour.WorkedYear = (int)reader["WorkedYear"];

                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Reported Hour Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "TimeEntryDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return reportedHour;
        }
        public Guid AddUpdateReportedHour(ReportedHour reportedHour)
        {
          
            Guid reportedHourGuidOutput = new Guid();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ReportingPeriodContractHoursAddUpdate]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ReportingPeriodContractHoursID", SqlDbType.BigInt))
                           .Value = 0;
                        command.Parameters
                           .Add(new SqlParameter("@ReportingPeriodContractHoursGuid", SqlDbType.UniqueIdentifier))
                           .Value = reportedHour.Guid;
                        command.Parameters
                           .Add(new SqlParameter("@ReportingPeriodGuid", SqlDbType.UniqueIdentifier))
                           .Value = reportedHour.ReportingPeriodGuid;
                        command.Parameters
                           .Add(new SqlParameter("@ContractGuid", SqlDbType.UniqueIdentifier))
                           .Value = reportedHour.ContractGuid;
                        command.Parameters
                           .Add(new SqlParameter("@Hours", SqlDbType.Decimal, 2))
                           .Value = reportedHour.Hours;
                        command.Parameters
                           .Add(new SqlParameter("@InvoiceStatusGuid", SqlDbType.UniqueIdentifier))
                           .Value = reportedHour.InvoiceStatusGuid;
                        
                        command.Parameters
                              .Add(new SqlParameter("@ReportingPeriodContractHoursGuidOutput", SqlDbType.UniqueIdentifier))
                             .Direction = ParameterDirection.Output;
                        connection.Open();

                        var iRet = (int)command.ExecuteNonQuery();
                        reportedHourGuidOutput = (Guid)command.Parameters["@ReportingPeriodContractHoursGuidOutput"].Value;

                        connection.Close();
                        
                    }
                }
                Status.StatusMessage = "Reported Hour Saved";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "TimeEntryDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return reportedHourGuidOutput;
        }

        public bool DeleteReportedHour(Guid reportedHourGuid)
        {

            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ReportedHourDelete]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                           .Add(new SqlParameter("@ReportedHourGuid", SqlDbType.UniqueIdentifier))
                           .Value = reportedHourGuid;
                        connection.Open();


                        var iRet = (int)command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Reported Hour Deleted";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "TimeEntryDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return Status.Success;
        }

        public List<ReportingPeriod> GetReportingPeriodsList(int workedYear)
        {
            var reportingPeriodsList = new List<ReportingPeriod>();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ReportingPeriodsGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                         .Add(new SqlParameter("@WorkedYear", SqlDbType.Int))
                         .Value = workedYear;
                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var item = new ReportingPeriod();

                            item.Guid = (Guid)reader["Guid"];
                            item.StartDate = (DateTime)reader["StartDate"];
                            item.EndDate = (DateTime)reader["EndDate"];
                            item.ProjectedWorkDays = (int)reader["ProjectedWorkDays"];


                            reportingPeriodsList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Reporting Periods Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "TimeEntryDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return reportingPeriodsList;
        }

        public List<ReportingYear> GetReportingYearsList()
        {
            var reportingYearsList = new List<ReportingYear>();
            try
            {
                var connection = GetConnection("Data");
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[ReportingYearsGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                       
                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var item = new ReportingYear();

                            item.Year = (int)reader["Year"];
                            item.IsCurrentYear = (bool)reader["IsCurrentYear"];



                            reportingYearsList.Add(item);
                        }
                        connection.Close();
                    }
                }
                Status.StatusMessage = "Reporting Years Returned";
                Status.Success = true;
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "TimeEntryDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }

            return reportingYearsList;
        }
    }
}
