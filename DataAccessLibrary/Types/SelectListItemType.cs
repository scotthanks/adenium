﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class SelectListItemType
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool IsDefault { get; set; }
    }
}