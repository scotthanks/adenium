using System;

namespace ePS.Types
{
    public class OrderSummaryType
    {
        public Guid OrderGuid { get; set; }
        public string OrderNo { get; set; }
        public string ShipWeekString { get; set; }
        public string ProgramCategoryName { get; set; }
        public string ProductFormName { get; set; }
        public int OrderQty { get; set; }
    }
}