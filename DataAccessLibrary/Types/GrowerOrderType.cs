﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class GrowerOrderType
    {
        public Guid OrderGuid { get; set; }
        public string OrderNo { get; set; }
        public string GrowerName { get; set; }
        public string Description { get; set; }
        public string CustomerPoNo { get; set; }
        public string OrderType { get; set; }
        public string ProductFormCategoryName { get; set; }

        public IEnumerable<AddressType> ShipToAddresses { get; set; }
        public IEnumerable<CreditCardType> CreditCards { get; set; }

        public IEnumerable<SupplierOrderType> SupplierOrders { get; set; }
    }
}