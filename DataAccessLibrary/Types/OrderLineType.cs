﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Types
{
    public class OrderLineType
    {
        public Guid Guid { get; set; }
        public string ProductDescription { get; set; }
        public int Multiple { get; set; }
        public string SpeciesName { get; set; }
        public string VarietyName { get; set; }
        public int QuantityOrdered { get; set; }
        public bool? IsCarted { get; set; }
        public decimal Price { get; set; }
    }
}