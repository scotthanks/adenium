﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace BusinessServiceTestLibrary
{
    [TestClass]
    public class ProductFormServiceTests
    {
        [TestMethod]
        public void BusinessServiceProductFormGetTest()
        {
            var service = new BusinessServiceLibrary.ProductFormService();

            var productForms = service.GetProductForms();

            foreach (var productForm in productForms)
            {
                System.Console.WriteLine( productForm.ToString());
            }

            productForms.Count.Should().BeGreaterThan(30);
        }

        [TestMethod]
        public void BusinessServiceProductFormGetWithSupplierGuidParameterTest()
        {
            var service = new BusinessServiceLibrary.ProductFormService();

            var productForms = service.GetProductForms();
            int fullCount = productForms.Count;

            productForms = service.GetProductForms(supplierGuid: new Guid("a070f7df-a563-453e-a558-0f7524fce705"));
            productForms.Count.Should().BeGreaterThan(2);
            productForms.Count.Should().BeLessThan(fullCount);

            foreach (var productForm in productForms)
            {
                System.Console.WriteLine(productForm.ToString());
            }
        }

        [TestMethod]
        public void BusinessServiceProductFormGetWithProductFormCategoryGuidParameterTest()
        {
            var service = new BusinessServiceLibrary.ProductFormService();

            var productForms = service.GetProductForms();
            int fullCount = productForms.Count;

            productForms = service.GetProductForms(productFormCategoryGuid: new Guid("163c88c8-6071-4eac-ba62-3de1b86061bd"));
            productForms.Count.Should().BeGreaterThan(2);
            productForms.Count.Should().BeLessThan(fullCount);
        }

        [TestMethod]
        public void BusinessServiceProductFormGetWithActiveParameterTest()
        {
            var service = new BusinessServiceLibrary.ProductFormService();

            var productForms = service.GetProductForms();
            int fullCount = productForms.Count;

            productForms = service.GetProductForms(active: null);
            productForms.Count.Should().Be(fullCount);

            productForms = service.GetProductForms(active: true);
            int activeCount = productForms.Count;
            activeCount.Should().BeLessOrEqualTo(fullCount);

            productForms = service.GetProductForms(active: false);
            int inactiveCount = productForms.Count;
            inactiveCount.Should().BeLessThan(fullCount);

            (activeCount + inactiveCount).Should().Be(fullCount);
        }
    }
}
