﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
   
    public class Consultant
    {

        public Consultant()
        {
        }

        public Guid Guid { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsInternal { get; set; }
        public string Display
        {
            get
            {
                return this.CompanyName + " - " + this.FirstName + " " + this.LastName;

            }
        }
    }
}
