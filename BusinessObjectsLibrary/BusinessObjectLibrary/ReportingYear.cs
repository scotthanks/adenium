﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
   
    public class ReportingYear
    {

        public ReportingYear()
        {
        }

        public int Year { get; set; }
        public bool IsCurrentYear { get; set; }
       
       
        

        public string Display { 
             get
            {
                return   this.Year.ToString();
            }
        }

    }
}
