﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
   
    public class Client
    {

        public Client()
        {
        }

        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Display
        {
            get
            {
                return this.Name;
            }
        }
    }
}
