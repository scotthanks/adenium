﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class StatusObject
    {
        public StatusObject(Guid userGuid)
        {
            this.User = new UserObject(userGuid);
            
        }
        public StatusObject(string userName)
        {
            this.User = new UserObject(userName);
            this.NeedToLoadUser = true;
        }
        public UserObject User { get; set; }
       
        public bool Success { get; set; }
     
        public IEnumerable<ErrorObject> Errors { get; set; }
        public string StatusMessage { get; set; }
        public bool NeedToLoadUser { get; set; }
        public bool ForceDevEnvironment { get; set; }
    }
}
