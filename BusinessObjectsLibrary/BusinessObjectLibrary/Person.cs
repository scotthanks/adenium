﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
   
    public class Person
    {

        public Person()
        {
        }
        public Guid Guid { get; set; }
        public long ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Guid GrowerGuid { get; set; }
        public string GrowerName { get; set; }
        public string GrowerType { get; set; }
        public string UserRole { get; set; }
        public bool IsBadGrower { get; set; }
       
      
    }
}
