﻿using System;


namespace BusinessObjectsLibrary
{
   
    public class ReportedHour
    {

        public ReportedHour()
        {
        }

        public Guid Guid { get; set; }
        public Guid ContractGuid { get; set; }
        public Guid ReportingPeriodGuid { get; set; }
        public Guid InvoiceStatusGuid { get; set; }


        public string ContractDisplay { get; set; }
        public string ReportingPeriodDisplay { get; set; }
        public int WorkedYear { get; set; }
        public decimal Hours { get; set; }
        public decimal ConsultantDollarsPaid { get; set; }
        public decimal ConsultantRate { get; set; }
        public decimal ClientDollarsBilled { get; set; }
        public decimal ClientRate { get; set; }
        public string InvoiceStatusDisplay { get; set; }
      
       

        public string Display { 
             get
            {
                return this.ReportingPeriodDisplay + " - " + ContractDisplay;
            }
        }

    }
}
