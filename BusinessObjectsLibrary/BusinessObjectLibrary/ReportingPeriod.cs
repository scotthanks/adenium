﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
   
    public class ReportingPeriod
    {

        public ReportingPeriod()
        {
        }

        public Guid Guid { get; set; }
        public Guid ClientGuid { get; set; }
       
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ProjectedWorkDays { get; set; }
        public int ProjectedWorkHours {
            get {
                return this.ProjectedWorkDays * 8;
            }
        }
        

        public string Display { 
             get
            {
                return   this.StartDate.ToShortDateString() + " - " + this.EndDate.ToShortDateString();
            }
        }

    }
}
