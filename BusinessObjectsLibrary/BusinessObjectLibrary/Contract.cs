﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
   
    public class Contract
    {

        public Contract()
        {
        }

        public Guid Guid { get; set; }
        public Guid ClientGuid { get; set; }
        public Guid ConsultantGuid { get; set; }

        public string ClientName { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal ClientRate { get; set; }
        public decimal ConsultantRate { get; set; }
        public bool IsProposed { get; set; }
        public bool IsUnderContract { get; set; }
        public int PercentExpected { get; set; }

        public string Display { 
             get
            {
                return this.ClientName + " - " + this.CompanyName + " - " + this.FirstName + " - " + this.LastName + " - " +  this.StartDate.ToShortDateString();
            }
        }

    }
}
