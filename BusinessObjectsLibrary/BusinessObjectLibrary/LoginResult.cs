﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    [DataContract]
    public class LoginResult
    {
        [DataMember]
        public bool Success { get; set; }
    }
}
