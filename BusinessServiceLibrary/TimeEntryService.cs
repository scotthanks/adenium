﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using DataAccessLibrary;

namespace BusinessServiceLibrary
{
    public class TimeEntryService : BusinessServiceBase
    {
        public TimeEntryService(StatusObject status)
            : base(status)
        {

        }
        public List<ReportedHour> GetReportedHoursList(int workedYear)
        {

            var service = new TimeEntryDataService(Status);
            var list = service.GetReportedHoursList(workedYear);
            return list;
        }
        public ReportedHour GetReportedHour(Guid reportedHourGuid)
        {

            var service = new TimeEntryDataService(Status);
            var reportedHour = service.GetReportedHour(reportedHourGuid);
            return reportedHour;
        }
        public Guid AddUpdateReportedHour(ReportedHour reportedHour)
        {

            var service = new TimeEntryDataService(Status);
            var reportedHourReturn = service.AddUpdateReportedHour(reportedHour);
            return reportedHourReturn;
        }
        public bool DeleteReportedHour(Guid reportedHourGuid)
        {

            var service = new TimeEntryDataService(Status);
            var bRet = service.DeleteReportedHour(reportedHourGuid);
            return bRet;
        }
        public List<ReportingPeriod> GetReportingPeriodsList(int workedYear)
        {

            var service = new TimeEntryDataService(Status);
            var list = service.GetReportingPeriodsList(workedYear);
            return list;
        }
        public List<ReportingYear> GetReportingYearsList()
        {

            var service = new TimeEntryDataService(Status);
            var list = service.GetReportingYearsList();
            return list;
        }
    }
}
