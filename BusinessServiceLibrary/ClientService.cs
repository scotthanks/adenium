﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using DataAccessLibrary;

namespace BusinessServiceLibrary
{
    public class ClientService : BusinessServiceBase
    {
        public ClientService(StatusObject status)
            : base(status)
        {

        }
        public List<Client> GetClientList()
        {

            var service = new ClientDataService(Status);
            var list = service.GetClientList();
            return list;
        }
        public Client GetClient(Guid clientGuid)
        {

            var service = new ClientDataService(Status);
            var client = service.GetClient(clientGuid);
            return client;
        }
        public Guid AddUpdateClient(Guid clientGuid, string clientName)
        {

            var service = new ClientDataService(Status);
            var client = service.AddUpdateClient(clientGuid, clientName);
            return client;
        }
        public bool DeleteClient(Guid clientGuid)
        {

            var service = new ClientDataService(Status);
            var bRet = service.DeleteClient(clientGuid);
            return bRet;
        }
    }
}
