﻿using System;
using System.Collections.Generic;

using BusinessObjectsLibrary;
using DataAccessLibrary;
using System.Net.Mail;
using System.Net;

namespace BusinessServiceLibrary
{
    public class EmailQueueService : BusinessServiceBase
    {
        public EmailQueueService(StatusObject status)
            : base(status)
        {

        }

        public bool AddErrorLog(string subject, string body, string direction, string growerName, string orderNo)
        {

            var service = new EmailQueueDataService(Status);

            var bRet = service.AddErrorLog(subject, body, direction, growerName, orderNo);

          


            return bRet;
        }
        public bool AddEmailtoQueue(string subject, string body, int delaySendMinutes, string emailType, string emailFrom, string emailTo, string emailCC, string emailBCC, Guid orderGuid, int updateCount, int addCount, int cancelCount)
        {

            var service = new EmailQueueDataService(Status);
            var bRet = service.AddEmail(subject, body, delaySendMinutes, emailType, emailFrom, emailTo, emailCC, emailBCC, orderGuid, updateCount, addCount, cancelCount);
            return bRet;
        }
        public bool UserGetFromCurrentUserName(out Guid userGuid,out String email)
        {
            string theEmail = "";
            Guid theUserGuid = new Guid();
            var service = new EmailQueueDataService(Status);

            var bRet = service.UserGetFromCurrentUserName(out theUserGuid,out theEmail);
            email = theEmail;
            userGuid = theUserGuid;
            return bRet;
        }



        public bool SendMail(string fromEmailAddress, string displayName, string toEmailAddress,
                   string ccEmailAddress, string subject, string emailType, string bodyText,bool ccEPSAdmin, List<Attachment> attachments)
        {
            try
            {
                if (attachments.Count == 0)
                {
                    var emailService = new EmailQueueService(Status);
                    var bRet = emailService.AddEmailtoQueue(subject, bodyText, 0, emailType, fromEmailAddress,
                        toEmailAddress, ccEmailAddress, "", new Guid(), 0, 0, 0);
                    return bRet;
                }
                else
                {
                    var appSettingsReader = new System.Configuration.AppSettingsReader();
                    var softwareOwner = (string)appSettingsReader.GetValue("SoftwareOwner", typeof(string));
                    var sendEmail = (string)appSettingsReader.GetValue("SendingEmail", typeof(string));
                   
                    var bccEmail = (string)appSettingsReader.GetValue("BCCEmail", typeof(string));

                    switch (softwareOwner.ToUpper())
                    {
                        case "GFB":
                            {
                                var httpBody = true;

                                var smtpServerUrl = "mail.green-fuse.com";
                                var smtpServerPort = 26; // or 26 if there is an ISP block
                                var smtpUserName = "confirmations@green-fuse.com"; //username is email address
                                var smtpPassword = "AGreenThumbForAGreenPlant";


                                ////This is for the SES service
                                //smtpServerUrl = "email-smtp.us-east-1.amazonaws.com";
                                //smtpServerPort = "587";
                                //smtpUserName = "AKIAJHBIKFHCZZRXW75A";
                                //smtpPassword = "Ahw30Dg/08/goXsVY5u+v9yk9720jPLtE6GMraPS+dUS";

                                var smtpClient = new SmtpClient(smtpServerUrl, smtpServerPort)
                                {
                                    Credentials = new NetworkCredential(smtpUserName, smtpPassword)
                                };
                                smtpClient.EnableSsl = true;

                                var mailMessage =
                                    new System.Net.Mail.MailMessage
                                    {
                                        From = new MailAddress(fromEmailAddress),
                                        Subject = subject,
                                        Body = bodyText
                                    };


                                mailMessage.To.Add(toEmailAddress);
                                if (ccEmailAddress != "")
                                {
                                    mailMessage.CC.Add(ccEmailAddress);
                                }
                                if (ccEPSAdmin == true)
                                {
                                    mailMessage.Bcc.Add(bccEmail);
                                }
                                mailMessage.Priority = MailPriority.Normal;
                                mailMessage.IsBodyHtml = httpBody;

                                foreach (var attachment in attachments)
                                {
                                    mailMessage.Attachments.Add(attachment);
                                }


                                //SmtpClient client = new SmtpClient();
                                //client.Host = "mail.green-fuse.com";
                                //client.Port = 26; // or 26 if there is an ISP block
                                //client.EnableSsl = true;
                                //client.UseDefaultCredentials = false;
                                //client.Credentials = new System.Net.NetworkCredential("confirmations@green-fuse.com", "AGreenThumbForAGreenPlant");
                                //client.DeliveryMethod = SmtpDeliveryMethod.Network;

                                smtpClient.Send(mailMessage);
                                break;
                            }
                        case "EPS":
                            {

                                MailMessage msg = new MailMessage();
                                msg.From = new MailAddress(fromEmailAddress, displayName);
                                // To addresses
                                msg.To.Add(toEmailAddress);

                                if (ccEmailAddress != "")
                                {
                                    msg.CC.Add(ccEmailAddress);
                                }
                                if (ccEPSAdmin == true)
                                {
                                    msg.Bcc.Add(bccEmail);
                                }


                                msg.Priority = MailPriority.Normal;
                                msg.Subject = subject;

                                msg.Body = bodyText;
                                msg.IsBodyHtml = true;

                                foreach (var attachment in attachments)
                                {
                                    msg.Attachments.Add(attachment);
                                }

                                SmtpClient client = new SmtpClient();
                                client.Host = "smtp.sendgrid.net";
                                client.Port = 587;
                                client.EnableSsl = true;
                                client.UseDefaultCredentials = false;
                                client.Credentials = new System.Net.NetworkCredential("azure_f3a2a5bf2305dd9f34f6543b2877f916@azure.com", "5nelccmx");
                                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                client.Send(msg);
                                break;
                            }
                        case "DYN":
                            {
                                //to do
                                break;
                            }
                        default:
                            {
                                break;
                            }

                    }





                }
                return true;
            }
            
            catch (Exception ex)
            {
                string errmsg = ex.Message;
                return false;
            }
        }
    }
}