﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using DataAccessLibrary;

namespace BusinessServiceLibrary
{
    public class LoginService
    {
        static string authCookie { get; set; }
        public bool Login(string guid, string user, string pwd)
        {
           
            string form = "Guid=" + guid;
            form += "&username=" + user;
            form += "&password=" + pwd;

            string url = EpsUrlService.EpsUrl + "/AdminAuth";
            var webApi = new DataAccessLibrary.WebApi<LoginResult>("AdminAuth");
           

            return true;

        }

        public void PostForm(string url, NameValueCollection form)
        {
            using (WebClient client = new WebClient())
            {
               
                client.Headers.Add(HttpRequestHeader.Cookie, "369");
            
                byte[] response = client.UploadValues(url, form);
            }       
        }

        public static string HttpPost(string URI, string Parameters)
        {
            //http://stackoverflow.com/questions/930807/c-sharp-login-to-website-via-program
            HttpWebRequest httpWebRequest = System.Net.HttpWebRequest.CreateHttp(URI);
 
            httpWebRequest.CookieContainer = new CookieContainer(3);
            httpWebRequest.Method = "POST"; // take out

            const string ACCESS_CODE_VALUE = "369";
            const string ACCESS_COOKIE_NAME = "AccessCookie";
            const double ACCESS_COOKIE_DURATION_HOURS = 36d;
            var accessCookie = new Cookie(ACCESS_COOKIE_NAME, ACCESS_CODE_VALUE);
            accessCookie.Path = "/";
            accessCookie.Expires = DateTime.Now.AddHours(ACCESS_COOKIE_DURATION_HOURS);

            httpWebRequest.CookieContainer.Add(new Uri(URI), accessCookie);

            //Add these, as we're doing a POST
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";
            //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            httpWebRequest.ContentLength = bytes.Length;
            System.IO.Stream os = httpWebRequest.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp = httpWebRequest.GetResponse();
            if (resp == null) 
                return null;
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            authCookie = resp.Headers["Set-cookie"];
            return sr.ReadToEnd().Trim();
        }

    }



}
