﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Web;

using System.Globalization;
namespace BusinessServiceLibrary
{
    public static class UtilityService 
    {
       
       
        public static string GetMountainTime(string utcTime)
        {
            TimeZoneInfo mstZone = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
            DateTime mtnTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(utcTime), mstZone);
            string LogTimeString = mtnTime.ToString("g", CultureInfo.CreateSpecificCulture("en-us"));
            return LogTimeString;
        }

    }
}