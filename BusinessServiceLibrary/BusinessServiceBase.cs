﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;
using DataAccessLibrary;




namespace BusinessServiceLibrary
{
    public abstract class BusinessServiceBase
    {
        public BusinessServiceBase(StatusObject status)
        {

            this.Status = status;
           
            if (Status.User.UserGuid == new Guid() && status.NeedToLoadUser == true)
            {
               status.NeedToLoadUser = false;
               var service = new EmailQueueService(status);
               string email = "";
               Guid userGuid = new Guid();
               var bRet = service.UserGetFromCurrentUserName(out userGuid, out email);
               status.User.UserGuid = userGuid;
               status.User.UserEmail = email;
            }
     
           
        
        
        
        }
        public StatusObject Status { get; set; }

        public bool AddError(ErrorObject error)
        {
            List<ErrorObject> errorList = null;
            if (Status.Errors == null)
            { errorList = new List<ErrorObject>(); }
            else
            { errorList = Status.Errors.ToList(); }
            errorList.Add(error);
            Status.Errors = errorList;
            return true;
           
        }
     
    }
}





