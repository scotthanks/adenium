﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using DataAccessLibrary;
//using Helpers;


namespace BusinessServiceLibrary
    
{
     public class UserProfileService
    {

        public UserProfileService(StatusObject status)
        {
         
            this.Status = status;

            

        }
        
        public StatusObject Status { get; set; }
       
       

        public bool AddError(ErrorObject error)
        {
            List<ErrorObject> errorList = null;
            if (Status.Errors == null)
            { errorList = new List<ErrorObject>(); }
            else
            { errorList = Status.Errors.ToList(); }
            errorList.Add(error);
            Status.Errors = errorList;
            return true;

        }
       

     
       
    }
}
