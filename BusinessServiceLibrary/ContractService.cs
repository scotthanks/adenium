﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using DataAccessLibrary;

namespace BusinessServiceLibrary
{
    public class ContractService : BusinessServiceBase
    {
        public ContractService(StatusObject status)
            : base(status)
        {

        }
        public List<Contract> GetContractList()
        {

            var service = new ContractDataService(Status);
            var list = service.GetContractList();
            return list;
        }
        public Contract GetContract(Guid contractGuid)
        {

            var service = new ContractDataService(Status);
            var contract = service.GetContract(contractGuid);
            return contract;
        }
        public Guid AddUpdateContract(Contract contract)
        {

            var service = new ContractDataService(Status);
            var contractReturn = service.AddUpdateContract(contract);
            return contractReturn;
        }
        public bool DeleteContract(Guid contractGuid)
        {

            var service = new ContractDataService(Status);
            var bRet = service.DeleteContract(contractGuid);
            return bRet;
        }
        public List<Consultant> GetConsultantList()
        {

            var service = new ContractDataService(Status);
            var list = service.GetConsultantList();
            return list;
        }
        public Consultant GetConsultant(Guid consultantGuid)
        {

            var service = new ContractDataService(Status);
            var consultant = service.GetConsultant(consultantGuid);
            return consultant;
        }
        public Guid AddUpdateConsultant(Guid consultantGuid, string companyName, string firstName, string lastName, bool IsInternal)
        {

            var service = new ContractDataService(Status);
            var consultant = service.AddUpdateConsultant(consultantGuid, companyName, firstName, lastName, IsInternal);
            return consultant;
        }
        public bool DeleteConsultant(Guid consultantGuid)
        {

            var service = new ContractDataService(Status);
            var bRet = service.DeleteConsultant(consultantGuid);
            return bRet;
        }
    }
}
