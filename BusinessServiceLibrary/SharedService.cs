﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using DataAccessLibrary;

namespace BusinessServiceLibrary
{
    public class SharedService : BusinessServiceBase
    {
        public SharedService(StatusObject status)
            : base(status)
        {

        }
        public List<ListItem> GetLookupList(string listName)
        {

            var service = new SharedDataService(Status);
            var list = service.GetLookupList(listName);
            return list;
        }
       
    }
}
