﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ClientApi
{


    public class ClientDetailResponse : ResponseBase
    {


        public Client Client { get; set; }

        public ClientDetailResponse(StatusObject status, Guid clientGuid)
           : base(status)
        {
            try
            {



                var service = new ClientService(status);
                 Client = service.GetClient(clientGuid);


                status.StatusMessage = "Client Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
