﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ClientApi
{

    
    public class ClientListResponse : ResponseBase
    {

      
        public List<Client> Clients { get; set; }

        public ClientListResponse(StatusObject status)
            : base(status)
        {
            try
            {
               
               

                var service = new ClientService(status);
                Clients = service.GetClientList();


                status.StatusMessage = "Clients Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }
       

    }
    
 
}