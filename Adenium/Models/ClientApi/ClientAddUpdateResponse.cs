﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ClientApi
{
    public class ClientAddUpdateRequest
    {
       public Guid ClientGuid { get; set; }
        public string ClientName { get; set; }
    }

    public class ClientAddUpdateResponse : ResponseBase
    {

        public Guid ClientGuid { get; set; }


        public ClientAddUpdateResponse(StatusObject status, ClientAddUpdateRequest request)
           : base(status)
        {
            try
            {



                var service = new ClientService(status);
                ClientGuid = service.AddUpdateClient(request.ClientGuid, request.ClientName);


                status.StatusMessage = "Client Saved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
