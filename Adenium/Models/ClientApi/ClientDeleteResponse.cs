﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ClientApi
{
    public class ClientDeleteRequest
    {
        public Guid ClientGuid { get; set; }

    }

    public class ClientDeleteResponse : ResponseBase
    {
       



        public ClientDeleteResponse(StatusObject status, ClientDeleteRequest request)
           : base(status)
        {
            try
            {



                var service = new ClientService(status);
                var bRet = service.DeleteClient(request.ClientGuid);


                status.StatusMessage = "Client Deleted";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
