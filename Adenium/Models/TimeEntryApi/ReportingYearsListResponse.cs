﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.TimeEntryApi
{

    
    public class ReportingYearsListResponse : ResponseBase
    {

      
        public List<ReportingYear> ReportingYears { get; set; }

        public ReportingYearsListResponse(StatusObject status)
            : base(status)
        {
            try
            {
               
               

                var service = new TimeEntryService(status);
                ReportingYears = service.GetReportingYearsList();


                status.StatusMessage = "Reported Years Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }
       

    }
    
 
}