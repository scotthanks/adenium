﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.TimeEntryApi
{
    public class ReportedHourDeleteRequest
    {
        public Guid ReportedHourGuid { get; set; }

    }

    public class ReportedHourDeleteResponse : ResponseBase
    {
       



        public ReportedHourDeleteResponse(StatusObject status, ReportedHourDeleteRequest request)
           : base(status)
        {
            try
            {



                var service = new TimeEntryService(status);
                var bRet = service.DeleteReportedHour(request.ReportedHourGuid);


                status.StatusMessage = "Reported Hour Deleted";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
