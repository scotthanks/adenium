﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.TimeEntryApi
{
    public class ReportedHourAddUpdateRequest
    {
       public Guid ReportedHourGuid { get; set; }
       
        public Guid ReportingPeriodGuid { get; set; }
        public Guid ContractGuid { get; set; }
        public decimal Hours { get; set; }

        public Guid InvoiceStatusGuid { get; set; }
       
    }

    public class ReportedHourAddUpdateResponse : ResponseBase
    {


        public Guid ReportedHourGuid { get; set; }

        public ReportedHourAddUpdateResponse(StatusObject status, ReportedHourAddUpdateRequest request)
           : base(status)
        {
            try
            {
                var reportedHour = new ReportedHour();

                reportedHour.Guid = request.ReportedHourGuid;
                reportedHour.ReportingPeriodGuid = request.ReportingPeriodGuid;
                reportedHour.ContractGuid = request.ContractGuid;
                reportedHour.Hours = request.Hours;
                reportedHour.InvoiceStatusGuid = request.InvoiceStatusGuid;
                
               
               
              

                var service = new TimeEntryService(status);
                ReportedHourGuid = service.AddUpdateReportedHour(reportedHour);


                status.StatusMessage = "Reported Hour Saved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
