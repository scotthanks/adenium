﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.TimeEntryApi
{


    public class ReportedHourDetailResponse : ResponseBase
    {


        public ReportedHour ReportedHour { get; set; }

        public ReportedHourDetailResponse(StatusObject status, Guid reportedHourGuid)
           : base(status)
        {
            try
            {



                var service = new TimeEntryService(status);
                ReportedHour = service.GetReportedHour(reportedHourGuid);


                status.StatusMessage = "Reported Hour Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
