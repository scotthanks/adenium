﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.TimeEntryApi
{

    
    public class ReportingPeriodsListResponse : ResponseBase
    {

      
        public List<ReportingPeriod> ReportingPeriods { get; set; }

        public ReportingPeriodsListResponse(StatusObject status, int workedYear)
            : base(status)
        {
            try
            {
               
               

                var service = new TimeEntryService(status);
                ReportingPeriods = service.GetReportingPeriodsList(workedYear);


                status.StatusMessage = "ReportedHours Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }
       

    }
    
 
}