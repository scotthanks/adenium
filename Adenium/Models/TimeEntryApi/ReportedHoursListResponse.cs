﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.TimeEntryApi
{

    
    public class ReportedHoursListResponse : ResponseBase
    {

      
        public List<ReportedHour> ReportedHours { get; set; }

        public ReportedHoursListResponse(StatusObject status, int workedYear)
            : base(status)
        {
            try
            {
               
               

                var service = new TimeEntryService(status);
                ReportedHours = service.GetReportedHoursList(workedYear);


                status.StatusMessage = "ReportedHours Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }
       

    }
    
 
}