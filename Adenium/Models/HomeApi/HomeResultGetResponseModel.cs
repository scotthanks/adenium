﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;


namespace Adenium.Models.HomeApi
{


    public class HomeResultGetResponseModel : ResponseBase
    {

    

        public HomeResultGetResponseModel(StatusObject status)
            : base(status)
        {
            try
            {
                var users = -1;
                var activeUsers = 1;
                var linesOfCredit = -1;
                var searches = -1;
                //var service = new DashboardService(status);
                //Status.Success = service.GetHomeData(out users, out activeUsers, out linesOfCredit, out searches);
                Users = users;
                ActiveUsers = activeUsers;
                LinesOfCredit = linesOfCredit;
                Searches = searches;
                
                status.StatusMessage = "Home Data Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }
        public int Users { get; set; }
        public int ActiveUsers { get; set; }
        public int LinesOfCredit { get; set; }
        public int Searches { get; set; }
     
       
       

    }
}