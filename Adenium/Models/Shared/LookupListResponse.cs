﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.SharedApi
{

    
    public class LookupListResponse : ResponseBase
    {

      
        public List<ListItem> ListItems { get; set; }

        public LookupListResponse(StatusObject status, string listName)
            : base(status)
        {
            try
            {
               
               

                var service = new SharedService(status);
                ListItems = service.GetLookupList(listName);


                status.StatusMessage = "List Items Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }
       

    }
    
 
}