﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ContractApi
{
    public class ConsultantAddUpdateRequest
    {
       public Guid ConsultantGuid { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IsInternal { get; set; }
    }

    public class ConsultantAddUpdateResponse : ResponseBase
    {


        public Guid ConsultantGuid { get; set; }

        public ConsultantAddUpdateResponse(StatusObject status, ConsultantAddUpdateRequest request)
           : base(status)
        {
            try
            {
                var isInteral = false;
                if (request.IsInternal == "true")
                {
                    isInteral = true;
                }


                var service = new ContractService(status);
                ConsultantGuid = service.AddUpdateConsultant(request.ConsultantGuid, request.CompanyName,request.FirstName,request.LastName, isInteral);


                status.StatusMessage = "Consultant Saved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
