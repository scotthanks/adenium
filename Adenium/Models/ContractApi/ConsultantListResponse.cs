﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ContractApi
{

    
    public class ConsultantListResponse : ResponseBase
    {

      
        public List<Consultant> Consultants { get; set; }

        public ConsultantListResponse(StatusObject status)
            : base(status)
        {
            try
            {
               
               

                var service = new ContractService(status);
                Consultants = service.GetConsultantList();


                status.StatusMessage = "Consultants Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }
       

    }
    
 
}