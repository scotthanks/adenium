﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ContractApi
{
    public class ContractAddUpdateRequest
    {
       public Guid ContractGuid { get; set; }
       
        public Guid ClientGuid { get; set; }
        public Guid ConsultantGuid { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal ClientRate { get; set; }
        public decimal ConsultantRate { get; set; }
        public string IsProposed { get; set; }
        public string IsUnderContract { get; set; }
        public int PercentExpected { get; set; }
    }

    public class ContractAddUpdateResponse : ResponseBase
    {


        public Guid ContractGuid { get; set; }

        public ContractAddUpdateResponse(StatusObject status, ContractAddUpdateRequest request)
           : base(status)
        {
            try
            {
                var contract = new Contract();

                contract.Guid = request.ContractGuid;
                contract.ClientGuid = request.ClientGuid;
                contract.ClientRate = request.ClientRate;
                contract.ConsultantGuid = request.ConsultantGuid;
                contract.ConsultantRate = request.ConsultantRate;
                contract.StartDate = request.StartDate;
                contract.EndDate = request.EndDate;
                contract.PercentExpected = request.PercentExpected;
                contract.IsProposed = false;
                if (request.IsProposed == "true")
                {
                    contract.IsProposed = true;
                }

                contract.IsUnderContract = false;
                if (request.IsUnderContract == "true")
                {
                    contract.IsUnderContract = true;
                }

                var service = new ContractService(status);
                ContractGuid = service.AddUpdateContract( contract);


                status.StatusMessage = "Contract Saved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
