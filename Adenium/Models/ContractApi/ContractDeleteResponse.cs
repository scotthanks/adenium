﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ContractApi
{
    public class ContractDeleteRequest
    {
        public Guid ContractGuid { get; set; }

    }

    public class ContractDeleteResponse : ResponseBase
    {
       



        public ContractDeleteResponse(StatusObject status, ContractDeleteRequest request)
           : base(status)
        {
            try
            {



                var service = new ContractService(status);
                var bRet = service.DeleteContract(request.ContractGuid);


                status.StatusMessage = "Contract Deleted";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
