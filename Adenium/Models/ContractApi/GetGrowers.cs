﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web;

using BusinessObjectsLibrary;
using BusinessServiceLibrary;
using Newtonsoft.Json;

namespace ePSAdmin.Models.CustomerServiceApi
{

   
    public class GetGrowers :ResponseBase
    {
        public GetGrowers(StatusObject status, string likeParam)
            : base(status)
        {
            List<Grower> list = new List<Grower>();

           
            try
            {
                var service = new GrowerService(status);
                list = service.GetGrowers(likeParam);
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "Growers",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                this.Status.StatusMessage = "Errors Occurred";

                Status.Success = false;
            }
            finally {
                Growers = list;
            }
        }

        public List<Grower> Growers { get; set; }
       

    }

}
