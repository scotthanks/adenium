﻿using BusinessServiceLibrary;
using BusinessObjectsLibrary;
using AllAdmin.Controllers.api;
using System;


namespace AllAdmin.Models.OrderDetailApi
{
 
   


    public class CombineOrderResponseModel : AllAdmin.Models.ResponseBase
    {
        public CombineOrderResponseModel(StatusObject status, CombineOrder request)
             : base(status)
        {
           
            try
            {
            
               
                var service = new GrowerOrderService(status);
                var bRet = service.GrowerOrderCombine(request.OrderNoToKeep,request.OrderNoToDelete);


                Response = string.Format(status.StatusMessage);
            }
            catch (Exception ex)
            {
                Response = string.Format("Order line cancel failed: {0}", ex.Message);
            }

        }

        public string Response { get; set; }
    }
}