﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ContractApi
{
    public class ConsultantDeleteRequest
    {
        public Guid ConsultantGuid { get; set; }

    }

    public class ConsultantDeleteResponse : ResponseBase
    {
       



        public ConsultantDeleteResponse(StatusObject status, ConsultantDeleteRequest request)
           : base(status)
        {
            try
            {



                var service = new ContractService(status);
                var bRet = service.DeleteConsultant(request.ConsultantGuid);


                status.StatusMessage = "Consultant Deleted";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
