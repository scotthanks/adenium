﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ContractApi
{


    public class ContractDetailResponse : ResponseBase
    {


        public Contract Contract { get; set; }

        public ContractDetailResponse(StatusObject status, Guid contractGuid)
           : base(status)
        {
            try
            {



                var service = new ContractService(status);
                Contract = service.GetContract(contractGuid);


                status.StatusMessage = "Contract Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
