﻿using System;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ContractApi
{


    public class ConsultantDetailResponse : ResponseBase
    {


        public Consultant Consultant { get; set; }

        public ConsultantDetailResponse(StatusObject status, Guid consultantGuid)
           : base(status)
        {
            try
            {



                var service = new ContractService(status);
                Consultant = service.GetConsultant(consultantGuid);


                status.StatusMessage = "Consultant Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }


    }

}
