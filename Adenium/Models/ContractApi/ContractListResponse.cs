﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;

namespace Adenium.Models.ContractApi
{

    
    public class ContractListResponse : ResponseBase
    {

      
        public List<Contract> Contracts { get; set; }

        public ContractListResponse(StatusObject status)
            : base(status)
        {
            try
            {
               
               

                var service = new ContractService(status);
                Contracts = service.GetContractList();


                status.StatusMessage = "Contracts Retrieved";
                status.Success = true;
            }
            catch (Exception ex)
            {
                status.StatusMessage = ex.Message;
                status.Success = false;
            }

        }
       

    }
    
 
}