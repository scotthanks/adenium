﻿using System.Web.Mvc;

namespace Adenium.Controllers
{
    [Authorize]
    public class DashboardsController : Controller
    {

        public ActionResult DummenDashboard()
        {
            return View();
        }

        public ActionResult EPSDashboard()
        {
            return View();
        }
        public ActionResult DynamicPlantsDashboard()
        {
            return View();
        }
        
        public ActionResult GreenFuseDashboard()
        {
            return View();
        }
        public ActionResult DashboardOrders()
        {
            return View();
        }
        public ActionResult DashboardOrderLines()
        {
            return View();
        }
       
       

    }
}
