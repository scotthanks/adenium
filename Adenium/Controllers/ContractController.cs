﻿
using System.Web.Mvc;

namespace Adenium.Controllers
{
    [Authorize]
    public class ContractController : Controller
    {
        //
        // GET: /Accounting/

        public ActionResult ConsultantList()
        {
            return View();
        }
        public ActionResult ConsultantDetail()
        {
            return View();
        }
        public ActionResult ContractList()
        {
            return View();
        }
        public ActionResult ContractDetail()
        {
            return View();
        }
    }
}
