﻿using System.Web.Http;
using AttributeRouting.Web.Http;
using AllAdmin.Models.HomeApi;
using BusinessObjectsLibrary;




namespace AllAdmin.Controllers
{
    [Authorize]
    public class APIHomeController : ApiController
    {
  
        [HttpGet]
        [Route("Home")]
        public HomeResultGetResponseModel HomeResultGet()
             
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);
            var model = new HomeResultGetResponseModel(status);
            return model;

        }
     
    }
}
