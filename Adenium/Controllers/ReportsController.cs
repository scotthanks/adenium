﻿
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Adenium.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private string reportServer = "";
        private Uri reportServerURI;
        private string SSRSUserName = "";
        private string SSRSPassword = "";
        private string SSRSDomain = "";
        private string reportPath = "";

        private bool SetSSRSParamters()
        {
            var appSettingsReader = new System.Configuration.AppSettingsReader();
            reportServer = (string)appSettingsReader.GetValue("SSRSReportServer", typeof(string));
            reportServerURI = new Uri(reportServer);
            reportPath = (string)appSettingsReader.GetValue("SSRSReportPath", typeof(string));
            SSRSUserName = (string)appSettingsReader.GetValue("SSRSUserName", typeof(string));
            SSRSPassword = (string)appSettingsReader.GetValue("SSRSPassword", typeof(string));
            SSRSDomain = (string)appSettingsReader.GetValue("SSRSDomain", typeof(string));
            return true;
        }

        private ReportViewer reportViewer = new ReportViewer()
        {
            ProcessingMode = ProcessingMode.Remote,
            SizeToReportContent = true,
            Width = Unit.Pixel(1094),
            Height = Unit.Pixel(300),
            AsyncRendering = false,
            ShowParameterPrompts = false,
            ZoomMode = ZoomMode.FullPage,
            BackColor = System.Drawing.Color.White
        };



      
        public ActionResult OrderReport()
        {
            {
         
                return View();
            }
           
        }
        public ActionResult BookingsReport()
        {
            {

                return View();
            }

        }
        public ActionResult GreenFuseBreederReport()
        {
            {

                return View();
            }

        }
        public ActionResult YearOverYearReport()
        {
            {
                return View();
            }

        }

      

        //[HttpPost]
        //[ActionName("RunYearOverYearReport")]
        //public PartialViewResult RunYearOverYearReport(
        //string cboGrowerName,
        //string cboSupplierName,
        //string cboBrokerName
        // )
        //{
        //    string reportName = "Year Over Year Report";
        //    bool bRet = SetSSRSParamters();

        //    //Set up Viewer
        //    reportViewer.ServerReport.ReportPath = reportPath + reportName;
        //    reportViewer.ServerReport.ReportServerUrl = reportServerURI;

        //    //Set up Credentials

        //    IReportServerCredentials irsc = new CustomReportCredentials(SSRSUserName, SSRSPassword, SSRSDomain);
        //    // var irsc = new CustomReportCredentials(SSRSUserName, SSRSPassword, SSRSDomain);
        //    reportViewer.ServerReport.ReportServerCredentials = irsc;

        //    //Set up Parameters
        //    List<ReportParameter> reportParameters = new List<ReportParameter>();
        //    reportParameters.Add(new ReportParameter("pGrowerGuid", cboGrowerName));
        //    reportParameters.Add(new ReportParameter("pSupplierGuid", cboSupplierName));
        //    reportParameters.Add(new ReportParameter("pBrokerGuid", cboBrokerName));


        //    reportViewer.ServerReport.SetParameters(reportParameters);

        //    //Run the report
        //    reportViewer.ServerReport.Refresh();

        //    //Return Viewer
        //    ViewBag.ReportViewer = reportViewer;
        //    ViewBag.ReportToShow = true;
        //    return PartialView("~/Views/Shared/_ReportViewer.cshtml");
        //}

        public ActionResult OpenInvoiceReport()
        {
            {
                return View();
            }

        }

        
       
        public ActionResult B2BAvailability()
        {
            {

                return View();
            }

        }
     
        public ActionResult DummenAvailGap()
        {
            {

                return View();
            }

        }
        public ActionResult Searches()
        {
            {

                return View();
            }

        }
        public ActionResult SupplierReport()
        {
            {

                return View();
            }

        }
        public ActionResult B2BErrorListReport()
        {
            {

                return View();
            }

        }
        public ActionResult ProductFormSalesQtyQAReport()
        {
            {

                return View();
            }

        }

        public ActionResult GrowerReport()
        {
            {

                return View();
            }

        }
        public ActionResult GrowerProductListReport()
        {
            {

                return View();
            }

        }
        public ActionResult DummenQuoteSummary()
        {
            {

                return View();
            }
        }
        public ActionResult DummenZeroPrice()
        {
            {

                return View();
            }

        }    
           public ActionResult GeneticOwnerURC()
        {
            {

                return View();
            }

        }
        

       
        


    }
    
}

