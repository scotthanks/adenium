﻿
using System.Web.Mvc;

namespace Adenium.Controllers
{
    [Authorize]
    public class TimeEntryController : Controller
    {
        //
        // GET: /Accounting/

        public ActionResult ReportedHoursList()
        {
            return View();
        }
        public ActionResult ReportedHourDetail()
        {
            return View();
        }
    }
}
