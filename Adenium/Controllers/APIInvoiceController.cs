﻿using System.Web.Http;
using AttributeRouting.Web.Http;
using AllAdmin.Models.AccountingApi;
using BusinessObjectsLibrary;
using System;
//using System.Web.Mvc;

namespace AllAdmin.Controllers.api
{


    public class APIAccountingController : ApiController
    {
        [HttpGet]
        [Route("GetBillings")]
        public GetBillingResponseModel GetBillings([FromUri] GetBillingRequestModel request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            // request.Status = status;
            var response = new GetBillingResponseModel(status, request);
            return response;
        }
        [HttpGet]
        [Route("InvoiceDetail")]
        public GetInvoiceDetail InvoiceDetail([FromUri] string InvoiceNo)
        {

            var userName = User.Identity.Name;
            var status = new StatusObject(userName);


            var response = new GetInvoiceDetail(status, InvoiceNo);
            return response;

        }
    }






    //[GET("api/Invoice/PaymentsForInvoice"), System.Web.Http.HttpGet]
    //public GetPaymentsForInvoice GetPaymentsForInvoice([FromUri] string InvoiceNo)
    //{

    //    var userName = User.Identity.Name;
    //    var status = new StatusObject(userName);


    //    var response = new GetPaymentsForInvoice(status, InvoiceNo);
    //    return response;

    //}



    //[GET("api/Invoice/Invoices"), System.Web.Http.HttpGet]
    //public GetInvoicesResponseModel GetInvoices([FromUri] string GrowerGuid, string BrokerGuid, string InvoiceStatus, string DaysBack)
    //{

    //    var userName = User.Identity.Name;
    //    var status = new StatusObject(userName);
    //    var growerGuid = new Guid();
    //    if (GrowerGuid == "" || GrowerGuid == null)
    //    {
    //        growerGuid = Guid.Empty;
    //    }

    //    else
    //    {
    //        growerGuid = new Guid(GrowerGuid);
    //    }
    //    var brokerGuid = new Guid();
    //    brokerGuid = Guid.Empty;
    //    if (BrokerGuid == "" || BrokerGuid == null)
    //    {
    //        brokerGuid = Guid.Empty;
    //    }

    //    else
    //    {
    //        brokerGuid = new Guid(BrokerGuid);
    //    }
    //    var onlyOpen = false;
    //    int daysBack = Int32.Parse(DaysBack);
    //    if (InvoiceStatus == "OPEN" && daysBack == 0)
    //    {
    //        onlyOpen = true;
    //    }
    //    var response = new GetInvoicesResponseModel(status, onlyOpen, growerGuid, brokerGuid, daysBack);
    //    return response;

    //}

    //[GET("api/Invoice/GrowerAccounts"), System.Web.Http.HttpGet]
    //public GetGrowerAccounts GetGrowerAccounts([FromUri] string GrowerLike)
    //{

    //    var userName = User.Identity.Name;
    //    var status = new StatusObject(userName);
    //    var growerLike = "";
    //    if (GrowerLike == "" || GrowerLike == null)
    //    {
    //        growerLike = "";
    //    }

    //    else
    //    {
    //        growerLike = GrowerLike;
    //    }


    //    var response = new GetGrowerAccounts(status, growerLike);
    //    return response;

    //}
    //[GET("api/Invoice/BrokerAccounts"), System.Web.Http.HttpGet]
    //public GetBrokerAccounts GetBrokerAccounts([FromUri] string BrokerGuid)
    //{

    //    var userName = User.Identity.Name;
    //    var status = new StatusObject(userName);
    //    var brokerGuid = new Guid();
    //    if (BrokerGuid == "" || BrokerGuid == null)
    //    {
    //        brokerGuid = Guid.Empty;
    //    }

    //    else
    //    {
    //        brokerGuid = new Guid(BrokerGuid);
    //    }


    //    var response = new GetBrokerAccounts(status, brokerGuid);
    //    return response;

    //}
    //[GET("api/Invoice/InvoicePayments"), System.Web.Http.HttpGet]
    //public GetInvoicePayments InvoicePayments([FromUri] string GrowerGuid, string BrokerGuid)
    //{
    //    var userName = User.Identity.Name;
    //    var status = new StatusObject(userName);
    //    var growerGuid = new Guid();
    //    if (GrowerGuid == "" || GrowerGuid == null)
    //    {
    //        growerGuid = Guid.Empty;
    //    }

    //    else
    //    {
    //        growerGuid = new Guid(GrowerGuid);
    //    }
    //    var brokerGuid = new Guid();
    //    if (BrokerGuid == "" || BrokerGuid == null)
    //    {
    //        brokerGuid = Guid.Empty;
    //    }

    //    else
    //    {
    //        brokerGuid = new Guid(BrokerGuid);
    //    }

    //    var model = new GetInvoicePayments(status, growerGuid, brokerGuid);
    //    return model;
    //}

    //[GET("api/Invoice/PaymentsWithBalances"), System.Web.Http.HttpGet]
    //public GetPaymentsWithBalances PaymentsWithBalances([FromUri] string GrowerGuid, string BrokerGuid, string OnlyOpen)
    //{
    //    var userName = User.Identity.Name;
    //    var status = new StatusObject(userName);
    //    var growerGuid = new Guid();
    //    if (GrowerGuid == "" || GrowerGuid == null)
    //    {
    //        growerGuid = Guid.Empty;
    //    }

    //    else
    //    {
    //        growerGuid = new Guid(GrowerGuid);
    //    }
    //    var brokerGuid = new Guid();
    //    if (BrokerGuid == "" || BrokerGuid == null)
    //    {
    //        brokerGuid = Guid.Empty;
    //    }

    //    else
    //    {
    //        brokerGuid = new Guid(BrokerGuid);
    //    }
    //    bool onlyOpen = true;
    //    if (OnlyOpen == "FALSE")
    //    {
    //        onlyOpen = false;
    //    }
    //    var model = new GetPaymentsWithBalances(status, growerGuid, brokerGuid, onlyOpen);
    //    return model;
    //}
    //[GET("api/Invoice/Payments"), System.Web.Http.HttpGet]
    //public GetPaymentsResponseModel Payments([FromUri] CheckPaymentsRequest request)
    //{
    //    var userName = User.Identity.Name;
    //    var status = new StatusObject(userName);
    //    var model = new GetPaymentsResponseModel(status, request);
    //    return model;
    //}




    //[GET("api/Invoice/AccountBalance"), System.Web.Http.HttpGet]
    //public AccountBalanceResponse AccountBalance([FromUri] GLABalanceRequest request)
    //{
    //    var model = new AccountBalanceRequestModel(request);
    //    return model.Response;
    //}


    //[GET("api/Invoice/SupplierList"), System.Web.Http.HttpGet]
    //public GetSuppliersResponseModel Suppliers()
    //{
    //    var status = new StatusObject(new Guid());

    //    return new GetSuppliersResponseModel(status);

    //}

    //[GET("api/Invoice/CreditOrderLines"), System.Web.Http.HttpGet]
    //public GetCreditOrderLines CreditOrderLines([FromUri]string OrderGuid)
    //{
    //    var status = new StatusObject(new Guid());
    //    var x = new GetCreditOrderLines(status, OrderGuid);
    //    return x;
    //}
    //[GET("api/Invoice/CreditReasons"), System.Web.Http.HttpGet]
    //public GetCreditReasons CreditReasons()
    //{
    //    var status = new StatusObject(new Guid());
    //    var x = new GetCreditReasons(status);
    //    return x;
    //}
    //[GET("api/Invoice/SupplierOrderList"), System.Web.Http.HttpGet]
    //public GetSupplierOrdersResponseModel SupplierOrders([FromUri]Guid SupplierGuid)
    //{
    //    var status = new StatusObject(new Guid());
    //    var x = new GetSupplierOrdersResponseModel(status, SupplierGuid);
    //    return x;
    //}

    //[PUT("/api/Invoice/UpdateSupplierOrder2"), System.Web.Http.HttpPut]
    //public UpdateSupplierOrderModel2 UpdateSupplierOrder2([FromBody] SupplierOrderUpdateRequest request)
    //{
    //    var status = new StatusObject(new Guid());
    //    var model = new UpdateSupplierOrderModel2(status, request);
    //    return model;
    //}


    //[PUT("api/Invoice/Bill2"), System.Web.Http.HttpPut]
    //public CreateInvoiceResponseModel BillingInvoice([FromBody] BillingResponseItem request)
    //{

    //    // to do - Get User Guid from front end
    //    var status = new StatusObject(new Guid());
    //    var appSettingsReader = new System.Configuration.AppSettingsReader();
    //    string softwareOwner = (string)appSettingsReader.GetValue("SoftwareOwner", typeof(string));
    //    var model = new CreateInvoiceResponseModel(status, request, softwareOwner);
    //    if (!model.Status.Success)
    //    {
    //        throw new Exception("api/Invoice/Bill2 call failed");
    //    }

    //    return model;


    //}

    //[POST("/api/Invoice/SaveCreditLine"), System.Web.Http.HttpPost]
    //public SaveCreditLineResponseModel SaveCreditLine([FromBody] SaveCreditLineRequest request)
    //{
    //    try
    //    {
    //        var userName = User.Identity.Name;
    //        var status = new StatusObject(userName);
    //        var model = new SaveCreditLineResponseModel(status, request);


    //        return model;

    //    }
    //    catch (Exception ex)
    //    {
    //        var x = ex;
    //        return null;
    //    }
    //}
    //[POST("/api/Invoice/SendCreditMemo"), System.Web.Http.HttpPost]
    //public SendCreditMemo SendCreditMemo([FromBody] SendCreditMemoRequest request)
    //{
    //    try
    //    {
    //        var userName = User.Identity.Name;
    //        var status = new StatusObject(userName);
    //        var appSettingsReader = new System.Configuration.AppSettingsReader();
    //        string softwareOwner = (string)appSettingsReader.GetValue("SoftwareOwner", typeof(string));
    //        var model = new SendCreditMemo(status, request, softwareOwner);


    //        return model;

    //    }
    //    catch (Exception ex)
    //    {
    //        var x = ex;
    //        return null;
    //    }
    //}
    //[POST("/api/Invoice/EditPayment"), System.Web.Http.HttpPost]
    //public EditPaymentResponseModel EditPayment([FromBody] EditTransactionRequest request)
    //{
    //    try
    //    {
    //        var userName = User.Identity.Name;
    //        var status = new StatusObject(userName);
    //        var model = new EditPaymentResponseModel(status, request);


    //        return model;

    //    }
    //    catch (Exception ex)
    //    {
    //        var x = ex;
    //        return null;
    //    }
    //}
    //[POST("/api/Invoice/DeleteInvoice"), System.Web.Http.HttpPost]
    //public DeleteInvoiceResponseModel DeleteInvoice([FromBody] DeleteInvoiceRequest request)
    //{
    //    try
    //    {
    //        var userName = User.Identity.Name;
    //        var status = new StatusObject(userName);
    //        var model = new DeleteInvoiceResponseModel(status, request);


    //        return model;

    //    }
    //    catch (Exception ex)
    //    {
    //        var x = ex;
    //        return null;
    //    }
    //}
    //[POST("/api/Invoice/DeletePaymentOrCredit"), System.Web.Http.HttpPost]
    //public DeletePaymentOrCreditResponseModel DeletePaymentOrCredit([FromBody] DeletePaymentOrCreditRequest request)
    //{
    //    try
    //    {
    //        var userName = User.Identity.Name;
    //        var status = new StatusObject(userName);
    //        var transactionGuid = new Guid(request.TransactionGuid);
    //        var model = new DeletePaymentOrCreditResponseModel(status, transactionGuid, request.Type);


    //        return model;

    //    }
    //    catch (Exception ex)
    //    {
    //        var x = ex;
    //        return null;
    //    }
    //}

    //[POST("/api/Invoice/CreatePayment"), System.Web.Http.HttpPost]
    //public CreatePaymentResponseModel CreatePayment([FromBody] CreateTransactionRequest request)
    //{
    //    try
    //    {
    //        var userName = User.Identity.Name;
    //        var status = new StatusObject(userName);
    //        var model = new CreatePaymentResponseModel(status, request);

    //        status.StatusMessage = "Payment Created.";
    //        return model;

    //    }
    //    catch (Exception ex)
    //    {
    //        var x = ex;
    //        return null;
    //    }
    //}
    //[POST("/api/Invoice/DeletePaymentForInvoice"), System.Web.Http.HttpPost]
    //public DeletePaymentForInvoiceResponseModel DeletePaymentForInvoice([FromBody] DeleteInvoicePaymentRequest request)
    //{
    //    try
    //    {
    //        var userName = User.Identity.Name;
    //        var status = new StatusObject(userName);
    //        var theGuid = new Guid(request.TransactionGuid);
    //        var model = new DeletePaymentForInvoiceResponseModel(status, theGuid);


    //        return model;

    //    }
    //    catch (Exception ex)
    //    {
    //        var x = ex;
    //        return null;
    //    }
    //}

    //}
}
