﻿
using Adenium.Filters;
using System.Web.Mvc;

namespace Adenium.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            var appSettingsReader = new System.Configuration.AppSettingsReader();
            Session["SoftwareOwner"] = (string)appSettingsReader.GetValue("SoftwareOwner", typeof(string));
            var x = Session["SoftwareOwner"].ToString();
            ActionResult theReturn;

            switch (x.ToUpper()){
                case "GFB":
                    {
                        theReturn =  View("IndexGreenFuse");
                        break;
                    }
                case "DYN":
                    {
                        theReturn = View("IndexDynamicPlants");
                        break;
                    }
                case "EPSDEMO":
                    {
                        theReturn =  View("IndexDemo");
                        break;
                    }
                case "EPS":
                    {
                        theReturn = View("Index");
                        break;
                    }
                default:
                    {
                        theReturn =  View("Index");
                        break;
                    }
            }
            return theReturn;


        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        public ActionResult IndexDemo()
        {
            ViewBag.Message = "Demo home Page";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
    }
}
