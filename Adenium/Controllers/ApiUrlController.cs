﻿using DataAccessLibrary;
using System.Web.Mvc;

namespace AllAdmin.Controllers
{
    public class RequestModel
    {
        public string site { get; set; }
    }

    public class ApiUrlController : Controller
    {
        public string Get()
        {
          
            return EpsUrlService.EpsUrl;
        }

        public ActionResult SetEpsUrl()
        {
            return View();
        }
        
        public string Roles()
        {
            string[] roleNames = System.Web.Security.Roles.GetRolesForUser();
            if (roleNames.Length > 0)
                return string.Format("   role: {0}", roleNames[0]);
            return "";
        }
    }
}
