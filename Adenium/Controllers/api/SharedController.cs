﻿
using Adenium.Models.SharedApi;
using BusinessObjectsLibrary;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Adenium.Controllers.SharedApi
{
    [RoutePrefix("api/Shared")]
    public class SharedController : ApiController
    {
        [Route("LookupList"), ActionName("LookupList"), HttpGet]
        public LookupListResponse LookupList(string listName)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new LookupListResponse(status, listName);
            return model;

        }
      
    }

}

