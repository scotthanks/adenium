﻿using Adenium.Models.HomeApi;
using BusinessObjectsLibrary;
using System.Web.Http;

namespace Adenium.Controllers.API
{
    [Authorize]
    [RoutePrefix("api")]
    public class HomeController : ApiController
    {
        [Route("Home"), ActionName("Home"), HttpGet]
        public HomeResultGetResponseModel HomeResultGet()
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);
            var model = new HomeResultGetResponseModel(status);
            return model;
        }
    }
}
