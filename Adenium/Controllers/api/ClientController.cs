﻿
using Adenium.Models.ClientApi;
using BusinessObjectsLibrary;
using System.Web.Http;
using System;


namespace Adenium.Controllers.API
{
    [RoutePrefix("api/Client")]
    public class ClientAPIController : ApiController
    {
     
        [Route("ClientList"), ActionName("ClientList"), HttpGet]
        public ClientListResponse ClientList()
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);
            
            var model = new ClientListResponse(status);
            return model;

        }
        [Route("ClientDetail"), ActionName("ClientDetail"), HttpGet]
        public ClientDetailResponse ClientGet(Guid ClientGuid)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);
          
            var model = new ClientDetailResponse(status, ClientGuid);
            return model;

        }
        [Route("ClientAddUpdate"), ActionName("ClientAddUpdate"), HttpPut]
        public ClientAddUpdateResponse ClientPut([FromBody] ClientAddUpdateRequest request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);
           
            var model = new ClientAddUpdateResponse(status, request);
            return model;

        }
        [Route("ClientDelete"), ActionName("ClientDelete"), HttpDelete]
        public ClientDeleteResponse ClientDelete(ClientDeleteRequest request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ClientDeleteResponse(status, request);
            return model;

        }
    }
}
