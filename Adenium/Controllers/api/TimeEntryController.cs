﻿
using Adenium.Models.TimeEntryApi;
using BusinessObjectsLibrary;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Adenium.Controllers.TimeEntryApi
{
    [RoutePrefix("api/TimeEntry")]
    public class TimeEntryController : ApiController
    {
        [Route("ReportedHoursList"), ActionName("ReportedHoursList"), HttpGet]
        public ReportedHoursListResponse ReportedHoursList(int WorkedYear)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ReportedHoursListResponse(status, WorkedYear);
            return model;

        }
        [Route("ReportedHourDetail"), ActionName("ReportedHourDetail"), HttpGet]
        public ReportedHourDetailResponse ReportedHourGet(Guid ReportedHourGuid)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ReportedHourDetailResponse(status, ReportedHourGuid);
            return model;

        }
        [Route("ReportedHourAddUpdate"), ActionName("ReportedHourAddUpdate"), HttpPut]
        public ReportedHourAddUpdateResponse ReportedHourPut([FromBody] ReportedHourAddUpdateRequest request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ReportedHourAddUpdateResponse(status, request);
            return model;

        }
        
        [Route("ReportedHourDelete"), ActionName("ReportedHourDelete"), HttpDelete]
        public ReportedHourDeleteResponse ReportedHourDelete(ReportedHourDeleteRequest request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ReportedHourDeleteResponse(status, request);
            return model;

        }
        [Route("ReportingPeriodList"), ActionName("ReportingPeriodList"), HttpGet]
        public ReportingPeriodsListResponse ReportingPeriodsGet(int WorkedYear)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ReportingPeriodsListResponse(status, WorkedYear);
            return model;

        }
        [Route("ReportingYearsList"), ActionName("ReportingYearsList"), HttpGet]
        public ReportingYearsListResponse ReportingYearsGet()
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ReportingYearsListResponse(status);
            return model;

        }
        
    }

}

