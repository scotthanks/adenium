﻿
using Adenium.Models.ContractApi;
using BusinessObjectsLibrary;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Adenium.Controllers.API
{
    [RoutePrefix("api/Contract")]
    public class ContractController : ApiController
    {
        [Route("ContractList"), ActionName("ContractList"), HttpGet]
        public ContractListResponse ContractList()
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ContractListResponse(status);
            return model;

        }
        [Route("ContractDetail"), ActionName("ContractDetail"), HttpGet]
        public ContractDetailResponse ContractGet(Guid ContractGuid)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ContractDetailResponse(status, ContractGuid);
            return model;

        }
        [Route("ContractAddUpdate"), ActionName("ContractAddUpdate"), HttpPut]
        public ContractAddUpdateResponse ContractPut([FromBody] ContractAddUpdateRequest request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ContractAddUpdateResponse(status, request);
            return model;

        }
        [Route("ContractDelete"), ActionName("ContractDelete"), HttpDelete]
        public ContractDeleteResponse ContractDelete(ContractDeleteRequest request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ContractDeleteResponse(status, request);
            return model;

        }

        [Route("ConsultantList"), ActionName("ConsultantList"), HttpGet]
        public ConsultantListResponse ConsultantList()
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ConsultantListResponse(status);
            return model;

        }
        [Route("ConsultantDetail"), ActionName("ConsultantDetail"), HttpGet]
        public ConsultantDetailResponse ConsultantGet(Guid ConsultantGuid)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ConsultantDetailResponse(status, ConsultantGuid);
            return model;

        }
        [Route("ConsultantAddUpdate"), ActionName("ConsultantAddUpdate"), HttpPut]
        public ConsultantAddUpdateResponse ConsultantPut([FromBody] ConsultantAddUpdateRequest request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ConsultantAddUpdateResponse(status, request);
            return model;

        }
        [Route("ConsultantDelete"), ActionName("ConsultantDelete"), HttpDelete]
        public ConsultantDeleteResponse ConsultantDelete(ConsultantDeleteRequest request)
        {
            var userName = User.Identity.Name;
            var status = new StatusObject(userName);

            var model = new ConsultantDeleteResponse(status, request);
            return model;

        }
    }

}

