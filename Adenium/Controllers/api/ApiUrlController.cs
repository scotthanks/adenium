﻿using DataAccessLibrary;
using System.Web.Http;

namespace AllAdmin.Controllers.API
{
    [RoutePrefix("api")]
    public class ApiUrlController : ApiController
    {
        [Route("ChangeEpsUrl"), ActionName("ChangeEpsUrl"), HttpGet]
        public string ChangeEpsUrl([FromUri] RequestModel request)
        {
            EpsUrlService.SetEpsUrl(request.site);
            return "Environment Changed To: " + EpsUrlService.EpsUrl;

        }
    }
}
