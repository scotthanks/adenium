﻿using BusinessObjectsLibrary;
using BusinessServiceLibrary;
using System;
using System.Timers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using DataAccessLibrary;

namespace Adenium
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801


    public class MvcApplication : System.Web.HttpApplication
    {
      
        static Timer GreenFuseB2BOrderTimer = null;
        static Timer GreenFuseB2BGrowerOrderEmailTimer = null;

       
        static Timer EPSB2BDummenOrderTimer = null;

        static Timer AdminReportTimer = null;
        static Timer AdminProcessTimer = null;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            // this is the call to register your WebApi routes
            GlobalConfiguration.Configure(WebApiConfig.Register);

            // this is the call to register your MVC routes
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            var appSettingsReader = new System.Configuration.AppSettingsReader();
            var runB2B = (string)appSettingsReader.GetValue("RunB2B", typeof(string));

            var softwareOwner = (string)appSettingsReader.GetValue("SoftwareOwner", typeof(string));
           

           
        }
      
       
       
    }
}