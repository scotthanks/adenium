﻿

//Generic Ajax Call - overides global error handler
var modelAjaxCall = function (element, uri, httpVerb, data, onSuccess, callbacks) {
   //alert("ajaxCall URI = " + uri);
  //  alert(traverseObj(data, false));
    jQuery.ajax({
        url: uri,
        type: httpVerb,
        data: data,
        async: false,
        success: function (data, textStatus, jqXHR) {
            // viewModel.floatMessage(element, "hi", 2500);
            // alert(traverseObj(data, false));
           //  alert(textStatus);
            if (data.Status.Success === true) {
              //  alert("success");
                onSuccess(data);
              //  alert("about to call back");
                viewModelProcessEventChain(callbacks);
            }
            else {
                alert("Error Thrown: " + errorThrown + " " + uri);
                 viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alert("Error Thrown " + errorThrown + " " + uri);
            viewModelFloatMessage(element, "Error Thrown " + errorThrown, 2500);
        },
        always: function (jqXHR, textStatus) {
            //alert("about to call back");
            //viewModelProcessEventChain(callbacks);
            //if (textStatus == "success") {
            //    //alert("hi");
            //    //this is not firing.....
                 
            //} else {
            //    //  alert("hi");
            //};
        }
    });
};
var traverseObj = function (obj, useHtml) {
       //alert("in traverse");
    var returnStr = "<br />";
    if (typeof useHtml === 'undefined' || useHtml) {
        //alert("bingo!");
        //Leave returnStr as is
    } else if (!useHtml) {
        returnStr = "\n";
    };
    if (obj === null) return "null";
    var s = "";
    var offset = 0;

    var traverse = function (o) {
        if (o === 'null') { alert('[[null error traversing object ]]'); } else {
            //s += "..<br />";
            offset += 3;
            $.each(o, function (key, item) {
                if (item === null) {
                    for (var i = 0; i < offset; i++) { s += "." };
                    s += key + ": " + 'null';
                    s += returnStr;
                } else {

                    var type = typeof item;
                    //s += key + ": " + type;;
                    if (type === "object") {
                        //alert(key + ": " + type);
                        if (typeof key === 'string') {
                            for (i = 0; i < offset; i++) { s += "." };
                            s += key + ":" + returnStr;
                        };
                        //alert(key + ": " + o);
                        traverse(item);
                    } else {
                        for (i = 0; i < offset; i++) { s += "." };
                        s += key + ": " + item;
                        s += returnStr;
                    };

                };



            });
            offset -= 3;
        };
    };

    traverse(obj);
    return s;

};
var viewModelFloatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
  
    if (typeof element === 'undefined') { return false; };
    
    var top, left;
    top = null;
    left = null;
   // alert("element.length: " + element.length.toString());
    if (element === null) {
        //Leave values null
        top = null;
        left = null;
         //alert("in float3");
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + element.height() - 35 / 2);
        left = Math.floor(position.left + element.width() + 30);
      //  alert("in float4 top:" + top.toString() + " left: " + left.toString() );
    }
   //  alert("in float5" + left + "-" + top + "-" + content + "-" + interval );
    viewModelFloatingMessage(left, top, content, interval);
};
var viewModelFloatingMessage = function (x, y, content, interval) {
  //  alert("in FloatingMessage" + x.toString() + y.toString() + content);
    var $container = $("<div class=\"eps_floating_message_containers\" />");
    var $display = $("<div class=\"eps_floating_message_displays\" />");
    var $contents = $("<div class=\"eps_floating_message_contents\" />");

    $contents.html(content);
    $display.append($contents);
    $container.append($display);
    $("body").append($container);
    x = null;
    y = null;

    if (x === null) {
        x = Math.max($(window).width() - $container.outerWidth(), 0) / 2;
        x += $(window).scrollLeft();
    };
    if (y === null) {
        // Center the modal in the viewport
        y = $(window).height();
        y = y - $container.outerHeight();
        y = Math.max(y / 2, 0);
        y += $(window).scrollTop();
    };
   // alert(x + ", " + y);

    $container.css({
        "left": x,
        "top": y,
    });
    $container.fadeIn("fast").delay(interval).fadeOut(1000).remove("slow");
};
var viewModelProcessEventChain = function (callbacks) {
    //alert("ProcessEventChain");
    if (callbacks !== undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};
var eps_tools_HoverMessage = function (element) {
    if (typeof element === "undefined" || element === null) { return false; };  //must have an element to hover around
    var method = {};
    var id = element.data("hoverMessageContainerId") || "";
    if (id === "") {
        //this element does not have a hover message so wireup one
        method.id = eps_createNewGuid();
        $container = $("<div />").prop({ "id": method.id, "class": "eps_hover_message_containers" });
        var el = $(element) || null; //begin with the element itself
        if (el !== null) {
            el.data({ "hoverMessageContainerId": method.id });  //add the element to this
        };
        $("body").append($container);
    } else {
        method.id = id;
    };
    method.element = element;
    //method.id = (id == "") ? eps_createNewGuid() : id;


    method.name = "Fred Flintsone";
    method.test = function () {
        alert(this.name);
    };
    method.load = function (content) {
        //alert(this.id);
        //var $container = $("#" + this.id).empty();      //empty incase this is a reload
        var $display = $("<div />").prop({ "class": "eps_hover_message_displays" });
        var $contents = $("<div />").prop({ "class": "eps_hover_message_contents" });
        $contents.html(content);
        $display.append($contents);
        $container.append($display);
        return method;
    }
    method.display = function (position) {
        //alert("position: " + position + "\nid: " + this.id);

        var x = 0, y = 0;
        switch (position) {
            case 4: //hover message goes immediately to left of element
                //var position = element.offset();
                //var top = Math.floor(position.top + ((element.height() - 35) / 2));
                //var left = Math.floor(position.left + element.width() + 30);
                var pos = this.element.offset();

                x = Math.floor(pos.left - $container.width() - 10);

                y = Math.floor(pos.top + ((this.element.height() - 35) / 2));

                //y += 100;
                break;
            default:
                //alert("here");
                //center horizontally in viewport
                x = Math.max($(window).width() - $container.outerWidth(), 0) / 2;
                x += $(window).scrollLeft();
                // Center vertically in the viewport
                y = $(window).height();
                y = y - $container.outerHeight();
                y = Math.max(y / 2, 0);
                y += $(window).scrollTop();
        }

        $container.css({
            "left": x,
            "top": y,
        });
        $("#" + this.id).fadeIn("fast");



    };
    method.remove = function () {
        $("#" + this.id).remove();
        $(this.element).data({ "hoverMessageContainerId": "" });
    };

    return method;

};

var eps_tools_FormSubmitDialog = function (settings) {
        var modal = (function () {
        if (typeof settings.modal === 'undefined' || settings.modal === null || settings.modal === true) {
            return true;
        }
        return false;
    }());

    eps_tools_Dialog.open({
        'title': settings.title || null,
        'content': settings.content || null,
        'action': null,     //clear action from prior uses of dialog
        "size": settings.size || null,
        "locate": settings.locate || null,
        'modal': modal
    });

};

var eps_tools_Dialog = (function () {


    var method = {},
        overlayId = 'eps_dialog_overlay',
        containerId = "eps_dialog_container",
        displayId = "eps_dialog_display",
        titleId = "eps_dialog_title",
        contentId = "eps_dialog_content",
        actionId = "eps_dialog_action",
        closeId = "eps_dialog_close";

    var $overlay = $('<div />').prop({ "id": overlayId });
    var $container = $('<div />').prop({ "id": containerId });
    var $display = $('<div />').prop({ "id": displayId });
    var $title = $('<div />').prop({ "id": titleId });
    var $content = $('<div />').prop({ "id": contentId });
    var $action = $('<div />').prop({ "id": actionId });
    var $close = $('<div />').prop({ "id": closeId });

    $container.on("keydown", function (e) {
        e.stopPropagation();
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        //ToDo: split enter and escape, enter should trigger action, escape trigger close;
        if (key === 13 || key === 27) {
            e.preventDefault();
            method.close();
        }
    });

    $close.on("click", function () {
        method.close();
    });

    $display.append($title, $content, $action);
    $container.append($display, $close);

    $(document).ready(function () {
        $('body').append($overlay, $container);
    });

    //alert("bingo " + ($("#eps_dialog_container").length || -1));


    method.open = function (settings) {

        var modal = (function () {
            if (typeof settings.modal === 'undefined' || settings.modal === null || settings.modal === true) {
                return true;
            }
            return false;
        }());

        if (modal) {
            //$overlay.css({"width":"100%", "height":"100%"});
            $overlay.fadeIn("slow");
            $overlay.on("click", function () {
                method.close();
            });
        };
        $title.html(settings.title);
        $content.html(settings.content);
        $action.html(settings.action);

        method.size(settings.size);

        method.locate(settings.locate);
        $container.fadeIn("slow").focus();
    };

    method.close = function () {
        $overlay.fadeOut(500);
        $container.fadeOut(500, function () {
            $title.empty();
            $content.empty();
            $action.empty();
        });
    };

    // Set the size of the modal dialog
    method.size = function (size) {
        if (typeof size === 'undefined' || size === null) { return false; };

        //if (typeof size.width === 'undefined' || typeof size.height === 'undefined') { return false; };
        //if (typeof size.width !== 'number' || typeof size.height !== 'number') { return false; };
        //if (size.width < 50) { size.width = 50 };
        //if (size.height < 30) { size.width = 30 };

        $content.css({
            "width": size.width || 50,
            "height": size.height || 30
        });
        return true;
    };

    method.locate = function (locate) {
        var top, left;

        // Center the modal in the viewport
        top = Math.max($(window).height() - $container.outerHeight(), 0) / 2;
        //top += $(window).scrollTop();
        left = Math.max($(window).width() - $container.outerWidth(), 0) / 2;
        //left += $(window).scrollLeft();

        //set location if settings provided
        if (typeof locate !== 'undefined' && locate !== null) {
            if (typeof locate.top !== 'undefined' && locate.top !== null) { top = locate.top; };
            if (typeof locate.left !== 'undefined' && locate.left !== null) { left = locate.left; };
        };

        //set css to location
        $container.css({
            top: top + $(window).scrollTop(),
            left: left + $(window).scrollLeft()
        });
    };

    return method;


}());






var  eps_createNewGuid = function () {

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });

}
var sort_by = function (field, reverse, primer) {

    var key = primer ?
        function (x) { return primer(x[field]) } :
        function (x) { return x[field] };

    reverse = !reverse ? 1 : -1;

    return function (a, b) {
        return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
    }
}



function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}

function getLocalDateStringFromGMTDate(date) {
    // alert(date);
    var theDate = new Date(date);
    //  alert(theDate.toLocaleString());
    var subtractMinutes = theDate.getTimezoneOffset();

    theDate.setMinutes(theDate.getMinutes() - subtractMinutes);
    //  alert(theDate.toLocaleString());
    var myDate = new Date(theDate.toLocaleString());
    //  alert(myDate.toLocaleString());


    return myDate.toLocaleString();
};

var reportRunStart = function () {
   // alert("in reportRunStart");
    //$('#reset').prop('disabled', true);
    //$('#submit').prop('disabled', true);
    //$('.run-report').addClass('collapse');
    //$('.please-wait').removeClass('collapse');
    $("#RunReport").text("Please Wait...");
};

var reportRunEnd = function () {
   // alert("in reportRunEnd");
    //$('#reset').prop('disabled', false);
    //$('#submit').prop('disabled', false);
    //$('.run-report').removeClass('collapse');
    //$('.please-wait').addClass('collapse');
    //$('#criteria').collapse('hide');

    var xFrameId = $('#xViewer > iframe:eq(0)').attr('id');
    var xFrame = $('#' + xFrameId);
    var xFrameHeight = xFrame.contents().height();
   // alert(xFrameHeight);
    document.getElementById(xFrameId).style.height = xFrameHeight + 50 + "px";
   var xFrameWidth = xFrame.contents().width();
   // var xFrameWidth = "2064px"
   
    document.getElementById(xFrameId).style.width = xFrameWidth + 50 + "px";
  //  alert(document.getElementById(xFrameId).style.width);
    console.log(xFrameId + " " + xFrameHeight);
    console.log(xFrameId + " " + xFrameWidth);
    // $('#showHideCriteria').scrollTop(0);
    //$('html, body').animate({ scrollTop: $('#showHideCriteria').offset().top }, "fast");
    $("#RunReport").text("Run Report");

};

Date.prototype.formatMMDDYYYY = function () {
    return (this.getMonth() + 1) +
        "/" + this.getDate() +
        "/" + this.getFullYear();
};



// THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
function isNumber(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;
    return true;
}



var GetCurrentDate = function () {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    return today;

};