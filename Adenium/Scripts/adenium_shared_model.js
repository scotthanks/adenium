﻿
///Shared Business Service Page Functions
var LoadClients = function (likeParam, element, onSuccessCall, callbacks) {   
    var url = "../api/Client/ClientList";
    var data = {       
    }
    var onSuccess = function (data) {
        //alert("in success after call");
        if (data.Status.Success == true) {
            // alert(traverseObj(data.Clients));
            onSuccessCall(data);
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call");
    modelAjaxCall(element, url, "GET", data, onSuccess, callbacks);
};
var LoadConsultants = function (likeParam, element, onSuccessCall, callbacks) {
    var url = "../api/Contract/ConsultantList";
    var data = {
    }
    var onSuccess = function (data) {
        //alert("in success after call");
        if (data.Status.Success == true) {
            // alert(traverseObj(data.Clients));
            onSuccessCall(data);
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call");
    modelAjaxCall(element, url, "GET", data, onSuccess, callbacks);
};
var LoadContracts = function (likeParam, element, onSuccessCall, callbacks) {
    var url = "../api/Contract/ContractList";
    var data = {
    }
    var onSuccess = function (data) {
        //alert("in success after call");
        if (data.Status.Success == true) {
            // alert(traverseObj(data.Contracts));
            onSuccessCall(data);
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call");
    modelAjaxCall(element, url, "GET", data, onSuccess, callbacks);
};
var LoadReportingPeriods = function (workedYear, element, onSuccessCall, callbacks) {
    var url = "../api/TimeEntry/ReportingPeriodList";
  //  alert(workedYear);
    var data = {
        WorkedYear: workedYear
    }
    var onSuccess = function (data) {
        //alert("in success after call");
        if (data.Status.Success == true) {
            // alert(traverseObj(data.ReportingPeriods));
            onSuccessCall(data);
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call");
    modelAjaxCall(element, url, "GET", data, onSuccess, callbacks);
};
var LoadReportingYears = function (element, onSuccessCall, callbacks) {
    var url = "../api/TimeEntry/ReportingYearsList";
    var data = {
        
    }
    var onSuccess = function (data) {
        //alert("in success after call");
        if (data.Status.Success == true) {
             console.log(data);
            onSuccessCall(data);
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call");
    modelAjaxCall(element, url, "GET", data, onSuccess, callbacks);
};
var LoadLookupList = function (listName,element, onSuccessCall, callbacks) {
    var url = "../api/Shared/LookupList";
    var data = {
        ListName: listName,
    }
    var onSuccess = function (data) {
        //alert("in success after call");
        if (data.Status.Success == true) {
           // console.log(data);
            onSuccessCall(data);
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call");
    modelAjaxCall(element, url, "GET", data, onSuccess, callbacks);
};
DisplayList = function (cboElement,itemList,idenitiferGuid,includeAll) {

    var list = "";
    if (includeAll == true) {
        if (idenitiferGuid === null) {
            list += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
        }
        else {
            list += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
        }
    }
   
    
    $.each(itemList, function (i, item) {
        
        if (item.Guid === idenitiferGuid) {
            list += "<option value='" + item.Guid + "' selected>" + item.Display + "</option>";
        }
        else {
            list += "<option value='" + item.Guid + "'>" + item.Display + "</option>";
        }

    });
    cboElement.empty();
  
   // cboElement.html(list); //add the options to the html
   
    //var newOption = $('<option value="1">test</option>');
    cboElement.append(list);
    cboElement.trigger("chosen:updated");
    cboElement.chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold:3

    });
}