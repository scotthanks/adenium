﻿using System.Web;
using System.Web.Mvc;

namespace Adenium
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new System.Web.Mvc.AuthorizeAttribute());   // gib - this should require a logged in user 
                                                                    //for all controller actions expect those maked AllowAnonymous
        }
    }
}