﻿
var model = {};
model.data = {
    reportData: [],
};

model.config = {

   
    urlReport: "../api/Report/GetDummenQuoteSummaryReportData",


};




model.loadReport = function () {
    var url = model.config.urlReport; 
    var element = $("#" + viewModel.config.reportTableId);
    var data = {};
   
    var onSuccess = function (data) {
        // alert(traverseObj(data));
         model.data.reportData = data.ReportData;

        if (data.Status.Success == true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    modelAjaxCall(element, url, "Get", data, onSuccess, []);
};

var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
 
};



viewModel.Init = function () {



    $("#get_report_button").on("click", function () {
        
        model.loadReport( );
    });
    $("#export_button").on('click', function () {

        $("#reportTable").tableToCSV();
    });
};




viewModel.RefreshReportDisplay = function (data) {

 //   alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">Grower</th>"');
    row.append('<th style="vertical-align:top;align:center">Program Season Code</th>"');
    row.append('<th style="vertical-align:top;align:center">Program Season Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Program Code</th>"');
    row.append('<th style="vertical-align:top;align:center">Program Name</th>"');
    row.append('<th style="vertical-align:top;align:center">ProductCount</th>"');
   


    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        var cell = $("<td />");
        cell.append(item.GrowerName);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.ProgramSeasonCode);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.ProgramSeasonName);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.ProgramCode);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.ProgramName);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.ProductCount);
        row.append(cell);


       

        tbody.append(row);


    });


};


