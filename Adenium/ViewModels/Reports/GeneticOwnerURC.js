﻿
var model = {};
model.data = {

    reportData: [],
};

model.config = {
    urlReport: "../api/Report/GetGeneticOwnerURCReportData/"   
};


model.loadGeneticOwners = function (geneticOwnerGuid) {
    var element = $("#cboGrowerName");

    var onSuccessCall = function (data) {
       //  alert("in success after call");
        model.data.geneticOwners = data.GeneticOwners;
        viewModel.RefreshGeneticOwnerList(geneticOwnerGuid);
        return true;
    }
    bret = LoadGeneticOwners(element, onSuccessCall, []);
}
model.loadMonths = function (startMonthCode, endMonthCode) {
    var element = $("#cboMonthStart");

    var onSuccessCall = function (data) {
      //   alert("in success after call");
        model.data.months = data.Months;
        viewModel.RefreshMonthLists(startMonthCode, endMonthCode);
        return true;
    };
    bret = LoadMonths(element, onSuccessCall, []);
};



var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable"
};



viewModel.Init = function () {
    var geneticOwnerGuid = localStorage.getItem("URCReportGeneticOwnerGuid");
    model.loadGeneticOwners(geneticOwnerGuid);

    var startMonth = localStorage.getItem("URCReportStartMonth");
    if (startMonth == null) {
        startMonth = "201601";
        localStorage.setItem("URCReportStartMonth", "201601");
    }
    var endMonth = localStorage.getItem("URCReportEndMonth");
    if (endMonth == null) {
        endMonth = "201612";
        localStorage.setItem("URCReportEndMonth", "201612");

    }
    model.loadMonths(startMonth, endMonth);

   

    $("#cboMonthStart").on('change', function () {
        localStorage.setItem("URCReportStartMonth", $("#cboMonthStart").val());
    });
    $("#cboMonthEnd").on('change', function () {
        localStorage.setItem("URCReportEndMonth", $("#cboMonthEnd").val());
    });


    $("#cboGeneticOwnerName").on('change', function () {
        localStorage.setItem("URCReportGeneticOwnerGuid", $("#cboGeneticOwnerName").val());
    });
  
    
};



viewModel.RefreshGeneticOwnerList = function (geneticOwnerGuid) {
    var geneticOwnerList = "";
    $.each(model.data.geneticOwners, function (i, item) {
        if (item.Guid == geneticOwnerGuid || (geneticOwnerGuid == null && item.Guid == '00000000-0000-0000-0000-000000000000')) {
            geneticOwnerList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            geneticOwnerList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }

    });
    

    $("#cboGeneticOwnerName").html(geneticOwnerList); //add the options to the html


};

viewModel.RefreshMonthLists = function (startMonthCode, endMonthCode) {

    var startMonthList = "";

    $.each(model.data.months, function (i, item) {
        if (item.Code == startMonthCode) {
            startMonthList += "<option value='" + item.Code + "' selected>" + item.Display + "</option>";
        }
        else {
            startMonthList += "<option value='" + item.Code + "'>" + item.Display + "</option>";
        }

    });
    $("#cboMonthStart").html(startMonthList); //add the options to the html



    var endMonthList = "";

    $.each(model.data.months, function (i, item) {
        if (item.Code == endMonthCode) {
            endMonthList += "<option value='" + item.Code + "' selected>" + item.Display + "</option>";
        }
        else {
            endMonthList += "<option value='" + item.Code + "'>" + item.Display + "</option>";
        }

    });
    $("#cboMonthEnd").html(endMonthList); //add the options to the html


};
viewModel.RefreshReportDisplay = function (data,monthStartCode,monthEndCode) {
   // alert(traverseObj(data));
    var quantities= [];
  
   
  
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">Genetic Owner</th>"');
    row.append('<th style="vertical-align:top;align:center">Variety Name</th>"');
    
    for (i = monthStartCode; i <= monthEndCode; i++) {
        row.append('<th style="vertical-align:top;align:center">' + i + '</th>"');
        quantities.push(0);
        //alert(i.toString().substring(4, 2));
        //if (i.toString().substring(4,2) = "12") {
        //    i = i + 100 - 11;
        //    alert(i);
        //}
    }


    row.append('<th style="vertical-align:top;align:center">Total</th>"');
    

    tbody.append(row);

    var theTotal = 0;
    var variety = "";
    $.each(data, function (i, item) {
        if (item.VarietyName != variety) {
            variety = item.VarietyName;
           // alert("cleararry");
            for (j = monthStartCode; j <= monthEndCode; j++){
                quantities[j - monthStartCode] = 0;
            }
          //  alert(quantities.toString());
           
        }
        
        quantities[item.MonthCode - monthStartCode] = item.URCQty;
       // alert(quantities.toString());
        //alert(item.MonthCode);
        //alert(monthStartCode);
        //alert(quantities[item.MonthCode - monthStartCode]);
        
        if (i == data.length - 1 || data[i + 1].VarietyName != variety) {
            var row = $("<tr />");
            row.prop(rowProperties);
           
        
       

            var cell = $("<td />");
            cell.append(item.GeneticOwner);
            row.append(cell);

            var cell = $("<td />");
            cell.append(item.VarietyName);
            row.append(cell);
            //alert(quantities.toString());
            theTotal = 0;
            for (j = monthStartCode; j <= monthEndCode; j++) {
                //alert(quantities[j - monthStartCode]);
                cell = $("<td />");
                cell.append(quantities[j - monthStartCode]);
                row.append(cell);
                theTotal = theTotal + quantities[j - monthStartCode];
            }
          

            cell = $("<td />");
            cell.append(theTotal);
            row.append(cell);

       
            tbody.append(row);

        }
    });


};


