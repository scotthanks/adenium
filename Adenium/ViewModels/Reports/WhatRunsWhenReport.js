﻿
var model = {};
model.data = {

    reportData: [],
};

model.config = {

   
    urlReport: "../api/Report/GetWhatRunsWhenReportData/",
    
};



model.loadReport = function () {
    // alert("In Load Programs");
  //  alert(format);
   // alert(email);
    var url = model.config.urlReport;
   

    var element = $("#" + viewModel.config.reportTableId);

    var data = {};
   

    var onSuccess = function (data) {
        // alert(traverseObj(data));
        model.data.reportData = data.ReportData;

        if (data.Status.Success === true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //  alert("about to call ajax");
    //   alert(element.visible);


    modelAjaxCall(element, url, "Get", data, onSuccess, []);

};

var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
    cboFormatID: "cboFormat",
    chkEmailID: "chkSendEmail",
};



viewModel.Init = function () {

  
    $("#get_report_button").on("click", function () {

        model.loadReport( );
    });
};



viewModel.RefreshReportDisplay = function (data) {

    //alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">ID</th>"');
    row.append('<th style="vertical-align:top;align:center">Job Type</th>"');
    row.append('<th style="vertical-align:top;align:center">Job Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Job Description</th>"');
    row.append('<th style="vertical-align:top;align:center">Last Run Date</th>"');
    row.append('<th style="vertical-align:top;align:center">Next Run Date</th>"');
    row.append('<th style="vertical-align:top;align:center">Minutes Between</th>"');
    row.append('<th style="vertical-align:top;align:center">Is Active</th>"');
    row.append('<th style="vertical-align:top;align:center">Email From</th>"');
    row.append('<th style="vertical-align:top;align:center">Email To</th>"');
    row.append('<th style="vertical-align:top;align:center">Email CC</th>"');
   

    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        var cell = $("<td />");
        cell.append(item.ID);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.JobType);
        row.append(cell);
        cell = $("<td />");

        cell.append(item.JobName);
        row.append(cell);
        
        cell = $("<td />");
        cell.append(item.JobDescription);
        row.append(cell);

        cell = $("<td />");
        cell.append(getLocalDateStringFromGMTDate(item.LastRunDate));
        row.append(cell);

        cell = $("<td />");
        cell.append(getLocalDateStringFromGMTDate(item.NextRunDate));
        row.append(cell);

        cell = $("<td />");
        cell.append(item.MinutesBetweenRun);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.IsActive);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.EmailFrom);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.EmailTo);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.EmailCC);
        row.append(cell);

      


        tbody.append(row);

    
    });


};


