﻿
var model = {};
model.data = {

    reportData: [],
};

model.config = {

};
model.loadGeneticOwners = function (geneticOwnerGuid) {
    var element = $("#cboBreederCode");

    var onSuccessCall = function (data) {
        //  alert("in success after call");
        model.data.breederCodes = data.GeneticOwners;
        viewModel.RefreshGeneticOwnerList(geneticOwnerGuid)
        return true;
    }
    bret = LoadGreenFuseGeneticOwners(element, onSuccessCall, []);



}


var viewModel = {};
viewModel.config = {
 
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
};



viewModel.Init = function () {
    model.loadGeneticOwners(null);

};





viewModel.RefreshGeneticOwnerList = function (geneticOwnerGuid) {

    var geneticOwnerList = "";


    $.each(model.data.breederCodes, function (i, item) {
        if (item.Guid == geneticOwnerGuid || (geneticOwnerGuid == null && item.Guid == '00000000-0000-0000-0000-000000000000')) {
            geneticOwnerList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            geneticOwnerList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }

    });
    $("#cboBreederCode").html(geneticOwnerList); //add the options to the html



    //$("#cboBreederCode").chosen({
    //    no_results_text: "Oops, nothing found!",
    //    disable_search_threshold: 5

    //});
}
