﻿
var model = {};
model.data = {
    reportData: [],
};

model.config = {

   
    urlReport: "../api/Report/GetDummenZeroPriceReportData",


};




model.loadReport = function () {
    var url = model.config.urlReport; 
    var element = $("#" + viewModel.config.reportTableId);
    var data = {};
   
    var onSuccess = function (data) {
        // alert(traverseObj(data));
         model.data.reportData = data.ReportData;

        if (data.Status.Success == true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    modelAjaxCall(element, url, "Get", data, onSuccess, []);
};

var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
 
};



viewModel.Init = function () {



    $("#get_report_button").on("click", function () {
        
        model.loadReport( );
    });
    $("#export_button").on('click', function () {

        $("#reportTable").tableToCSV();
    });
};




viewModel.RefreshReportDisplay = function (data) {

 //   alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">Grower</th>"');
    row.append('<th style="vertical-align:top;align:center">Seller</th>"');
    row.append('<th style="vertical-align:top;align:center">OrderNo</th>"');
    row.append('<th style="vertical-align:top;align:center">Year</th>"');
    row.append('<th style="vertical-align:top;align:center">Week</th>"');
    row.append('<th style="vertical-align:top;align:center">LineNumber</th>"');
    row.append('<th style="vertical-align:top;align:center">Status</th>"');
    row.append('<th style="vertical-align:top;align:center">Supplier Identifier</th>"');
    row.append('<th style="vertical-align:top;align:center">Supplier Description</th>"');
    row.append('<th style="vertical-align:top;align:center">Qty</th>"');
    row.append('<th style="vertical-align:top;align:center">Price</th>"');
    row.append('<th style="vertical-align:top;align:center">Cost</th>"');



    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        var cell = $("<td />");
        cell.append(item.GrowerName);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.SellerName);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.OrderNo);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Year);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Week);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.LineNumber);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.Status);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.SupplierIdentifier);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.SupplierDescription);
        row.append(cell);
        cell = $("<td />");
        cell.append(item.QtyOrdered);
        row.append(cell);
        cell = $("<td />");
        cell.append(accounting.formatMoney((item.ActualPrice, "$", 4, ",", ".", "%s%v")));
        row.append(cell);
        cell = $("<td />");
        cell.append(accounting.formatMoney((item.ActualCost, "$", 4, ",", ".", "%s%v")));
        row.append(cell);


       

        tbody.append(row);


    });


};


