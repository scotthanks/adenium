﻿
var model = {};
model.data = {

    reportData: [],
};

model.config = {

    
};

model.loadGrowers = function (growerGuid) {
    var element = $("#cboGrowerName");

    var onSuccessCall = function (data) {
        //alert("in success after call");
        model.data.growers = data.Growers;
        viewModel.RefreshGrowerList(growerGuid);
        return true;
    };
    bret = LoadGrowers("", element, onSuccessCall, []);
};
model.loadBrokers = function (brokerGuid) {
    var element = $("#cboBrokerName");

    var onSuccessCall = function (data) {
        //alert("in success after call");
        model.data.brokers = data.Brokers;
        viewModel.RefreshBrokerList(brokerGuid);
        return true;
    };
    bret = LoadBrokers("", element, onSuccessCall, []);
};
model.loadSuppliers = function (supplierGuid) {
    var element = $("#cboSupplierName");

    var onSuccessCall = function (data) {
        //alert("in success after call");
        model.data.suppliers = data.Suppliers;
        viewModel.RefreshSupplierList(supplierGuid);
        return true;
    };
    bret = LoadSuppliers("", element, onSuccessCall, []);
};


var viewModel = {};
viewModel.config = {
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
 
};



viewModel.Init = function () {


    model.loadGrowers();
    model.loadSuppliers();
    model.loadBrokers();
  
};


viewModel.RefreshBrokerList = function (brokerGuid) {

    var brokerList = "";
    if (brokerGuid === null) {
        brokerList += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
    }
    else {
        brokerList += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
    }

    $.each(model.data.brokers, function (i, item) {
        if (item.Guid === brokerGuid) {
            brokerList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            brokerList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }
    });

    $("#cboBrokerName").html(brokerList); //add the options to the html
};

viewModel.RefreshSupplierList = function (supplierGuid) {

    var supplierList = "";
    if (supplierGuid === null) {
        supplierList += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
    }
    else {
        supplierList += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
    }

    $.each(model.data.suppliers, function (i, item) {
        if (item.Guid === supplierGuid) {
            supplierList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            supplierList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }
    });

    $("#cboSupplierName").html(supplierList); //add the options to the html
};

viewModel.RefreshGrowerList = function (growerGuid) {

    var growerList = "";
    if (growerGuid === null) {
        growerList += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
    }
    else {
        growerList += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
    }

    $.each(model.data.growers, function (i, item) {
        if (item.Guid === growerGuid) {
            growerList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
           growerList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }
    });

    $("#cboGrowerName").html(growerList); //add the options to the html
};



