﻿
var model = {};
model.data = {
    reportData: [],
};

model.config = {

   
    urlReport: "../api/Report/GetSearches",

};



model.loadReport = function () {
    // alert("In Load Programs");
   
    var recordCount = localStorage.getItem("RecordCount");

    var url = model.config.urlReport;
    url += "?RecordCount=" + recordCount;
   
    var element = $("#" + viewModel.config.reportTableId);

    var data = {};
   
   // alert(supplierLocationCode);


    var onSuccess = function (data) {
        // alert(traverseObj(data));
         model.data.reportData = data.ReportData;

        if (data.Status.Success == true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //  alert("about to call ajax");
    //   alert(element.visible);


    modelAjaxCall(element, url, "Get", data, onSuccess, []);

};

var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
 
};



viewModel.Init = function () {
    if (localStorage.getItem("RecordCount")=== null) {
        localStorage.setItem("RecordCount", $("#txtRecordCount").val());
    }
    $("#txtRecordCount").val(localStorage.getItem("RecordCount"));
    model.loadReport();
  

    $("#txtRecordCount").on('change', function () {
        localStorage.setItem("RecordCount", $("#txtRecordCount").val());

    });
    $("#get_report_button").on("click", function () {

        model.loadReport();
    });
  
};


viewModel.RefreshReportDisplay = function (data) {

 //   alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">Search Date</th>"');
    row.append('<th style="vertical-align:top;align:center">Search Term</th>"');
    row.append('<th style="vertical-align:top;align:center">User Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Email</th>"');


    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        var cell = $("<td />");
        var d = new Date(item.SearchDate);
        cell.append(formatDate(d));
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SearchTerm);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.UserName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Email);
        row.append(cell);


        tbody.append(row);


    });


};


