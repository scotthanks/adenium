﻿
var model = {};
model.data = {
    reportData: [],
};

model.config = {

   
    urlReport: "../api/Report/GetDummenAvailGapReportData",


};




model.loadReport = function () {
    // alert("In Load Programs");

    var url = model.config.urlReport;
    var startWeek = localStorage.getItem("StartWeek");
    var endWeek = localStorage.getItem("EndWeek");
    url += "?StartWeek=" + startWeek + "&EndWeek=" + endWeek;
   
    var element = $("#" + viewModel.config.reportTableId);

    var data = {};
   
   // alert(supplierLocationCode);


    var onSuccess = function (data) {
        // alert(traverseObj(data));
         model.data.reportData = data.ReportData;

        if (data.Status.Success == true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //  alert("about to call ajax");
    //   alert(element.visible);


    modelAjaxCall(element, url, "Get", data, onSuccess, []);

};

var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
 
};



viewModel.Init = function () {



    $("#txtStartWeek").on('change', function () {
        localStorage.setItem("StartWeek", $("#txtStartWeek").val());

    });
    $("#txtEndWeek").on('change', function () {
        localStorage.setItem("EndWeek", $("#txtEndWeek").val());

    });

    $("#get_report_button").on("click", function () {
        localStorage.setItem("StartWeek", $("#txtStartWeek").val());
        localStorage.setItem("EndWeek", $("#txtEndWeek").val());
        model.loadReport( );
    });
    $("#export_button").on('click', function () {

        $("#reportTable").tableToCSV();
    });
};




viewModel.RefreshReportDisplay = function (data) {

 //   alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">Program Code</th>"');
    row.append('<th style="vertical-align:top;align:center">Program Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Species Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Variety Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Supplier Identifier</th>"');
    row.append('<th style="vertical-align:top;align:center">Supplier Description</th>"');
    row.append('<th style="vertical-align:top;align:center">Status</th>"');


    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        var cell = $("<td />");
        cell.append(item.ProgramCode);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.ProgramName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SpeciesName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.VarietyName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SupplierIdentifier);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SupplierDescription);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Status);
        row.append(cell);

       

        tbody.append(row);


    });


};


