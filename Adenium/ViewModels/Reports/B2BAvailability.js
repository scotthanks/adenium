﻿
var model = {};
model.data = {
    supplierCodes: [],
    reportData: [],
};

model.config = {

   
    urlReport: "../api/Report/GetB2BAvailabilityReportData",
    urlLocationCodes: "../api/Report/GetSupplierLocationCodes",

};


model.loadSupplierLocations = function () {


    var url = model.config.urlLocationCodes;
   

    var element = $("#cboSupplierCode");

    var data = {};

    var onSuccess = function (data) {
        // alert(traverseObj(data));
        model.data.supplierCodes = data.LocationCodes;

        if (data.Status.Success == true) {
            //   alert("In Success");
            viewModel.RefreshSupplierCodeList();

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //  alert("about to call ajax");
    //   alert(element.visible);


    modelAjaxCall(element, url, "Get", data, onSuccess, []);
}


model.loadReport = function (supplierLocationCode) {
   // alert("In Load Programs: " + supplierLocationCode);

    var url = model.config.urlReport;
    url += "?SupplierLocationCode=" + supplierLocationCode;
   
    var element = $("#" + viewModel.config.reportTableId);

    var data = {};
   
   // alert(supplierLocationCode);


    var onSuccess = function (data) {
        // alert(traverseObj(data));
         model.data.reportData = data.ReportData;

        if (data.Status.Success == true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //  alert("about to call ajax");
    //   alert(element.visible);


    modelAjaxCall(element, url, "Get", data, onSuccess, []);

};

var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
 
};



viewModel.Init = function () {

    model.loadSupplierLocations();
    
    localStorage.setItem("SupplierCode", "GFB");

    $("#cboSupplierCode").on('change', function () {
        localStorage.setItem("SupplierCode", $("#cboSupplierCode").chosen().val());

    });

    $("#get_report_button").on("click", function () {

        model.loadReport(
            localStorage.getItem("SupplierCode")
            );
    });

    $("#export_button").on('click', function () {
        $("#reportTable").tableToCSV();
    });
};



viewModel.RefreshSupplierCodeList = function () {
    
    var supplierCodeList = ""
    $.each(model.data.supplierCodes, function (i, item) {     
            supplierCodeList += "<option value='" + item.Code + "'>" + item.Display + "</option>";
    });
   
    
    $("#cboSupplierCode").html(supplierCodeList); //add the options to the html

   

    $("#cboSupplierCode").chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 5

    });
}

viewModel.RefreshReportDisplay = function (data) {

 //   alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">SupplierIdentifier</th>"');
    row.append('<th style="vertical-align:top;align:center">SupplierDescription</th>"');
    row.append('<th style="vertical-align:top;align:center">Program</th>"');
    row.append('<th style="vertical-align:top;align:center">Variety</th>"');
    row.append('<th style="vertical-align:top;align:center">Species</th>"');
    row.append('<th style="vertical-align:top;align:center">Ship Year</th>"');
    row.append('<th style="vertical-align:top;align:center">Ship Week</th>"');
    row.append('<th style="vertical-align:top;align:center">Quantity</th>"');


    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        var cell = $("<td />");
        cell.append(item.SupplierIdentifier);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SupplierDescription);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.ProgramName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.VarietyName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SpeciesName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Year);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Week);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Qty);
        row.append(cell);


        tbody.append(row);


    });


};


