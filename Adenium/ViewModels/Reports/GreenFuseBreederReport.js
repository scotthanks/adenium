﻿
var model = {};
model.data = {

    reportData: [],
};

model.config = {

   
    urlReport: "../api/Report/GetGreenFuseOrderReportData/",
    
};


model.loadGeneticOwners = function (geneticOwnerGuid) {
    var element = $("#cboBreederCode");

    var onSuccessCall = function (data) {
       //  alert("in success after call");
         model.data.breederCodes = data.GeneticOwners;
        viewModel.RefreshGeneticOwnerList(geneticOwnerGuid)
        return true;
    }
    bret = LoadGreenFuseGeneticOwners( element, onSuccessCall, []);


    
}

model.loadSpecies = function (speciesCode) {
    var element = $("#cboSpeciesName");

    var onSuccessCall = function (data) {
         //alert("in success after call");
        model.data.species = data.Species;
        viewModel.RefreshSpeciesList(speciesCode)
        return true;
    }
    bret = LoadSpecies( element, onSuccessCall, []);
}

model.loadReport = function (geneticOwnerGuid,speciesCode,varietyNameLike,fromYear,fromWeek,toYear,toWeek,format,email) {
    // alert("In Load Programs");
  //  alert(format);
   // alert(email);
    var url = model.config.urlReport;
    url += "?GeneticOwnerGuid=" + geneticOwnerGuid;
    url += "&SpeciesCode=" + speciesCode;
    url += "&VarietyNameLike=" + varietyNameLike;
    url += "&FromYear=" + fromYear;
    url += "&FromWeek=" + fromWeek;
    url += "&ToYear=" + toYear;
    url += "&ToWeek=" + toWeek;
    url += "&Format=" + format;
    url += "&SendEmail=" + email;

    var element = $("#" + viewModel.config.reportTableId);

    var data = {};
   
    //alert(growerGuid);
    //alert(fromYear);
    //alert(fromWeek);
    //alert(toYear);
    //alert(toWeek);

    var onSuccess = function (data) {
        // alert(traverseObj(data));
        model.data.reportData = data.ReportData;

        if (data.Status.Success == true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData,format);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //  alert("about to call ajax");
    //   alert(element.visible);


    modelAjaxCall(element, url, "Get", data, onSuccess, []);

};

var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
    cboFormatID: "cboFormat",
    chkEmailID: "chkSendEmail",
};



viewModel.Init = function () {

    model.loadGeneticOwners();
    model.loadSpecies();
    $("#fromYear").val(new Date().getFullYear());
    $("#fromWeek").val(1);
    $("#toYear").val(new Date().getFullYear());
    $("#toWeek").val(53);

    localStorage.setItem("OrderReportGeneticOwnerGuid", '00000000-0000-0000-0000-000000000000');
    localStorage.setItem("OrderReportSpeciesCode", 'ALL');
    localStorage.setItem("OrderReportVarietyName", '');
    localStorage.setItem("OrderReportFormat", 'VarietyWeek');

    $("#cboFormat").chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 5

    });

    $("#cboBreederCode").on('change', function () {
       // alert($("#cboBreederCode").chosen().val());
        localStorage.setItem("OrderReportGeneticOwnerGuid", $("#cboBreederCode").chosen().val());
      //  alert(localStorage.getItem("OrderReportGeneticOwnerGuid"));

    });
    $("#cboSpeciesName").on('change', function () {
      //  alert($("#cboSpeciesName").chosen().val());
        localStorage.setItem("OrderReportSpeciesCode", $("#cboSpeciesName").chosen().val());
      //  alert(localStorage.getItem("OrderReportSpeciesGuid"));

    });
    $("#cboFormat").on('change', function () {
        localStorage.setItem("OrderReportFormat", $("#cboFormat").chosen().val());
    });
    $("#txtVarietyName").on('change', function () {
        localStorage.setItem("OrderReportVarietyName", $("#txtVarietyName").val());

    });
    $("#get_report_button").on("click", function () {
        
        model.loadReport(
            localStorage.getItem("OrderReportGeneticOwnerGuid"),
            localStorage.getItem("OrderReportSpeciesCode"),
            localStorage.getItem("OrderReportVarietyName"),
            $("#fromYear").val(),
            $("#fromWeek").val(),
            $("#toYear").val(),
            $("#toWeek").val(),
            localStorage.getItem("OrderReportFormat"),
            $('#chkSendEmail').is(":checked")
            );
    });
};



viewModel.RefreshGeneticOwnerList = function (geneticOwnerGuid) {
    
    var geneticOwnerList = "";
   

    $.each(model.data.breederCodes, function (i, item) {
        if (item.Guid == geneticOwnerGuid || (geneticOwnerGuid == null && item.Guid == '00000000-0000-0000-0000-000000000000')) {
            geneticOwnerList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            geneticOwnerList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }
       
    });
   
    
    $("#cboBreederCode").html(geneticOwnerList); //add the options to the html

   

    $("#cboBreederCode").chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 5

    });
}


viewModel.RefreshSpeciesList = function (speciesCode) {
  
    var speciesList = "";
    //alert(speciesCode);
    if (speciesCode == null) {
       // alert("is null");
        speciesList += "<option value='All' selected>All</option>";
    }
    else {
       // alert("is not null");
        speciesList += "<option value='All'>All</option>";
    }

    $.each(model.data.species, function (i, item) {
       // alert("item.name:" + item.Name);
        if (item.Code == speciesCode) {
          //  alert("is selected");
            speciesList += "<option value='" + item.Code + "' selected>" + item.Name + "</option>";
        }
        else {
            speciesList += "<option value='" + item.Code + "'>" + item.Name + "</option>";
        }

    });


    $("#cboSpeciesName").html(speciesList); //add the options to the html



    $("#cboSpeciesName").chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 5

    });
}

viewModel.RefreshReportDisplay = function (data,format) {

 //   alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    if (format == "VarietyWeek" || format == "OrderDetail")
    {
        row.append('<th style="vertical-align:top;align:center">Ship Year</th>"');
        row.append('<th style="vertical-align:top;align:center">Ship Week</th>"');
    }
    if (format == "OrderDetail") {
        row.append('<th style="vertical-align:top;align:center">Order No</th>"');
    }
    row.append('<th style="vertical-align:top;align:center">Species</th>"');
    row.append('<th style="vertical-align:top;align:center">Variety</th>"');
    row.append('<th style="vertical-align:top;align:center">Quantity Ordered</th>"');
    row.append('<th style="vertical-align:top;align:center">Quantity Shipped</th>"');
    row.append('<th style="vertical-align:top;align:center">Quantity Credit</th>"');
    row.append('<th style="vertical-align:top;align:center">Quantity Total</th>"');
    row.append('<th style="vertical-align:top;align:center">Breeder Code</th>"');
    row.append('<th style="vertical-align:top;align:center">Marketing Cost</th>"');
    row.append('<th style="vertical-align:top;align:center">Marketing Cost/Unit</th>"');

 

    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);
        if (format == "VarietyWeek" || format == "OrderDetail" )
        {
            cell = $("<td />");
            cell.append(item.Year);
            row.append(cell);

            cell = $("<td />");
            cell.append(item.Week);
            row.append(cell);
        }
        if (format == "OrderDetail") {
            cell = $("<td />");
            cell.append(item.OrderNo);
            row.append(cell); 
        }
        cell = $("<td />");
        cell.append(item.Species);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Variety);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.QuantityOrdered);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.QuantityShipped);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.QuantityCredit);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.QuantityTotal);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.GeneticOwnerCode);
        row.append(cell);
        //cell = $("<td />");
        //cell.append(item.GeneticOwnerName);
        //row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.RoyaltyCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.RoyaltyPerUnit, "$", 2, ",", ".", "%s%v"));
        row.append(cell);




        tbody.append(row);

    
    });


};


