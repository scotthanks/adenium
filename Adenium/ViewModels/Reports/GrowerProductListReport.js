﻿
var model = {};
model.data = {

    reportData: [],
};

model.config = {


    urlReport: "../api/Report/GetGrowerProductListReportData/",

};


model.loadGrowers = function (growerGuid) {
    var element = $("#cboGrowerName");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.growers = data.Growers;
        viewModel.RefreshGrowerList(growerGuid)
        return true;
    }
    bret = LoadGrowers("", element, onSuccessCall, []);
}


model.loadReport = function (growerGuid, beginDate) {
    //  alert(growerGuid);
    //  alert(format);
    // alert(email);
    var url = model.config.urlReport;
    url += "?GrowerGuid=" + growerGuid;
    url += "&BeginDate=" + beginDate;


    var element = $("#" + viewModel.config.reportTableId);

    var data = {};



    var onSuccess = function (data) {
        // alert(traverseObj(data));
        model.data.reportData = data.ReportData;

        if (data.Status.Success === true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //  alert("about to call ajax");
    //   alert(element.visible);


    modelAjaxCall(element, url, "Get", data, onSuccess, []);

};

var viewModel = {};
viewModel.config = {

    reportDivId: "report_table_div",
    reportTableId: "reportTable",

};



viewModel.Init = function () {

    model.loadGrowers();

    $("#txtBeginDate").val('4/1/18');


    localStorage.setItem("GrowerProductListReportGrowerGuid", '00000000-0000-0000-0000-000000000000');


    $("#cboGrowerName").on('change', function () {
        localStorage.setItem("GrowerProductListReportGrowerGuid", $("#cboGrowerName").val());

    });


    //$("#get_report_button").on("click", function () {

    //    model.loadReport(
    //        localStorage.getItem("GrowerProductListReportGrowerGuid"),
    //        $("#txtBeginDate").val()
    //    );
    //});

    //$("#export_button").on('click', function () {
    //    $("#reportTable").tableToCSV();
    //});
};



viewModel.RefreshGrowerList = function (growerGuid) {

    var growerList = "";
    if (growerGuid === null) {
        growerList += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
    }
    else {
        growerList += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
    }

    $.each(model.data.growers, function (i, item) {
        if (item.Guid === growerGuid) {
            growerList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            growerList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }

    });


    $("#cboGrowerName").html(growerList); //add the options to the html



    //$("#cboGrowerName").chosen({
    //    no_results_text: "Oops, nothing found!",
    //    disable_search_threshold: 5

    //});
}

viewModel.RefreshReportDisplay = function (data) {

    //   alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">Grower Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Program Code</th>"');
    row.append('<th style="vertical-align:top;align:center">Program Season Code</th>"');
    row.append('<th style="vertical-align:top;align:center">Seller Code</th>"');
    row.append('<th style="vertical-align:top;align:center">Species ID</th>"');
    row.append('<th style="vertical-align:top;align:center">Species Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Variety ID</th>"');
    row.append('<th style="vertical-align:top;align:center">Variety Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Product ID</th>"');
    row.append('<th style="vertical-align:top;align:center">Supplier Identifier</th>"');
    row.append('<th style="vertical-align:top;align:center">Product Form Code</th>"');
    row.append('<th style="vertical-align:top;align:center">Product Form Name</th>"');
    row.append('<th style="vertical-align:top;align:center">Sales Unit Qty</th>"');
    row.append('<th style="vertical-align:top;align:center">Price</th>"');
    row.append('<th style="vertical-align:top;align:center">EOD Price</th>"');
    row.append('<th style="vertical-align:top;align:center">Cutting Cost</th>"');
    row.append('<th style="vertical-align:top;align:center">Royalty Cost</th>"');
    row.append('<th style="vertical-align:top;align:center">Tag Cost</th>"');



    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        var cell = $("<td />");
        cell.append(item.GrowerName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.ProgramCode);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.ProgramSeasonCode);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SellerCode);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SpeciesID);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SpeciesName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.VarietyID);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.VarietyName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.ProductID);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SupplierIdentifier);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.ProductFormCode);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.ProductFormName);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SalesUnitQty);
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.Price, "$", 4, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.EODPrice, "$", 4, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.CuttingCost, "$", 4, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.RoyaltyCost, "$", 4, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.TagCost, "$", 4, ",", ".", "%s%v"));
        row.append(cell);



        tbody.append(row);


    });


};


