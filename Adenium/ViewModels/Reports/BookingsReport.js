﻿
var model = {};
model.data = {

    reportData: [],
};

model.config = {

   
    urlReport: "../api/Report/GetBookingReportData/",
    
};


model.loadGrowers = function (growerGuid) {
    var element = $("#cboGrowerName");

    var onSuccessCall = function (data) {
          // alert("in success after call");
           model.data.growers = data.Growers;
           viewModel.RefreshGrowerList(growerGuid)
           return true;
        }
    bret = LoadGrowers("", element, onSuccessCall, []);
}
model.loadSuppliers = function (supplierGuid) {
    var element = $("#cboSupplierName");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.suppliers = data.Suppliers;
        viewModel.RefreshSupplierList(supplierGuid)
        return true;
    }
    bret = LoadSuppliers("", element, onSuccessCall, []);
}
model.loadBrokers = function (brokerGuid) {
    var element = $("#cboBrokerName");

    var onSuccessCall = function (data) {
         //alert("in success after call");
        model.data.brokers = data.Brokers;
        viewModel.RefreshBrokerList(brokerGuid)
        return true;
    }
    bret = LoadBrokers("", element, onSuccessCall, []);
}

model.loadReport = function (growerGuid,supplierGuid,brokerGuid,fromYear,fromWeek,toYear,toWeek,format,email) {
    // alert("In Load Programs");
  //  alert(format);
   // alert(email);
    var url = model.config.urlReport;
    url += "?GrowerGuid=" + growerGuid;
    url += "&SupplierGuid=" + supplierGuid;
    url += "&BrokerGuid=" + brokerGuid;
    url += "&FromYear=" + fromYear;
    url += "&FromWeek=" + fromWeek;
    url += "&ToYear=" + toYear;
    url += "&ToWeek=" + toWeek;
    url += "&Format=" + format;
    url += "&SendEmail=" + email;

    var element = $("#" + viewModel.config.reportTableId);

    var data = {};
   
    //alert(growerGuid);
    //alert(fromYear);
    //alert(fromWeek);
    //alert(toYear);
    //alert(toWeek);

    var onSuccess = function (data) {
        // alert(traverseObj(data));
        model.data.reportData = data.ReportData;

        if (data.Status.Success === true) {
            //   alert("In Success");
            viewModel.RefreshReportDisplay(model.data.reportData,format);

        }
        else {
            //  alert("error");
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //  alert("about to call ajax");
    //   alert(element.visible);


    modelAjaxCall(element, url, "Get", data, onSuccess, []);

};

var viewModel = {};
viewModel.config = {
  
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
    cboFormatID: "cboFormat",
    chkEmailID: "chkSendEmail",
};



viewModel.Init = function () {

    model.loadGrowers();
    model.loadSuppliers();
    model.loadBrokers();
    $("#fromYear").val(new Date().getFullYear());
    $("#fromWeek").val(1);
    $("#toYear").val(new Date().getFullYear());
    $("#toWeek").val(53);

    localStorage.setItem("OrderReportGrowerGuid", '00000000-0000-0000-0000-000000000000');
    localStorage.setItem("OrderReportSupplierGuid", '00000000-0000-0000-0000-000000000000');
    localStorage.setItem("OrderReportBrokerGuid", '00000000-0000-0000-0000-000000000000');

    $("#cboGrowerName").on('change', function () {
        localStorage.setItem("OrderReportGrowerGuid", $("#cboGrowerName").chosen().val());

    });
    //$("#cboGrowerName").on('change', function () {
    //    localStorage.setItem("OrderReportGrowerGuid", $("#cboGrowerName").val());

    //});
    //$("#cboSupplierName").on('change', function () {
    //    localStorage.setItem("OrderReportSupplierGuid", $("#cboSupplierName").chosen().val());

    //});
    $("#cboBrokerName").on('change', function () {
        localStorage.setItem("OrderReportBrokerGuid", $("#cboBrokerName").chosen().val());

    });
    $("#get_report_button").on("click", function () {

        model.loadReport(
            localStorage.getItem("OrderReportGrowerGuid"),
            localStorage.getItem("OrderReportSupplierGuid"),
            localStorage.getItem("OrderReportBrokerGuid"),

            $("#fromYear").val(),
            $("#fromWeek").val(),
            $("#toYear").val(),
            $("#toWeek").val(),
            $("#cboFormat").val(),
            $('#chkSendEmail').is(":checked")
            );
    });
};



viewModel.RefreshGrowerList = function (growerGuid) {
    
    var growerList = "";
    if (growerGuid === null) {
        growerList += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
    }
    else {
        growerList += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
    }

    $.each(model.data.growers, function (i, item) {
        if (item.Guid === growerGuid) {
            growerList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            growerList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }
       
    });
   
    
    $("#cboGrowerName").html(growerList); //add the options to the html

   

    $("#cboGrowerName").chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 5

    });
}

viewModel.RefreshSupplierList = function (supplierGuid) {

    var supplierList = "";
    if (supplierGuid === null) {
        supplierList += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
    }
    else {
        supplierList += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
    }

    $.each(model.data.suppliers, function (i, item) {
        if (item.Guid === supplierGuid) {
            supplierList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            supplierList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }

    });


    $("#cboSupplierName").html(supplierList); //add the options to the html



    $("#cboSupplierName").chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 5

    });
}

viewModel.RefreshBrokerList = function (brokerGuid) {
  
    var brokerList = "";
    if (brokerGuid === null) {
        brokerList += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
    }
    else {
        brokerList += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
    }

    $.each(model.data.brokers, function (i, item) {
       // alert("item.name:" + item.Name);
        if (item.Guid === brokerGuid) {
            brokerList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            brokerList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }

    });


    $("#cboBrokerName").html(brokerList); //add the options to the html



    $("#cboBrokerName").chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 5

    });
}

viewModel.RefreshReportDisplay = function (data,format) {

 //   alert(traverseObj(data));
    var tbody = $("#" + viewModel.config.reportTableId + " tbody");

    tbody.empty();


    var row = $("<tr />");
    var rowProperties = { "class": "reportdata_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top;align:center">Booked Year</th>"');
    row.append('<th style="vertical-align:top;align:center">Booked Week</th>"');
    row.append('<th style="vertical-align:top;align:center">Grower</th>"');
    row.append('<th style="vertical-align:top;align:center">Broker</th>"');
    row.append('<th style="vertical-align:top;align:center">Sales Rep</th>"');
    row.append('<th style="vertical-align:top;align:center">Order Number</th>"');
    row.append('<th style="vertical-align:top;align:center">Vendor Order Number</th>"');
    row.append('<th style="vertical-align:top;align:center">Customer PO</th>"');
    row.append('<th style="vertical-align:top;align:center">Shipped Year</th>"');
    row.append('<th style="vertical-align:top;align:center">Shipped Week</th>"');
    row.append('<th style="vertical-align:top;align:center">Monday Date</th>"');
    row.append('<th style="vertical-align:top;align:center">Line Number</th>"');
    row.append('<th style="vertical-align:top;align:center">Species</th>"');
    row.append('<th style="vertical-align:top;align:center">Variety</th>"');
    row.append('<th style="vertical-align:top;align:center">Supplier Identifier</th>"');
    row.append('<th style="vertical-align:top;align:center">Quantity Ordered</th>"');
    row.append('<th style="vertical-align:top;align:center">Quantity Shipped</th>"');
    row.append('<th style="vertical-align:top;align:center">Quantity Credit</th>"');
    row.append('<th style="vertical-align:top;align:center">Status</th>"');


    tbody.append(row);

    $.each(data, function (i, item) {
        var row = $("<tr />");
        row.prop(rowProperties);

        cell = $("<td />");
        cell.append(item.BookedYear);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.BookedWeek);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.Grower);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Broker);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SalesRep);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.OrderNo);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.VendorOrderNo);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.CustomerPO);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Year);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Week);
        row.append(cell);

        cell = $("<td />");
        var myDate = new Date(item.MondayDate);
        var theDate = myDate.getMonth() + 1 + "/" + myDate.getDate() + "/" + myDate.getFullYear();
        cell.append(theDate);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.LineNumber);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Species);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Variety);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.SupplierIdentifier);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.QuantityOrdered);
        row.append(cell);
 
        cell = $("<td />");
        cell.append(item.QuantityShipped);
        row.append(cell);
       
        cell = $("<td />");
        cell.append(item.QuantityCredit);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.Status);
        row.append(cell);




        tbody.append(row);


    });


};


