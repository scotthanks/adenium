﻿
var model = {};
model.data = {

    reportData: [],
};

model.config = {

    
};


model.loadSuppliers = function (supplierGuid) {
    var element = $("#cboSupplierName");

    var onSuccessCall = function (data) {
        //alert("in success after call");
        model.data.suppliers = data.Suppliers;
        viewModel.RefreshSupplierList(supplierGuid);
        return true;
    };
    bret = LoadSuppliers("", element, onSuccessCall, []);
};


var viewModel = {};
viewModel.config = {
    reportDivId: "report_table_div",
    reportTableId: "reportTable",
 
};



viewModel.Init = function () {


    model.loadSuppliers();

   
    $("#txtStartWeek").val(201901);
    $("#txtEndWeek").val(201953);

    //localStorage.setItem("AvailabilityReportSupplierGuid", '00000000-0000-0000-0000-000000000000');
   


    //$("#cboSupplierName").on('change', function () {
    //    localStorage.setItem("AvailabilityReportSupplierGuid", $("#cboSupplierName").val());

    //});
   
  
};




viewModel.RefreshSupplierList = function (supplierGuid) {

    var supplierList = "";
    //if (supplierGuid === null) {
    //    supplierList += "<option value='00000000-0000-0000-0000-000000000000' selected>All</option>";
    //}
    //else {
    //    supplierList += "<option value='00000000-0000-0000-0000-000000000000'>All</option>";
    //}

    $.each(model.data.suppliers, function (i, item) {
        if (item.Guid === supplierGuid) {
            supplierList += "<option value='" + item.Guid + "' selected>" + item.Name + "</option>";
        }
        else {
            supplierList += "<option value='" + item.Guid + "'>" + item.Name + "</option>";
        }

    });


    $("#cboSupplierName").html(supplierList); //add the options to the html



};



