﻿
var model = {};
model.data = {
    dashboardOrders: [],
};

model.config = {

    urlDashboardOrderLines: "/api/Dashboards/Orders",
    
};

model.loadDashboardOrders = function () {


    var url = model.config.urlDashboardOrderLines;
    // var element = $("#" + viewModel.config.orderTableId);
    var element = $("#dashboardOrders_table_div");
    var data = {};
    data.FiscalYear = localStorage.getItem("DASHBOARDORDER_FiscalYear");
    if (data.FiscalYear == null) {
        data.FiscalYear = "";
    }
   
    data.Seller = localStorage.getItem("DASHBOARDORDER_Seller");
    if (data.Seller == null) {
        data.Seller = "";
    }
 
    data.OrderStatus = localStorage.getItem("DASHBOARDORDER_OrderStatus");
    if (data.OrderStatus == null) {
        data.OrderStatus = "";
    }
    data.IncludeInternal = localStorage.getItem("DASHBOARDORDER_IncludeInternal");
    if (data.IncludeInternal == null) {
        data.IncludeInternal = "";
    }
    data.Grower = localStorage.getItem("DASHBOARDORDER_Grower");
    if (data.Grower == null) {
        data.Grower = "";
    }
   
    //  var element = null;
    var onSuccess = function (data) {
       //   alert("in success after call");
        model.data.dashboardOrders = null;

        if (data.Status.Success == true) {
            
            //alert(traverseObj(data));
            model.data.dashboardOrders = data.DashboardOrders;
            viewModel.RefreshOrdersDisplay();
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
  //  alert("about to call:" + url);
 //   alert(traverseObj(data));
    modelAjaxCall(element, url, "GET", data, onSuccess, []);
};


var viewModel = {};
viewModel.config = {
  
    dashboardOrdersTableId: "dashboardOrdersTable",
    orderEditLinkClass: "dashboard_edit_links",
};



viewModel.Init = function () {
    
   


    model.loadDashboardOrders();
    
};


viewModel.RefreshOrdersDisplay = function () {
    

    // alert("refresh grid")
    var tbody = $("#" + viewModel.config.dashboardOrdersTableId + " tbody");
    tbody.empty();

    var cellPropLeft = { "style": "text-align:left" };

       
    var row = $("<tr />");
    var rowProperties = { "class": "dashboard_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top">Order No</th>"');
    row.append('<th style="vertical-align:top">Line Count</th>"');
    row.append('<th style="vertical-align:top">Status</th>"');
    row.append('<th style="vertical-align:top">Person Name</th>"');
    row.append('<th style="vertical-align:top">Grower</th>"');
    row.append('<th style="vertical-align:top">Broker</th>"');
    row.append('<th style="vertical-align:top">Week Number</th>"');
    row.append('<th style="vertical-align:top">Qty Ordered</th>"');
    row.append('<th style="vertical-align:top">Product Price</th>"');
    row.append('<th style="vertical-align:top">Freight Price</th>"');
    row.append('<th style="vertical-align:top">Total Price</th>"');
    row.append('<th style="vertical-align:top">Product Cost</th>"');
    row.append('<th style="vertical-align:top">Freight Cost</th>"');
    row.append('<th style="vertical-align:top">Marketing Cost</th>"');
    row.append('<th style="vertical-align:top">Total Cost</th>"');
    row.append('<th style="vertical-align:top">Profit</th>"');

   
    tbody.append(row);
  //  alert("count: " + model.data.growers.length);
    var bAddItem = false;
    $.each(model.data.dashboardOrders, function (i, item) {
       

        //   alert("add item");
        var row = $("<tr />");
        if (item.IsBadGrower == true) {
            var rowWarningProperties = { "class": " dashboard_warning_table_rows" };
            row.prop(rowWarningProperties);
        }
        else {
            var rowProperties = { "class": "dashboard_table_rows" };
            row.prop(rowProperties);
        }


        var cell = $("<td />");
        cell.prop(cellPropLeft);
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.orderEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text(item.OrderNo);
        editLink.on("click", function () {
            var url = "/CustomerService/OrderDetail?aGuid=" + item.OrderGuid + "&ShipWeekStr=" + item.WeekNumber + "&BillingStatus=" + item.Status + "&OrderStatus=" + item.MinLineStatus
            window.location.href = url;
        });
        cell.append(editLink);
        row.append(cell);


        cell = $("<td />");
        editLink = $("<a />");
        editLink.prop(editLinkProperties);
        editLink.text(item.LineCount);
        editLink.on("click", function () {
            localStorage.setItem("DASHBOARDORDERLINE_FiscalYear", "All");
            localStorage.setItem("DASHBOARDORDERLINE_OrderStatus", "All");
            //localStorage.setItem("DASHBOARDORDERLINE_Seller", "All");
            localStorage.setItem("DASHBOARDORDERLINE_IncludeInternal", "All");
            localStorage.setItem("DASHBOARDORDERLINE_Grower", "All");
            localStorage.setItem("DASHBOARDORDERLINE_OrderNo", item.OrderNo);
            var url = "/Dashboards/DashboardOrderLines"
            window.location.href = url;

        });
        cell.append(editLink);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.Status);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.PersonName);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.Grower);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.Broker);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.WeekNumber);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.QtyOrdered);
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.ProductPrice, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.FreightPrice, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.TotalPrice, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.ProductCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.FreightCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.MarketingCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.TotalCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.Profit, "$", 2, ",", ".", "%s%v"));
        row.append(cell);
       
        tbody.append(row);
      
    });



}



function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}





