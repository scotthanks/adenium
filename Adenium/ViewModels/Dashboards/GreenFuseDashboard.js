﻿

var model = {};
model.data = {
    dashboard: [],
    dashboardByGrower: [],
    lastDashboard: [],
    Heartbeat: "Go",

};

model.config = {
    urlEPSDashboard: "/api/Dashboards/GreenFuseDashboard?FiscalYear=",
   
};


model.loadDashboard = function () {
    //  alert("load Dashboard for " + fiscalYear);
    var fiscalYear = localStorage.getItem("EPS_FiscalYear");
    var uri = model.config.urlEPSDashboard + fiscalYear + "&IncludeInternal= false";
    var element = $("#" + viewModel.config.dashboardTableId);
    var onSuccess = function (data) {
        model.data.lastDashboard = model.data.dashboard;
        model.data.dashboard = data.GFDashboardItemList;
        if (data.Status.Success == true) {
            // viewModel.floatMessage(element, data.Status.StatusMessage, 2500);


            viewModel.RefreshDashboardDisplay();
        }
        else {
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
    //   alert("about to call ajax");
    //   alert(element.visible);
    modelAjaxCall(element, uri, "GET", {}, onSuccess, []);
}



var viewModel = {};
viewModel.config = {
    dashboardTableDivId: "dashboard_table_div",
    dashboardTableId: "dashboardTable",
    dashboardByGrowerTableId: "dashboardByGrowerTable",
    dashboardEditLinkClass: "dashboard_edit_links",
    dashboardEditLinkChangedClass: "dashboard_edit_changed_links",
};

viewModel.Init = function(){
    var sYear = localStorage.getItem("EPS_FiscalYear");
    if (sYear === null) {
        sYear = "2018-19"
    }
    if (sYear.toString() == "") {
        sYear = "2018-19"
    }
    $("#fiscalYearID").val(sYear);
    $("#fiscalYearID").on("change", function () {
            localStorage.setItem("EPS_FiscalYear", $("#fiscalYearID").val());
           
            model.loadDashboard();
            model.loadDashboardbyGrower();

         })
             .on("focus", function (e) {
                 $(this).select();
             });
    $("#export_button").on('click', function () {
        $("#dashboardTable").tableToCSV();
    });

    model.loadDashboard();
    model.data.Heartbeat = "Go";
    viewModel.Heartbeat();
};



viewModel.Heartbeat = function () {
    

    model.loadDashboard();
    model.loadDashboardbyGrower();

    var newtime = 40000;
    // alert(model.data.Heartbeat);
    if (model.data.Heartbeat != "Stop") {
        setTimeout(function () { viewModel.Heartbeat(); }, newtime);
    }
}
viewModel.SetAllDefaults = function () {
    localStorage.setItem("OrderNo","");
    localStorage.setItem("GrowerNameLike","");
    localStorage.setItem("FiscalYear","All");
    localStorage.setItem("WeekNunber","");
    localStorage.setItem("OrderStatus", "All");
    localStorage.setItem("SellerCode", "All");
    localStorage.setItem("FromDashboard", "0");

}
viewModel.RefreshDashboardDisplay = function () {

    // alert("refresh grid")
    var tbody = $("#" + viewModel.config.dashboardTableId + " tbody");
    tbody.empty();

    var rowProperties = { "class": "dashboard_table_rows" };
   

    var row = $("<tr />");
    
    row.prop(rowProperties);
    
    

    row.append('<th style="vertical-align:top">Status</th>"');
    row.append('<th style="vertical-align:top">Orders</th>"');
    row.append('<th style="vertical-align:top">Order Lines</th>"');
    row.append('<th style="vertical-align:top">Units Ordered</th>"');
    row.append('<th style="vertical-align:top">Units Shipped</th>"');
    row.append('<th style="vertical-align:top">Product Price</th>"');
    row.append('<th style="vertical-align:top">Freight Price</th>"');
    row.append('<th style="vertical-align:top">Total Price</th>"');
    row.append('<th style="vertical-align:top">Product Cost</th>"');
    row.append('<th style="vertical-align:top">Freight Cost</th>"');
    row.append('<th style="vertical-align:top">Marketing Cost</th>"');
    row.append('<th style="vertical-align:top">Total Cost</th>"');
    row.append('<th style="vertical-align:top">Profit</th>"');

    //for right aligned cells     
    var cellProp = {"style": "text-align:right" };
    var cellPropLeft = {"style": "text-align:left" };
    //for inks
    var editLinkProperties = { "class": viewModel.config.dashboardEditLinkClass };
    var editLinkChangedProperties = { "class": viewModel.config.dashboardEditLinkChangedClass };
    
    tbody.append(row);
    //  alert("orderNo: " + orderNo);
    var bAddItem = false;
    $.each(model.data.dashboard, function (i, item) {
  
       
        var row = $("<tr />");
        switch(item.Status) {
            case "Total":
                var rowTotalProperties = { "class": "dashboard_total_table_rows" };
                row.prop(rowTotalProperties);
                break;
            case "Cancelled":
                var rowCancelledProperties = { "class": "dashboard_cancelled_table_rows" };
                row.prop(rowCancelledProperties);
                break;
            case "Not Yet In Cart":
            case "Pending":
                //alert("here");
                var rowPendingProperties = { "class": "dashboard_pending_table_rows" };
                row.prop(rowPendingProperties);
                break;
            default:
                var rowProperties = { "class": "dashboard_table_rows" };
                row.prop(rowProperties);
        }
        
       
        
      

        var cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.Status);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        var editLink = $("<a />");
        editLink.prop(editLinkProperties);
       
        $.each(model.data.lastDashboard, function (j, lastItem) {
            if (lastItem.Status == item.Status) {
                if (lastItem.Orders != item.Orders) {
                    editLink.prop(editLinkChangedProperties);
                }

            }
        });
        editLink.on("click", function () {
            localStorage.setItem("DASHBOARDORDER_OrderStatus", item.Status);
            localStorage.setItem("DASHBOARDORDER_Seller", "GFB");
            localStorage.setItem("DASHBOARDORDER_IncludeInternal", false);
            localStorage.setItem("DASHBOARDORDER_FiscalYear", $("#fiscalYearID").val());
            localStorage.setItem("DASHBOARDORDER_Grower", "All");
            var url = "/Dashboards/DashboardOrders"
            window.location.href = url;
        });

        editLink.text(item.Orders);
        cell.append(editLink);
        row.append(cell);

        cell = $("<td />");
        editLink = $("<a />");
        editLink.prop(editLinkProperties);
        $.each(model.data.lastDashboard, function (j, lastItem) {
            if (lastItem.Status == item.Status) {
                if (lastItem.OrderLines != item.OrderLines) {
                    editLink.prop(editLinkChangedProperties);
                }

            }
        });
        editLink.on("click", function () {
        
         
            localStorage.setItem("DASHBOARDORDERLINE_FiscalYear", $("#fiscalYearID").val());
            localStorage.setItem("DASHBOARDORDERLINE_OrderStatus", item.Status);
            localStorage.setItem("DASHBOARDORDERLINE_Seller", "GFB");
            localStorage.setItem("DASHBOARDORDERLINE_IncludeInternal", false);
            localStorage.setItem("DASHBOARDORDERLINE_Grower", "All");
            localStorage.setItem("DASHBOARDORDERLINE_OrderNo","");

            var url = "/Dashboards/DashboardOrderLines"
            window.location.href = url;
          
        });
        editLink.text(item.OrderLines);
        cell.append(editLink);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.CuttingsOrdered);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.CuttingsShipped);
        row.append(cell);


        cell = $("<td />");
        cell.prop(cellProp);
        cell.append(accounting.formatMoney(item.ProductPrice, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellProp);
        cell.append(accounting.formatMoney(item.Freight, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellProp);
        cell.append(accounting.formatMoney(item.TotalPrice, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

 
        cell = $("<td />");
        cell.prop(cellProp);
        cell.append(accounting.formatMoney(item.ProductCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellProp);
        cell.append(accounting.formatMoney(item.FreightCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);


        cell = $("<td />");
        cell.prop(cellProp);
        cell.append(accounting.formatMoney(item.MarketingCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);


        cell = $("<td />");
        cell.prop(cellProp);
        cell.append(accounting.formatMoney(item.TotalCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

      
        
        cell = $("<td />");
        cell.prop(cellProp);
        cell.append(accounting.formatMoney(item.Profit, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        tbody.append(row);
        
    });

}