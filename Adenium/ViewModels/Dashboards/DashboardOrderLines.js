﻿
var model = {};
model.data = {
    dashboardOrderLines: [],
};

model.config = {

    urlDashboardOrderLines: "/api/Dashboards/OrderLines",
    
};

model.loadDashboardOrderLines = function () {


    var url = model.config.urlDashboardOrderLines;
    // var element = $("#" + viewModel.config.orderTableId);
    var element = $("#dashboardOrderLines_table_div");
    var data = {};
    data.FiscalYear = localStorage.getItem("DASHBOARDORDERLINE_FiscalYear");
    if (data.FiscalYear == null) {
        data.FiscalYear = "";
    }
    data.Seller = localStorage.getItem("DASHBOARDORDER_Seller");
    if (data.Seller == null) {
        data.Seller = "";
    }
    data.OrderStatus = localStorage.getItem("DASHBOARDORDERLINE_OrderStatus");
    if (data.OrderStatus == null) {
        data.OrderStatus = "";
    }
    data.IncludeInternal = localStorage.getItem("DASHBOARDORDERLINE_IncludeInternal");
    if (data.IncludeInternal == null) {
        data.IncludeInternal = "";
    }
    data.Grower = localStorage.getItem("DASHBOARDORDERLINE_Grower");
    if (data.Grower == null) {
        data.Grower = "";
    }
    data.OrderNo = localStorage.getItem("DASHBOARDORDERLINE_OrderNo");
    if (data.OrderNo == null) {
        data.OrderNo = "";
    }

    //  var element = null;
    var onSuccess = function (data) {
       //   alert("in success after call");
        model.data.dashboardOrderLines = null;

        if (data.Status.Success == true) {
            
        //    alert(traverseObj(data));
            model.data.dashboardOrderLines = data.DashboardOrderLines;
            viewModel.RefreshOrderLinesDisplay();
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
  //  alert("about to call:" + url);
  //  alert(traverseObj(data));
    modelAjaxCall(element, url, "GET", data, onSuccess, []);
};


var viewModel = {};
viewModel.config = {
  
    dashboardOrderLinesTableId: "dashboardOrderLinesTable",
};



viewModel.Init = function () {
    
   


    model.loadDashboardOrderLines();

    $("#export_button").on('click', function () {

        $("#dashboardOrderLinesTable").tableToCSV();
    });

};


viewModel.RefreshOrderLinesDisplay = function () {
    

    // alert("refresh grid")
    var tbody = $("#" + viewModel.config.dashboardOrderLinesTableId + " tbody");
    tbody.empty();

    var cellPropLeft = { "style": "text-align:left" };

       
    var row = $("<tr />");
    var rowProperties = { "class": "dashboard_table_rows" };
    row.prop(rowProperties);

    row.append('<th style="vertical-align:top">ID</th>"');
    row.append('<th style="vertical-align:top">Line No</th>"');
    row.append('<th style="vertical-align:top">Order No</th>"');
    row.append('<th style="vertical-align:top">Status</th>"');
    row.append('<th style="vertical-align:top">Grower</th>"');
    row.append('<th style="vertical-align:top">Broker</th>"');
    row.append('<th style="vertical-align:top">Week Number</th>"');
    row.append('<th style="vertical-align:top">Variety</th>"');
    row.append('<th style="vertical-align:top">Supplier Description</th>"');
    row.append('<th style="vertical-align:top">Qty Ordered</th>"');
    row.append('<th style="vertical-align:top">Product Price</th>"');
    row.append('<th style="vertical-align:top">Freight Price</th>"');
    row.append('<th style="vertical-align:top">Total Price</th>"');
    row.append('<th style="vertical-align:top">Product Cost</th>"');
    row.append('<th style="vertical-align:top">Freight Cost</th>"');
    row.append('<th style="vertical-align:top">Marketing Cost</th>"');
    row.append('<th style="vertical-align:top">Total Cost</th>"');
    row.append('<th style="vertical-align:top">Profit</th>"');

   
    tbody.append(row);
  //  alert("count: " + model.data.growers.length);
    var bAddItem = false;
    $.each(model.data.dashboardOrderLines, function (i, item) {
        var rowProperties = { "class": "dashboard_table_rows" };

        //   alert("add item");
        var row = $("<tr />");
        row.prop(rowProperties);


        var cell = $("<td />");
        cell.append(item.ID);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.LineNumber);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.OrderNo);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.Status);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.Grower);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.Broker);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.WeekNumber);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.Variety);
        row.append(cell);

        cell = $("<td />");
        cell.prop(cellPropLeft);
        cell.append(item.SupplierDescription);
        row.append(cell);

        cell = $("<td />");
        cell.append(item.QtyOrdered);
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.ProductPrice, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.FreightPrice, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.TotalPrice, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.ProductCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.FreightCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.MarketingCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.TotalCost, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        cell = $("<td />");
        cell.append(accounting.formatMoney(item.Profit, "$", 2, ",", ".", "%s%v"));
        row.append(cell);




      

       
        tbody.append(row);
       
        
    });



}







