﻿
var model = {};
model.data = {
    reportedHourGuid: "",
    reportedHour: [],
    reportingPeriods: [],
    contracts: [],
    invoiceStatuses: [],
    workedYear: 0,
};

model.config = {
    urlReportedHourDetail: "../api/TimeEntry/ReportedHourDetail",
    urlReportedHourAddUpdate: "../api/TimeEntry/ReportedHourAddUpdate",
    
    
};
model.loadReportedHourDetail = function () {
  // alert(model.clientGuid);
    var element = $("#hour_detail_div");
    var url = model.config.urlReportedHourDetail;
    var data = {
        ReportedHourGuid: model.reportedHourGuid
    }

    var onSuccess = function (data) {
        
        if (data.Status.Success == true) {
             
            model.data.reportedHour = data.ReportedHour;
            model.data.workedYear = model.data.reportedHour.WorkedYear;
            viewModel.RefreshReportedHourDisplay(model.data.reportedHour);
            model.loadReportingPeriods(model.data.reportedHour.ReportingPeriodGuid);
            model.loadContracts(model.data.reportedHour.ContractGuid);
            model.loadReportingYears();
            model.loadInvoiceStatuses();
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
      //alert("about to  call get");
    modelAjaxCall(element, url, "GET", data, onSuccess, []);




}
model.updateReportedHourDetail = function () {
   //  alert(clientGuid);
    var element = $("#hour_detail_div");
    var url = model.config.urlReportedHourAddUpdate;

    var reportingPeriodGuid = $('#cboReportingPeriod').val();
    var contractGuid = $('#cboContract').val();
    var hours = $('#Hours_input').val();
    var invoiceStatusGuid = $('#cboInvoiceStatus').val();
    
    

    var data = {
        ReportedHourGuid: model.reportedHourGuid,
        ReportingPeriodGuid: reportingPeriodGuid,
        ContractGuid: contractGuid,
        Hours: hours,
        InvoiceStatusGuid: invoiceStatusGuid,
       
        
    }
    console.log(data);
    var onSuccess = function (data) {
        
        if (data.Status.Success == true) {
            //  alert("in success after call");
           // alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);


        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
  //  alert("about to  call put");
    modelAjaxCall(element, url, "PUT", data, onSuccess, []);




}

model.loadReportingPeriods = function (reportingPeriodGuid) {
    var element = $("#cboReportingPeriod");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.reportingPeriods = data.ReportingPeriods;
        viewModel.RefreshReportingPeriodList(reportingPeriodGuid);
       

        return true;
    }
   
    bret = LoadReportingPeriods(model.data.workedYear, element, onSuccessCall, []);
};
model.loadContracts = function (contractGuid) {
    var element = $("#cboContract");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.contracts = data.Contracts;
        viewModel.RefreshContractList(contractGuid);


        return true;
    }
    bret = LoadContracts("", element, onSuccessCall, []);
};

model.loadReportingYears = function () {
    var element = $("#cboReportingYear");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.reportingYears = data.ReportingYears;
        viewModel.RefreshReportingYearList();
        return true;
    }
    bret = LoadReportingYears(element, onSuccessCall, []);
};
model.loadInvoiceStatuses = function () {
    var element = $("#cboInvoiceStatus");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.invoiceStatuses = data.ListItems;
        viewModel.RefreshInvoiceStatusList(model.data.reportedHour.InvoiceStatusGuid);

        return true;
    }
    bret = LoadLookupList("InvoiceStatus",element, onSuccessCall, []);
};


var viewModel = {};
viewModel.config = {
  
    hourGridId: "hourGrid",
    hourTableId: "hourTable",
    hourEditLinkClass: "hour_edit_links",
};



viewModel.Init = function () {
    model.reportedHourGuid = localStorage.getItem("ReportedHourGuid");
    model.loadReportedHourDetail();
    //alert(model.reportedHourGuid);
  
   

    $("#cboReportingYear").on('change', function () {
        // alert("change");
        model.data.workedYear = $("#cboReportingYear").val();
        model.loadReportingPeriods(model.data.reportedHour.ReportingPeriodGuid);
        
    });
    $("#cboReportingPeriod").on('change', function () {
        // alert("change");
        model.updateReportedHourDetail()

    });
    $("#cboContract").on('change', function () {
        // alert("change");
        model.updateReportedHourDetail()

    });
    $("#Hours_input").on('change', function () {
        // alert("change");
        model.updateReportedHourDetail()

    });
    $("#cboInvoiceStatus").on('change', function () {
        // alert("change");
        model.updateReportedHourDetail()

    });
   
    

    //key press event for numbers
    $('#Hours_input').keypress(function (event) {
        return isNumber(event, this)
    });
  
};


viewModel.RefreshReportedHourDisplay = function (reportedHour) {
    // alert(client.Name);
    $('#cboReportingPeriod').val(reportedHour.ReportingPeriodGuid);
    $('#cboContract').val(reportedHour.ContractGuid);
    $('#Hours_input').val(reportedHour.Hours);
    $('#cboInvoiceStatus').val(reportedHour.InvoiceStatus);
   
};

viewModel.RefreshReportingPeriodList = function (reportingPeriodGuid) {
   // alert(reportingPeriodGuid);
    var element = $("#cboReportingPeriod");
    DisplayList(element, model.data.reportingPeriods, reportingPeriodGuid, false);
    
};

viewModel.RefreshContractList = function (contractGuid) {
    var element = $("#cboContract");
    DisplayList(element, model.data.contracts, contractGuid, false);

};
viewModel.RefreshInvoiceStatusList = function (invoiceStatusGuid) {
    var element = $("#cboInvoiceStatus");
    console.log(model.data.invoiceStatuses);
    DisplayList(element, model.data.invoiceStatuses, invoiceStatusGuid, false);

};


viewModel.RefreshReportingYearList = function () {
    var cboElement = $("#cboReportingYear");
  //  console.log(model.data.reportingYears);



    var list = "";
    $.each(model.data.reportingYears, function (i, item) {
      //  console.log(item);
        if (item.Year == model.data.workedYear) {
            list += "<option value='" + item.Year + "' selected>" + item.Display + "</option>";
        }
        else {
            list += "<option value='" + item.Year + "'>" + item.Display + "</option>";
        }

    });


    cboElement.html(list); //add the options to the html

    cboElement.chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 3

    });

};