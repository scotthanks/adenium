﻿
var model = {};
model.data = {
    reportedHours: [],
    reportingYears: [],
    currentYear: 2020
};

model.config = {
    urlReportedHoursList: "../api/TimeEntry/ReportedHoursList",
    urlReportedHourAddUpdate: "../api/TimeEntry/ReportedHourAddUpdate",
    urlReportedHourDelete: "../api/TimeEntry/ReportedHourDelete",
    urlReportingYearsList: "../api/TimeEntry/ReportingYearsList",
    
   
};

model.loadReportedHours = function () {
    var element = $("#hours_table_div");
    var url = model.config.urlReportedHoursList;

    var data = {
        WorkedYear: model.data.currentYear 
    };
 
   
    var onSuccess = function (data) {
       // alert("in success after call");
      //  alert(traverseObj(data));
        if (data.Status.Success === true) {

            model.data.reportedHours = data.ReportedHours;
            viewModel.RefreshReportedHoursDisplay();
            return true;

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
  //  alert("about to  call:" + url);
    modelAjaxCall(element, url, "GET", data, onSuccess, []);
}


model.addReportedHours = function () {
    //  alert(clientGuid);
    var element = $("#button_add");
    var url = model.config.urlReportedHourAddUpdate;

   

    var data = {
        //to Do
        ReportedHourGuid: null,
        ReportingPeriodGuid: "56E78B32-D996-4249-8DD1-19852A253DEE",
        ContractGuid: "D8FCEBBB-5CB1-406B-BDCB-4055361897E4",
        Hours: 0,
        InvoiceStatus: "Invoiced",
        InvoiceStatusCode: 1,
        
    }   

    var onSuccess = function (data) {

        if (data.Status.Success == true) {
            //  alert("in success after call");
            // alert(data.Status.StatusMessage);
            console.log(data);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            localStorage.setItem("ReportedHourGuid", data.ReportedHourGuid);
      //      alert(data.ConsultantGuid);
            window.location.href = "/TimeEntry/ReportedHourDetail";

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call put");
    modelAjaxCall(element, url, "PUT", data, onSuccess, []);
}


model.deleteReportedHour = function (element, reportedHourGuid) {
    //  alert(clientGuid);

    var url = model.config.urlReportedHourDelete;

    var data = {
        ReportedHourGuid: reportedHourGuid,
    }

    var onSuccess = function (data) {

        if (data.Status.Success == true) {
            //  alert("in success after call");
            // alert(data.Status.StatusMessage);
            console.log(data);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            model.loadReportedHours();

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call put");
    modelAjaxCall(element, url, "DELETE", data, onSuccess, []);
}
model.loadReportingYears = function () {
    var element = $("#cboReportingYear");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.reportingYears = data.ReportingYears;
        viewModel.RefreshReportingYearList();

      
        $.each(model.data.reportingYears, function (i, item) {
            if (item.IsCurrentYear == 1) {
                model.data.currentYear = item.Year
            }
        });
        
        model.loadReportedHours();

        return true;
    }
    bret = LoadReportingYears(element, onSuccessCall, []);
};


var viewModel = {};
viewModel.config = {
 
    hoursTableId: "hoursTable",
    hoursEditLinkClass: "hours_edit_links",
};



viewModel.Init = function () {
   
    $("#cboReportingYear").on('change', function () {
        // alert("change");
        model.data.currentYear = $("#cboReportingYear").val();
        model.loadReportedHours();

    });

    model.loadReportingYears();
    $("#button_add").off().on("click", function () {
        model.addReportedHours();
    });



};


viewModel.RefreshReportedHoursDisplay = function () {
    

     //alert("refresh grid")
    var tbody = $("#" + viewModel.config.hoursTableId + " tbody");
    tbody.empty();

       
    var row = $("<tr />");
    var rowProperties = { "class": "timeEntryData_table_rows" };
    row.prop(rowProperties);
    

    row.append('<th style="vertical-align:top">Reporting Period</th>"');
    row.append('<th style="vertical-align:top">Contract</th>"');
    row.append('<th style="vertical-align:top">Hours</th>"');
    row.append('<th style="vertical-align:top">Client Billed</th>"');
    row.append('<th style="vertical-align:top">Consultant Paid</th>"');
    row.append('<th style="vertical-align:top">Invoice Status</th>"');
 
    
    row.append('<th style="vertical-align:top">Edit</th>"');
    row.append('<th style="vertical-align:top">Delete</th>"');
   
    var cellPropRight = { "style": "text-align:right" };
    var cellPropLeft = { "style": "text-align:left" };

    tbody.append(row);

 
    $.each(model.data.reportedHours, function (i, item) {
        
        //   alert("add item");
        var row = $("<tr />");
        var rowProperties = { "class": "timeEntryData_table_rows" };
        row.prop(rowProperties);
        
        var cell = $("<td />");
        cell.append(item.ReportingPeriodDisplay);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.ContractDisplay);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.Hours);
        row.append(cell);

        var cell = $("<td />");
        cell.prop(cellPropRight);
        cell.append(accounting.formatMoney(item.ClientDollarsBilled, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        var cell = $("<td />");
        cell.prop(cellPropRight);
        cell.append(accounting.formatMoney(item.ConsultantDollarsPaid, "$", 2, ",", ".", "%s%v"));
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.InvoiceStatusDisplay);
        row.append(cell);

    
       

        var cell = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.hoursEditLinkClass};
        editLink.prop(editLinkProperties);
        editLink.text("Edit");
        editLink.on("click", function () {
            localStorage.setItem("ReportedHourGuid", item.Guid);
            window.location.href = "/TimeEntry/ReportedHourDetail";
        });
        cell.append(editLink);
        row.append(cell);

        var cell = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.hoursEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text("Delete");
        editLink.on("click", function () {
            model.deleteReportedHour(editLink,item.Guid);
        });
        cell.append(editLink);
        row.append(cell);


        tbody.append(row);
        
    });


   

}

viewModel.RefreshReportingYearList = function () {
    var cboElement = $("#cboReportingYear");
    console.log(model.data.reportingYears);
    


    var list = "";
    $.each(model.data.reportingYears, function (i, item) {
        //console.log(item);
        if (item.IsCurrentYear == 1) {
            list += "<option value='" + item.Year + "' selected>" + item.Display + "</option>";
        }
        else {
            list += "<option value='" + item.Year + "'>" + item.Display + "</option>";
        }

    });


    cboElement.html(list); //add the options to the html

    cboElement.chosen({
        no_results_text: "Oops, nothing found!",
        disable_search_threshold: 3

    });
    
};



