﻿
var model = {};
model.data = {
    consultants: [],
};

model.config = {
    urlConsultantList: "../api/Contract/ConsultantList",
    urlConsultantAddUpdate: "../api/Contract/ConsultantAddUpdate",
    urlConsultantDelete: "../api/Contract/ConsultantDelete",
   
   
};
model.loadConsultants = function () {
    var element = $("#consultant_table_div");
    var url = model.config.urlConsultantList;

    var data = {};
 
   
    var onSuccess = function (data) {
       // alert("in success after call");
      //  alert(traverseObj(data));
        if (data.Status.Success === true) {

            model.data.consultants = data.Consultants;
            viewModel.RefreshConsultantDisplay();
            return true;

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //alert("about to  call:" + url);
    modelAjaxCall(element, url, "GET", data, onSuccess, []);
}


model.addConsultant = function () {
    //  alert(clientGuid);
    var element = $("#button_add");
    var url = model.config.urlConsultantAddUpdate;

    var name = "new";



    var data = {
        ConsultantGuid: null,
        CompanyName: " ",
        FirstName: " ",
        LastName: " ",
        IsInternal: "false",
    }   

    var onSuccess = function (data) {

        if (data.Status.Success == true) {
            //  alert("in success after call");
            // alert(data.Status.StatusMessage);
            console.log(data);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            localStorage.setItem("ConsultantGuid", data.ConsultantGuid);
      //      alert(data.ConsultantGuid);
            window.location.href = "/Contract/ConsultantDetail";

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call put");
    modelAjaxCall(element, url, "PUT", data, onSuccess, []);
}


model.deleteConsultant = function (element, consultantGuid) {
    //  alert(clientGuid);

    var url = model.config.urlConsultantDelete;

    var data = {
        ConsultantGuid: consultantGuid,
    }

    var onSuccess = function (data) {

        if (data.Status.Success == true) {
            //  alert("in success after call");
            // alert(data.Status.StatusMessage);
            console.log(data);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            model.loadConsultants();

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call put");
    modelAjaxCall(element, url, "DELETE", data, onSuccess, []);
}



var viewModel = {};
viewModel.config = {
 
    clientTableId: "consultantTable",
    clientEditLinkClass: "consultant_edit_links",
};



viewModel.Init = function () {
   
    model.loadConsultants();
    $("#button_add").off().on("click", function () {
        model.addConsultant();
    });
};


viewModel.RefreshConsultantDisplay = function () {
    

    // alert("refresh grid")
    var tbody = $("#" + viewModel.config.clientTableId + " tbody");
    tbody.empty();

       
    var row = $("<tr />");
    var rowProperties = { "class": "consultantData_table_rows" };
    row.prop(rowProperties);
    

    row.append('<th style="vertical-align:top">Company Name</th>"');
    row.append('<th style="vertical-align:top">First Name</th>"');
    row.append('<th style="vertical-align:top">Last Name</th>"');
    row.append('<th style="vertical-align:top">IsInternal</th>"');
    row.append('<th style="vertical-align:top">Edit</th>"');
    row.append('<th style="vertical-align:top">Delete</th>"');
   

   

    tbody.append(row);
  //  alert("count: " + model.data.growers.length);
    var bAddItem = false;
    $.each(model.data.consultants, function (i, item) {
        
        //   alert("add item");
        var row = $("<tr />");
        var rowProperties = { "class": "consultantData_table_rows" };
        row.prop(rowProperties);
        
        var cell = $("<td />");
        cell.append(item.CompanyName);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.FirstName);
        row.append(cell);
      
        var cell = $("<td />");
        cell.append(item.LastName);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.IsInternal);
        row.append(cell);

        var cell = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.consultantEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text("Edit");
        editLink.on("click", function () {
            localStorage.setItem("ConsultantGuid", item.Guid);
            window.location.href = "/Contract/ConsultantDetail";
        });
        cell.append(editLink);
        row.append(cell);

        var cell = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.consultantEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text("Delete");
        editLink.on("click", function () {
            model.deleteConsultant(editLink,item.Guid);
        });
        cell.append(editLink);
        row.append(cell);


        tbody.append(row);
        
    });


   

}






