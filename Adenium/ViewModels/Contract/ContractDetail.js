﻿
var model = {};
model.data = {
    contractGuid: "",
    contract: [],
    clients: [],
    consultants: [],
};

model.config = {
    urlContractDetail: "../api/Contract/ContractDetail",
    urlContractAddUpdate: "../api/Contract/ContractAddUpdate"
 
};
model.loadContractDetail = function () {
  // alert(model.clientGuid);
    var element = $("#contract_detail_div");
    var url = model.config.urlContractDetail;
    var data = {
        ContractGuid: model.contractGuid
    }

    var onSuccess = function (data) {
        
        if (data.Status.Success == true) {
             
            model.data.contract = data.Contract;
            viewModel.RefreshContractDisplay(model.data.contract);
            model.loadClients(model.data.contract.ClientGuid);
            model.loadConsultants(model.data.contract.ConsultantGuid);
            
        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
      //alert("about to  call get");
    modelAjaxCall(element, url, "GET", data, onSuccess, []);




}
model.updateContractDetail = function () {
   //  alert(clientGuid);
    var element = $("#contract_detail_div");
    var url = model.config.urlContractAddUpdate;

    var clientGuid = $('#cboClient').val();
    var consultantGuid = $('#cboConsultant').val();
    var startDate = $('#StartDate_input').val();
    var endDate = $('#EndDate_input').val();
    var clientRate = $('#ClientRate_input').val();
    var consultantRate = $('#ConsultantRate_input').val();
    var isProposed = $('#IsProposed_input').prop('checked');
    var isUnderContract = $('#IsUnderContract_input').prop('checked');
    var percentExpected = $('#PercentExpected_input').val();
    

    var data = {
        ContractGuid: model.contractGuid,
        ClientGuid: clientGuid,
        ConsultantGuid: consultantGuid,
        StartDate: startDate,
        EndDate: endDate,
        ClientRate: clientRate,
        ConsultantRate: consultantRate,
        IsProposed: isProposed,
        IsUnderContract: isUnderContract,
        PercentExpected: percentExpected,
    }
    console.log(data);
    var onSuccess = function (data) {
        
        if (data.Status.Success == true) {
            //  alert("in success after call");
           // alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);


        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
  //  alert("about to  call put");
    modelAjaxCall(element, url, "PUT", data, onSuccess, []);




}

model.loadClients = function (clientGuid) {
    var element = $("#cboClient");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.clients = data.Clients;
        viewModel.RefreshClientList(clientGuid);
       

        return true;
    }
    bret = LoadClients("", element, onSuccessCall, []);
};
model.loadConsultants = function (consultantGuid) {
    var element = $("#cboConsultant");

    var onSuccessCall = function (data) {
        // alert("in success after call");
        model.data.consultants = data.Consultants;
        viewModel.RefreshConsultantList(consultantGuid);


        return true;
    }
    bret = LoadConsultants("", element, onSuccessCall, []);
};
var viewModel = {};
viewModel.config = {
  
    contractGridId: "contractGrid",
    contractTableId: "contractTable",
    contractEditLinkClass: "contract_edit_links",
};



viewModel.Init = function () {
    
    model.contractGuid = localStorage.getItem("ContractGuid");
    //alert(model.consultantGuid);
  
    model.loadContractDetail();


    $("#cboClient").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });
    $("#cboConsultant").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });
    $("#StartDate_input").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });
    $("#EndDate_input").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });
    $("#ClientRate_input").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });
    $("#ConsultantRate_input").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });
    $("#IsProposed_input").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });
    $("#IsUnderContract_input").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });
    $("#PercentExpected_input").on('change', function () {
        // alert("change");
        model.updateContractDetail()

    });

    //key press event for numbers
    $('#ClientRate_input').keypress(function (event) {
        return isNumber(event, this)
    });
    $('#ConsultantRate_input').keypress(function (event) {
        return isNumber(event, this)
    });
    $('#PercentExpected_input').keypress(function (event) {
        return isNumber(event, this)
    });
};


viewModel.RefreshContractDisplay = function (contract) {
    // alert(client.Name);
    $('#cboClient').val(contract.ClientGuid);
    $('#cboConsultant').val(contract.ConsultantGuid_input);
   
    var startDate = new Date(contract.StartDate);
    $('#StartDate_input').val(startDate.formatMMDDYYYY());
    var endDate = new Date(contract.EndDate);
    $('#EndDate_input').val(endDate.formatMMDDYYYY());
    $('#ClientRate_input').val(contract.ClientRate);
    $('#ConsultantRate_input').val(contract.ConsultantRate);
    $('#IsProposed_input').prop('checked', contract.IsProposed);
    $('#IsUnderContract_input').prop('checked', contract.IsUnderContract);
    $('#PercentExpected_input').val(contract.PercentExpected);




    
    $("#StartDate_input").datepicker();
    $("#EndDate_input").datepicker();
   

};

viewModel.RefreshClientList = function (clientGuid) {
    var element = $("#cboClient");
    DisplayList(element, model.data.clients, clientGuid, false);
    
};

viewModel.RefreshConsultantList = function (consultantGuid) {
    var element = $("#cboConsultant");
    DisplayList(element, model.data.consultants, consultantGuid, false);

};

