﻿
var model = {};
model.data = {
    contracts: [],
};

model.config = {
    urlContractList: "../api/Contract/ContractList",
    urlContractAddUpdate: "../api/Contract/ContractAddUpdate",
    urlContractDelete: "../api/Contract/ContractDelete",
   
   
};
model.loadContracts = function () {
    var element = $("#contract_table_div");
    var url = model.config.urlContractList;

    var data = {};
 
   
    var onSuccess = function (data) {
       // alert("in success after call");
      //  alert(traverseObj(data));
        if (data.Status.Success === true) {

            model.data.contracts = data.Contracts;
            viewModel.RefreshContractDisplay();
            return true;

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
  //  alert("about to  call:" + url);
    modelAjaxCall(element, url, "GET", data, onSuccess, []);
}


model.addContract = function () {
    //  alert(clientGuid);
    var element = $("#button_add");
    var url = model.config.urlContractAddUpdate;

    var name = "new";



    var data = {
        
        ContractGuid: null,
        ClientGuid: "13173D0D-CC89-4FE6-AB6F-E99DF563E77D",
        ConsultantGuid: "E525939C-FC3D-41A7-AF8A-C8B1486D63DD",
        StartDate: GetCurrentDate(),
        EndDate: GetCurrentDate(),
        ClientRate: 0,
        ConsultantRate: 0,
        IsProposed: false,
        IsUnderContract: false,
        PercentExpected: 100,
    }   

    var onSuccess = function (data) {

        if (data.Status.Success == true) {
            //  alert("in success after call");
            // alert(data.Status.StatusMessage);
            console.log(data);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            localStorage.setItem("ContractGuid", data.ContractGuid);
      //      alert(data.ConsultantGuid);
            window.location.href = "/Contract/ContractDetail";

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call put");
    modelAjaxCall(element, url, "PUT", data, onSuccess, []);
}


model.deleteContract = function (element, contractGuid) {
    //  alert(clientGuid);

    var url = model.config.urlContractDelete;

    var data = {
        ContractGuid: contractGuid,
    }

    var onSuccess = function (data) {

        if (data.Status.Success == true) {
            //  alert("in success after call");
            // alert(data.Status.StatusMessage);
            console.log(data);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            model.loadContracts();

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call put");
    modelAjaxCall(element, url, "DELETE", data, onSuccess, []);
}



var viewModel = {};
viewModel.config = {
 
    contractTableId: "contractTable",
    contractEditLinkClass: "contract_edit_links",
};



viewModel.Init = function () {
   
    model.loadContracts();
    $("#button_add").off().on("click", function () {
        model.addContract();
    });
};


viewModel.RefreshContractDisplay = function () {
    

    // alert("refresh grid")
    var tbody = $("#" + viewModel.config.contractTableId + " tbody");
    tbody.empty();

       
    var row = $("<tr />");
    var rowProperties = { "class": "contractData_table_rows" };
    row.prop(rowProperties);
    

    row.append('<th style="vertical-align:top">Client</th>"');
    row.append('<th style="vertical-align:top">Company Name</th>"');
    row.append('<th style="vertical-align:top">First Name</th>"');
    row.append('<th style="vertical-align:top">Last Name</th>"');
    row.append('<th style="vertical-align:top">Start Date</th>"');
    row.append('<th style="vertical-align:top">End Date</th>"');
    row.append('<th style="vertical-align:top">Client Rate</th>"');
    row.append('<th style="vertical-align:top">Consultant Rate</th>"');
    row.append('<th style="vertical-align:top">Percent Expected</th>"');
    row.append('<th style="vertical-align:top">Proposed</th>"');
    row.append('<th style="vertical-align:top">Under Contract</th>"');
    row.append('<th style="vertical-align:top">Edit</th>"');
    row.append('<th style="vertical-align:top">Delete</th>"');
   
    var cellPropRight = { "style": "text-align:right" };
    var cellPropLeft = { "style": "text-align:left" };

    tbody.append(row);
  //  alert("count: " + model.data.growers.length);
    var bAddItem = false;
    $.each(model.data.contracts, function (i, item) {
        
        //   alert("add item");
        var row = $("<tr />");
        var rowProperties = { "class": "contractData_table_rows" };
        row.prop(rowProperties);
        
        var cell = $("<td />");
        cell.append(item.ClientName);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.CompanyName);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.FirstName);
        row.append(cell);
      
        var cell = $("<td />");
        cell.append(item.LastName);
        row.append(cell);

        var cell = $("<td />");
        var startDate = new Date(item.StartDate); 
        cell.append(startDate.formatMMDDYYYY());
        row.append(cell);

        var cell = $("<td />");
        var endDate = new Date(item.EndDate);
        cell.append(endDate.formatMMDDYYYY());
        row.append(cell);

        var cell = $("<td />");
        cell = $("<td />");
        cell.prop(cellPropRight);
        cell.append(accounting.formatMoney(item.ClientRate, "$", 2, ",", ".", "%s%v"));
        row.append(cell);



        var cell = $("<td />");
        cell = $("<td />");
        cell.prop(cellPropRight);
        cell.append(accounting.formatMoney(item.ConsultantRate, "$", 2, ",", ".", "%s%v"));
        row.append(cell);
        

        var cell = $("<td />");
        cell.append(item.PercentExpected);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.IsProposed);
        row.append(cell);

        var cell = $("<td />");
        cell.append(item.IsUnderContract);
        row.append(cell);

        var cell = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.contractEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text("Edit");
        editLink.on("click", function () {
            localStorage.setItem("ContractGuid", item.Guid);
            window.location.href = "/Contract/ContractDetail";
        });
        cell.append(editLink);
        row.append(cell);

        var cell = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.contractEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text("Delete");
        editLink.on("click", function () {
            model.deleteContract(editLink,item.Guid);
        });
        cell.append(editLink);
        row.append(cell);


        tbody.append(row);
        
    });


   

}





