﻿
var model = {};
model.data = {
    consultantGuid: "",
    consultant: [],
};

model.config = {
    urlConsultantDetail: "../api/Contract/ConsultantDetail",
    urlConsultantAddUpdate: "../api/Contract/ConsultantAddUpdate"
 
};
model.loadConsultantDetail = function () {
  // alert(model.clientGuid);
    var element = $("#consultant_detail_div");
    var url = model.config.urlConsultantDetail;
    var data = {
        ConsultantGuid: model.consultantGuid
    }

    var onSuccess = function (data) {
        
        if (data.Status.Success == true) {
             
            model.data.consultant = data.Consultant;
            viewModel.RefreshConsultantDisplay(model.data.consultant);
           

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
      //alert("about to  call get");
    modelAjaxCall(element, url, "GET", data, onSuccess, []);




}
model.updateConsultantDetail = function () {
   //  alert(clientGuid);
    var element = $("#consultant_detail_div");
    var url = model.config.urlConsultantAddUpdate;

    var companyName = $('#CompanyName_input').val();
    var firstName = $('#FirstName_input').val();
    var lastName = $('#LastName_input').val();
    var isInternal = $('#IsInternal_input').prop('checked');
    console.log(isInternal);
 

    var data = {
        ConsultantGuid: model.consultantGuid,
        CompanyName: companyName,
        FirstName: firstName,
        LastName: lastName,
        IsInternal: isInternal,
    }

    var onSuccess = function (data) {
        
        if (data.Status.Success == true) {
            //  alert("in success after call");
           // alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);


        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
  //  alert("about to  call put");
    modelAjaxCall(element, url, "PUT", data, onSuccess, []);




}

var viewModel = {};
viewModel.config = {
  
    consultantGridId: "consultantGrid",
    consultantTableId: "consultantTable",
    consultantEditLinkClass: "contract_edit_links",
};



viewModel.Init = function () {
    
    model.consultantGuid = localStorage.getItem("ConsultantGuid");
    //alert(model.consultantGuid);
  
    model.loadConsultantDetail();


    $("#CompanyName_input").on('change', function () {
        // alert("change");
        model.updateConsultantDetail()

    });
    $("#FirstName_input").on('change', function () {
        // alert("change");
        model.updateConsultantDetail()

    });
    $("#LastName_input").on('change', function () {
        // alert("change");
        model.updateConsultantDetail()

    });
    $("#IsInternal_input").on('change', function () {
        // alert("change");
        model.updateConsultantDetail()

    });
};


viewModel.RefreshConsultantDisplay = function (consultant) {
   // alert(client.Name);
    $('#CompanyName_input').val(consultant.CompanyName);
    $('#FirstName_input').val(consultant.FirstName);
    $('#LastName_input').val(consultant.LastName);
    console.log(consultant.IsInternal);
    $('#IsInternal_input').prop('checked', consultant.IsInternal);
 
}





