﻿

var model = {};
model.data = {
    Users: 0,
    ActiveUsers: 0,
    LinesOfCredit: 0,
    Searches: 0,
    
};

model.config = {
    
    urlLoadHomePage: "/api/Home",
   

};
var viewModel = {};
viewModel.config = {
    spanUsers: "spanUsers",
    spanActiveUsers: "spanActiveUsers",
    spanLine: "spanLine",
    spanSearch: "spanSearch",
};

viewModel.Init = function(){
   

    viewModel.loadBoxes();
};

viewModel.loadBoxes = function () {
    //alert("loadBoxes");
    var uri = model.config.urlLoadHomePage;
    var element = $("#" + viewModel.config.spanUsers);
    var onSuccess = function (data) {
       // alert(traverseObj(data));
        model.data.Users = data.Users;
        model.data.ActiveUsers = data.ActiveUsers;
        model.data.Searches = data.Searches;
        model.data.LinesOfCredit = data.LinesOfCredit;
        if (data.Status.Success === true) {
        
            
            viewModel.RefreshDisplay();
        }
        else {
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);

        }
    };
 //   alert("about to call ajax");
 //   alert(element.visible);
    modelAjaxCall(element, uri, "GET", {}, onSuccess, []);

  

}
viewModel.RefreshDisplay = function () {
    //alert(model.data.ActiveUsers);
    //alert($("#" + model.config.spanActiveUsers).text());
    //$("#submittername").text("testing");
    $("#" + viewModel.config.spanActiveUsers).text(model.data.ActiveUsers);
    $("#" + viewModel.config.spanUsers).text(model.data.Users);
    $("#" + viewModel.config.spanSearch).text(model.data.Searches);
    $("#" + viewModel.config.spanLine).text(model.data.LinesOfCredit);
};