﻿
var model = {};
model.data = {
    clients: [],
};

model.config = {
    urlClientList: "../api/Client/ClientList",
    urlClientAddUpdate: "../api/Client/ClientAddUpdate",
    urlClientDelete: "../api/Client/ClientDelete"

   
};
model.loadClients = function () {
    var element = $("#client_table_div");
    var url = model.config.urlClientList;

    var data = {};
 
   
    var onSuccess = function (data) {
       // alert("in success after call");
      //  alert(traverseObj(data));
        if (data.Status.Success === true) {

            model.data.clients = data.Clients;
            viewModel.RefreshClientDisplay();
            return true;

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //alert("about to  call:" + url);
    modelAjaxCall(element, url, "GET", data, onSuccess, []);
}

model.addClient = function () {
    //  alert(clientGuid);
    var element = $("#button_add");
    var url = model.config.urlClientAddUpdate;

    var name = "new";



    var data = {
        ClientGuid: null,
        ClientName: name,
    }

    var onSuccess = function (data) {

        if (data.Status.Success == true) {
            //  alert("in success after call");
            // alert(data.Status.StatusMessage);
            console.log(data);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            localStorage.setItem("ClientGuid", data.ClientGuid);
           // alert(data.ClientGuid);
            window.location.href = "/Client/ClientDetail";

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call put");
    modelAjaxCall(element, url, "PUT", data, onSuccess, []);
}


model.deleteClient = function (element,clientGuid) {
    //  alert(clientGuid);
    
    var url = model.config.urlClientDelete;

    var data = {
        ClientGuid: clientGuid,  
    }

    var onSuccess = function (data) {

        if (data.Status.Success == true) {
            //  alert("in success after call");
            // alert(data.Status.StatusMessage);
            console.log(data);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
            model.loadClients();

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
    //  alert("about to  call put");
    modelAjaxCall(element, url, "DELETE", data, onSuccess, []);
}


var viewModel = {};
viewModel.config = {

    clientTableId: "clientTable",
    clientEditLinkClass: "client_edit_links",
};


viewModel.Init = function () {
   
    model.loadClients();
    // Set add button event
    $("#button_add").off().on("click", function () {
        model.addClient();
    });
};


viewModel.RefreshClientDisplay = function () {
    

    // alert("refresh grid")
    var tbody = $("#" + viewModel.config.clientTableId + " tbody");
    tbody.empty();

       
    var row = $("<tr />");
    var rowProperties = { "class": "clientData_table_rows" };
    row.prop(rowProperties);
    

    row.append('<th style="vertical-align:top">Client Name</th>"');
    row.append('<th style="vertical-align:top">Edit</th>"');
    row.append('<th style="vertical-align:top">delete</th>"');
   

   

    tbody.append(row);
  //  alert("count: " + model.data.growers.length);
    var bAddItem = false;
    $.each(model.data.clients, function (i, item) {
        
        //   alert("add item");
        var row = $("<tr />");
        var rowProperties = { "class": "clientData_table_rows" };
        row.prop(rowProperties);
        
        var cell = $("<td />");
        cell.append(item.Name);
        row.append(cell);

       

        var cell = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.clientEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text("Edit");
        editLink.on("click", function () {
            localStorage.setItem("ClientGuid", item.Guid);
            window.location.href = "/Client/ClientDetail";
        });
        cell.append(editLink);
        row.append(cell);

        var cell = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");
        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.clientEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text("Delete");
        editLink.on("click", function () {
            model.deleteClient(editLink,item.Guid)
        });
        cell.append(editLink);
        row.append(cell);

        tbody.append(row);
        
    });


   

}






