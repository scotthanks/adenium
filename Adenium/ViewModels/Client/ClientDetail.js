﻿
var model = {};
model.data = {
    clientGuid: "",
    client: [],
};

model.config = {
    urlClientDetail: "../api/Client/ClientDetail",
    urlClientUpdate: "../api/Client/ClientAddUpdate"
 
};
model.loadClientDetail = function () {
  // alert(model.clientGuid);
    var element = $("#client_detail_div");
    var url = model.config.urlClientDetail;
    var data = {
        ClientGuid: model.clientGuid
    }

    var onSuccess = function (data) {
        
        if (data.Status.Success == true) {
             
            model.data.client = data.Client;
            viewModel.RefreshClientDisplay(model.data.client);
           

        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
      //alert("about to  call get");
    modelAjaxCall(element, url, "GET", data, onSuccess, []);




}
model.updateClientDetail = function () {
   //  alert(clientGuid);
    var element = $("#client_detail_div");
    var url = model.config.urlClientUpdate;

    var name = $('#ClientName_input').val();
    


    var data = {
        ClientGuid: model.clientGuid,
        ClientName: name,
    }
    console.log(data);
    var onSuccess = function (data) {
        
        if (data.Status.Success == true) {
            //  alert("in success after call");
           // alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);


        }
        else {
            alert(data.Status.StatusMessage);
            viewModelFloatMessage(element, data.Status.StatusMessage, 2500);
        }
    };
  //  alert("about to  call put");
    modelAjaxCall(element, url, "PUT", data, onSuccess, []);




}

var viewModel = {};
viewModel.config = {
  
    clientGridId: "clientGrid",
    clientTableId: "clientTable",
    clientEditLinkClass: "customerservice_edit_links",
};



viewModel.Init = function () {
    
    model.clientGuid = localStorage.getItem("ClientGuid");
    //alert(model.clientGuid);
  
    model.loadClientDetail();


    $("#ClientName_input").on('change', function () {
        // alert("change");
        model.updateClientDetail()

    });

};


viewModel.RefreshClientDisplay = function (client) {
   // alert(client.Name);
    $('#ClientName_input').val(client.Name);
  
    
}





